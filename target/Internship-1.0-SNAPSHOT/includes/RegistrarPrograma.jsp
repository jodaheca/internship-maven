<%-- 
    Document   : RegistrarPrograma
    Created on : 26-jun-2014, 9:20:47
    Author     : Usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registrar Programa</title>
    </head>
  
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <h1 class="text-center login-title">Registrar Programa</h1>
                        <div class="account-wall">
                                <br>
                                <form name="formulario" class="form-signin" accept-charset="UTF-8" action="IngresarProgramaServlet" method="POST">
                                    <input type="text" class="form-control" placeholder="Codigo" name="codigo" id="codigo" required>
                                     <br>
                                     <input type="text" class="form-control" placeholder="Nombre" name="nombre" id="nombre" required>
                                     <br>  
                                  <button class="btn btn-lg btn-primary btn-block" type="submit">
                                     Agregar Programa</button>
                               </form>
                             </div>
                    </div>
                </div>
            </div>
    </body>
</html>

