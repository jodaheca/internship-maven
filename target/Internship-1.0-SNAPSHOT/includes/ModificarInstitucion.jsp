<%-- 
    Document   : ModificarInstitucion
    Created on : 02-dic-2014, 11:29:00
    Author     : Usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    String codigo = request.getParameter("cod");
    String nombre = request.getParameter("nombre");
    String pais = request.getParameter("pais");
%>
<html >
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Modificar Institución</title>
    </head>

    <body>

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <h1 class="text-center login-title">Modificar Institución</h1>
                    <div class="account-wall">
                        <br>
                        <form name="formulario" class="form-signin" accept-charset="UTF-8" action="ModificarInstitucionServlet" method="post">

                            <input type="text" class="form-control" value="<%=nombre%>" placeholder="Nombre" name="nombre" id="nombre" required>
                            <br>
                            <input type="text" class="form-control" value="<%=pais%>" placeholder="Pais" name="pais" id="pais" required>
                            <br>
                                <input type="hidden" value="<%=codigo%>" id="codigo" name="codigo">
                            <br>
                            <button class="btn btn-lg btn-primary btn-block" type="submit">
                                Modificar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
