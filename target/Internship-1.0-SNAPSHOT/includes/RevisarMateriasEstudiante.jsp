<%-- 
    Document   : RevisarMateriasEstudiante
    Created on : 22-jul-2014, 16:59:58
    Author     : Joaquin David Hernandez <jdavidhc94@gmail.com>
--%>

<%@page import="udea.drai.intercambio.dto.Homologacion"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List materias = (List) request.getAttribute("materias");
    String modificacion = (String) request.getAttribute("modificacion");
   // List materias=null;
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Revisar Materias</title>
    </head>
    <body>
        <div class="container">
            <h1 class="text-center login-title"> Materias Cursadas</h1>
            <div class="account-wall">
                <br><br>
                <%
                    if (materias == null || materias.isEmpty() == true) {
                %>
                <div class="col-sm-6 col-md-4 col-md-offset-4">   
                    <p class="bg-info">No hay materias registradas en este semestre</p>
                </div>
                <%
                } else {
                %>
                <%
                    if (modificacion == "true") {
                %>
                <div class="col-sm-6 col-md-4 col-md-offset-4">    
                    <p class="bg-info">Modificacion registrada Exitosamente</p>
                </div>
                <%
                    }
                %>  
                <table class="table table-bordered">
                    <thead>
                        <tr> 
                            <th>Codigo origen</th> 
                            <th>nombre origen</th> 
                            <th>Codigo destino</th> 
                            <th>Nombre destino</th> 
                            <th>Revision</th>
                            <th>Nota</th>
                        </tr>
                    </thead> 
                    <tbody>
                        <%
                            for (int i = 0; i < materias.size(); i++) {
                                Homologacion nueva = (Homologacion) materias.get(i);
                                String nombreOrigen = nueva.getMateriaLocal();
                                String nombreDestino = nueva.getMateriaExtranjero();
                                String codOrigen = nueva.getCodLocal();
                                String codDestino = nueva.getCodExtranjero();
                                char revision = nueva.getRevision();
                                String nota = nueva.getNota();
                                int cod = nueva.getCodigo();

                        %>
                    <form accept-charset="UTF-8" action="ModificarHomologacion" method="POST">   
                        <tr class="active" > 
                            <td><%=codOrigen%></td> 
                            <td><%=nombreOrigen%></td> 
                            <td><%=codDestino%></td> 
                            <td><%=nombreDestino%></td> 
                            <td>
                                <select id="estado" name="estado"  <% if (revision == 'R') { %> style="color: #009900" <% } else if (revision == 'P') { %>style="color: #d9534f" <%} else if (revision == 'N') { %>style="color: #000" <%} %>>
                                    <option value="R" style="color: #009900" <% if (revision == 'R') { %> selected="selected" <% } %> >Revisado</option>
                                    <option value="P" style="color: #d9534f"<% if (revision == 'P') { %> selected="selected" <% } %>>Pendiente</option>
                                    <option value="N" style="color: #000"<% if (revision == 'N') { %> selected="selected" <% }%>>No Revisado</option>
                                    <option value="I" style="color: #6e0dff"<% if (revision == 'I') { %> selected="selected" <% }%>>Incompleto</option>
                                </select>

                            </td> 
                        <input type="hidden" value="<%=cod%>" name="codigo">
                        <td><input type="text" size="3" value="<%if (nota != null) {%><%=nota%><%}%>" id="nota" name="nota"></td>
                        <td><input type="submit"  value="Modificar" name="Modificar" id="modificar"></td> 

                        </tr>
                    </form>
                    <%
                            }
                        }
                    %> 
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
