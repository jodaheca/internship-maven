<%-- 
    Document   : ListarProgramasAdmin
    Created on : 13-ago-2014, 15:43:30
    Author     : Usuario
--%>
<%@page import="udea.drai.intercambio.dto.Persona"%>
<%@page import="udea.drai.intercambio.dto.Programa"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%     
    List programas=(List) request.getAttribute("programas");
    List dir=(List) request.getAttribute("directores");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista de Programas</title>
    </head>
    <body>
        <div class="container">
                        <h1 class="text-center login-title"> Programas Registrados</h1>
                                <div class="account-wall">
                        <br><br>
                        <% 
                            if(programas==null){
                                %>
                        <div class="col-sm-6 col-md-4 col-md-offset-4">              
                             <p class="bg-info">Ocurrio un erro al recuperar los Programas, intente Luego</p>
                        </div>
                             <%
                            }else  if(programas.size()==0){
                                %>
                                <div class="col-sm-6 col-md-4 col-md-offset-4">        
                                    <p class="bg-info">No existen Programas registrados </p>
                                    </div>
                            <%
                            }else{
                        %>
                    <table class="table table-bordered">
                        <thead>
                            <tr> 
                                <th>Codigo</th> 
                                <th>Nombre</th> 
                                <th>Director</th> 
                            </tr>
                        </thead> 
                        <tbody>
                            <%
                              for(int i=0;i<programas.size();i++){
                                Programa nueva=(Programa) programas.get(i);
                                String codigo=nueva.getCodigo();
                                String nombre=nueva.getNombre();
                                Persona director=(Persona) dir.get(i);
                                String direct=director.getNombre();
                                String dire = director.getCedula();
                            %>
                             <tr class="active">
                                <td><% if(codigo != null){ %><%=codigo%><% } %></td> 
                                <td><% if(nombre != null){ %><%=nombre%><% } %></td> 
                                <td><% if(direct != null){ %><%=direct%><% } %></td>
                                <td><form action="PrincipalAdmin.jsp?modificarPrograma" method="POST">
                                         <input type="hidden" id="codigo" name="codigo" value="<%=codigo%>">
                                         <input type="hidden" id="nombre" name="nombre" value="<%=nombre%>">
                                         <input type="hidden"  id="director" name="director" value="<%=dire%>">
                                          <input type="hidden"  id="direct" name="direct" value="<%=direct%>">
                                        <button class="btn btn-lg btn-primary btn-xs" type="submit">
                                     Modificar</button></form></td>
                                     <td><form action="PrincipalAdmin.jsp?eliminarPrograma" method="POST">
                                         <input type="hidden" id="cod" name="cod" value="<%=codigo%>">
                                        <button class="btn btn-lg btn-primary btn-xs" type="submit">
                                     Eliminar</button></form></td>
                            </tr>

                            <%    
                            }  
                            %> 
                        </tbody>
                    </table>
                        <%
                            }
                        %>
                </div>
            </div>
    </body>
</html>
