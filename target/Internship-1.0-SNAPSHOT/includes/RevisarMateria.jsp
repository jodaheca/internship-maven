<%-- 
    Document   : RevisarMateria
    Created on : 17-jul-2014, 11:16:20
    Author     : Usuario
--%>

<%@page import="udea.drai.intercambio.dto.Persona"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
 List<Persona> estudiantes=(List)request.getAttribute("estudiantes");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Revisar Materias</title>
    </head>
  
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <h1 class="text-center login-title">Revisar Materias</h1>
                        <div class="account-wall">
                                <br>
                                <form name="formulario" class="form-signin" accept-charset="UTF-8" action="MostrarMateriasServlet" method="POST">
                                    <select class="form-control" name="estudiante" id="estudiante" onchange="obtenerIntercambios()" required="true">
                                                <option value=""> Seleccione estudiante</option>
                                                <%
                                                    for(int i=0; i<estudiantes.size();i++){
                                                        Persona estudiante=(Persona) estudiantes.get(i);
                                                        String cedula=estudiante.getCedula();
                                                        String nombre=estudiante.getNombre();
                                                        String apellido = estudiante.getApellido();
                                                    
                                                %>
                                                <option value= <%=cedula%> onchange="obtenerIntercambios()">
                                                               <%=nombre+" "+apellido%>
                                                </option>
                                                <%
                                                 }%>
                                        </select>
                                     <br>
                                     <select id="intercambios" class="form-control" name="intercambio" onchange="obtenerSemestres()" required="true">
                                            <option value="">
                                            Seleccionar Intercambio
                                     </option>
                                     </select>
                                     <br>
                                     <select id="semestres" class="form-control" name="semestres" required="true">
                                            <option value="">
                                            Seleccionar Semestre
                                     </option>
                                     </select>
                                     <br>
                                  <button class="btn btn-lg btn-primary btn-block" type="submit">
                                     Revisar</button>
                               </form>
                             </div>
                    </div>
                </div>
            </div>

    </body>
</html>
