<%-- 
    Document   : ModificarSemestre
    Created on : 02-dic-2014, 10:37:01
    Author     : Usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    String codigo = request.getParameter("cod");
    String anio = request.getParameter("anio");
    String per = request.getParameter("periodo");
    char[] peri = per.toCharArray();
    char periodo = peri[0];
%>
<html >
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Modificar Semestre</title>
    </head>

    <body>

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <h1 class="text-center login-title">Modificar Semestre</h1>
                    <div class="account-wall">
                        <br>
                        <form name="formulario" class="form-signin" accept-charset="UTF-8" action="ModificarSemestreServlet" method="post">

                            <input type="text" class="form-control" value="<%=anio%>" placeholder="Año: Ejemplo(2014)" name="anio" id="anio" required>
                            <br>
                            <select class="form-control" name="periodo" id="periodo" required="true">
                                <option value=""> Seleccione Periodo</option>
                                <option value="1" <% if (periodo == '1') { %> selected="selected" <% } %> > 1 </option>
                                <option value="2" <% if (periodo == '2') { %> selected="selected" <% }%> > 2 </option>
                            </select>
                                <input type="hidden" value="<%=codigo%>" id="codigo" name="codigo">
                            <br>
                            <button class="btn btn-lg btn-primary btn-block" type="submit">
                                Modificar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

