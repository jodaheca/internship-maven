<%-- 
    Document   : RegistrarIntercambio
    Created on : 14-jul-2014, 9:46:32
    Author     : Usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registrar Intercambio</title>
    </head>
  
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <h1 class="text-center login-title">Registrar Intercambio</h1>
                        <div class="account-wall">
                                <br>
                                <form name="formulario" class="form-signin" accept-charset="UTF-8" action="IngresarIntercambioServlet" method="post">
                                    <input type="text" class="form-control" placeholder="Nombre del Intercambio" name="nombre" id="nombre" required>
                                     <br>
                                     <select class="form-control" name="numSemestres" id="numSemestres" required="true">
                                                <option value=""> Seleccione Numero Semestres</option>
                                                <%
                                                    for(int i=1; i<8;i++){
                                                    
                                                %>
                                                <option value= <%=i%>>
                                                               <%=i%>
                                                </option>
                                                <%
                                                 }%>
                                        </select>
                                     <br>
                                  <button class="btn btn-lg btn-primary btn-block" type="submit">
                                     Agregar Intercambio</button>
                               </form>
                             </div>
                    </div>
                </div>
            </div>
    </body>
</html>

