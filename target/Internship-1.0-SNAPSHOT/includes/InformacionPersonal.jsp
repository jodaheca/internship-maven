<%-- 
    Document   : InformacionPersonal
    Created on : 02-jul-2014, 9:37:21
    Author     : Usuario
--%>

<%@page import="udea.drai.intercambio.dao.ProgramaDAO"%>
<%@page import="udea.drai.intercambio.dao.EstudianteDAO"%>
<%@page import="udea.drai.intercambio.dto.Programa"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="udea.drai.intercambio.dto.Persona"%>
<%@page import="udea.drai.intercambio.dao.IntercambioDAO"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <%
            Persona pers = (Persona) sesion.getAttribute("persona");
            EstudianteDAO estDAO = new EstudianteDAO();
            pers = estDAO.getEstudianteXusuario(pers.getUsuario());
            ProgramaDAO managerPrograma = new ProgramaDAO();
            Programa prs = managerPrograma.obtenerNombre(pers.getPrograma());

            boolean band = Boolean.TRUE;
            String nombre = "";
            String apellido = "";
            String correo = "";
            if (pers.getNombre() == null || pers.getApellido() == null || pers.getEmail() == null || pers.getNombre().equals("") || pers.getApellido().equals("") || pers.getEmail().equals("")) {
        %>
    <span style="color: crimson;" ><p>!Por favor complete su información para continuar!</p></span>
    <%
            band = Boolean.FALSE;
        } else {
            nombre = pers.getNombre();
            apellido = pers.getApellido();
            correo = pers.getEmail();
        }
    %>
</head>
<body>
    <span style="font-weight: bold; font-size: 20px;">Información del estudiante</span>
    <br />
    <br />
    <div class="panel" id="info" style="background:#F8F8F8; width: auto; margin-right: 50px; margin-left:50px">
        <form id="datos" name="datos" accept-charset="UTF-8" action="estudianteServlet" method="post" onsubmit="if (!verificarPass()) {
                    alert('Las contraseñas deben coicidir!');
                    return false;
                } else {
                    return true;
                }">
            <div class="row" style="width:auto; ">
                <div class="col-sm-4">
                    <p> Nombre: </p>    
                    <input type="text" class="form-control" id="nombre" name="nombre" maxlength="80" required="true"  onkeyup="javascript:this.value = this.value.toUpperCase();" value="<%=nombre%>" />
                </div>
                <div class="col-sm-4">
                    <p> Apellido: </p>    
                    <input type="text" class="form-control" id="apellido" name="apellido" maxlength="80" required="true" onkeyup="javascript:this.value = this.value.toUpperCase();" value="<%=apellido%>" />
                </div>
                <div class="col-sm-4">
                    <p> Cédula: </p>
                    <input type="text" class="form-control" id="cedula" name="cedula" maxlength="15" required="true" value="<%=pers.getCedula()%>" readonly /> 
                </div>
                <div class="col-sm-3">

                    <input type="hidden" class="form-control" id="user" name="user" maxlength="15" required="true" value="<%=pers.getUsuario()%>" readonly/> 
                </div>
            </div>
            <div class="row" style="width:auto">
                <br />
                <div class="col-sm-4">
                    <p> Correo Electrónico: </p>
                    <input type="email" class=" form-control" id="email" name="email"  required="true" value="<%=correo%> "/> 
                </div>
                <div class="col-lg-4">
                    <p> Programa: </p>
                    <input type = "text" class="form-control" id="programa" name="programa" value="<%=prs.getCodigo()%>" readonly />
                    <input type = "text" class="form-control" id="prog" name="prog" value="<%=prs.getNombre()%>" readonly />
                </div>
                <div class="col-sm-4">
                    <p> Contraseña: </p>
                    <input type="password" class="form-control" id="contrasena" name="contrasena"  required="true" /> 
                </div>  
                <div class="col-sm-4">
                    <p style=" font-size: 14px; font-family: bold;"> Repetir Contraseña: </p>
                    <input type="password" class="form-control" id="contrasena2" name="contrasena2"  required="true" />
                     <a href="PrincipalEstudiante.jsp?newPassword" style="font-size: 10px;">Cambiar Contraseña</a>
                </div>            
               
            </div>

          <!--  <div class="row" >
                <div class="col-sm-4" style=" width:auto; ">-->
                    <br />
                    <input type="submit"  name="submit" id="submit" class="btn btn-primary " value="Actualizar" />
           

        </form>
    </div>
    <%
        if (band) {
    %>
    <span style="font-weight: bold; font-size: 20px;">Listado de solicitudes de intercambio</span>
    <div class="panel" id="intercambios" style="background:#F8F8F8; width: auto; margin-left:50px; margin-right:50px">
        <%
            HashMap a = new HashMap();
            IntercambioDAO iDAO = new IntercambioDAO();
            a = iDAO.cargarIntercambios(pers.getCedula());
            if (a != null && !a.isEmpty()) {
                Iterator it = a.entrySet().iterator();

        %>
        <form name="intercambios" accept-charset="UTF-8" action="PrincipalEstudiante.jsp?intercambios" style="margin: 30px;">
            <select name="intercambios" style="text-align:center"  class="form-control"  >
                <%                        while (it.hasNext()) {
                        Map.Entry e = (Map.Entry) it.next();
                %>
                <option value="<%=e.getKey()%>" > <%=e.getValue()%></option>
                <%
                    }
                %>
            </select>
            <input type="submit" value="Consultar" class="btn btn-primary " />
        </form>


        <%
            }else{
        %>
            <a href="PrincipalEstudiante.jsp?intercambios=new">Nueva solicitud de intercambio</a>
        </br>
        <%
            }
            }
        %>
    </div>
</body>
</html>
