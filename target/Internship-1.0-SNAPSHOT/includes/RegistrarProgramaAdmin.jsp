<%-- 
    Document   : RegistrarProgramaAdmin
    Created on : 13-ago-2014, 16:03:46
    Author     : Usuario
--%>
<%@page import="udea.drai.intercambio.dto.Persona"%>
<%@page import="java.util.List"%>
<%
  List Profesores=(List)request.getAttribute("profesores");
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registrar Programa</title>
    </head>
  
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <h1 class="text-center login-title">Registrar Programa</h1>
                        <div class="account-wall">
                                <br>
                                <form name="formulario" class="form-signin" accept-charset="UTF-8" action="IngresarProgramaAdminServlet" method="POST">
                                    <select class="form-control" name="profesor" id="profesor" required="true">
                                                <option value=""> Seleccione Director</option>
                                                <%
                                                    for(int i=0; i<Profesores.size();i++){
                                                        Persona profesor=(Persona) Profesores.get(i);
                                                        String cedula=profesor.getCedula();
                                                        String nombre=profesor.getNombre();
                                                    
                                                %>
                                                <option value= <%=cedula%>>
                                                               <%=nombre%>
                                                </option>
                                                <%
                                                 }%>
                                        </select>
                                        <br>
                                    <input type="text" class="form-control" placeholder="Codigo" name="codigo" id="codigo" required>
                                     <br>
                                     <input type="text" class="form-control" placeholder="Nombre" name="nombre" id="nombre" required>
                                     <br>  
                                  <button class="btn btn-lg btn-primary btn-block" type="submit">
                                     Agregar Programa</button>
                               </form>
                             </div>
                    </div>
                </div>
            </div>
    </body>
</html>
