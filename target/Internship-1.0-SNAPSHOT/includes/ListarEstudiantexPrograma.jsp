<%-- 
    Document   : ListarEstudiantexPrograma
    Created on : 09-oct-2014, 15:32:59
    Author     : Usuario
--%>

<%@page import="udea.drai.intercambio.dto.Persona"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%     
    List estudiantes = (List) request.getAttribute("estudiantePrograma");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Estudiantes x Porgrama</title>
    </head>
    <body>
        <div class="container">
                        <h1 class="text-center login-title"> Estudiantes x Programa</h1>
                                <div class="account-wall">
                        <br><br>
                        <% 
                            if(estudiantes==null){
                                %>
                                <div class="col-sm-6 col-md-4 col-md-offset-4">      
                                    <p class="bg-info ">Ocurrio un erro al recuperar los Estudiantes, intente Luego </p>
                                </div>
                         <%
                            } else  if(estudiantes.size()==0){
                                %>
                                <div class="col-sm-6 col-md-4 col-md-offset-4">        
                                    <p class="bg-info">No existen Estudiantes en este Programa </p>
                                    </div>
                            <%
                            }else{
                        %>
                    <table class="table table-bordered">
                        <thead>
                            <tr> 
                                <th>Cedula</th> 
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Programa</th>
                                <th>Email</th>
                            </tr>
                        </thead> 
                        <tbody>
                            <%
                              for(int i=0;i<estudiantes.size();i++){
                                Persona nueva=(Persona) estudiantes.get(i);
                                String cedula=nueva.getCedula();
                                String nombre=nueva.getNombre();
                                String programa=nueva.getPrograma();
                                String email=nueva.getEmail();
                                String apellido = nueva.getApellido();

                            %>
                             <tr class="active">
                                <td><% if(cedula != null){ %><%=cedula%><% } %></td> 
                                <td><% if(nombre != null){ %><%=nombre%><% } %></td> 
                                <td><% if(apellido != null){ %><%=apellido%><% } %></td> 
                                <td><% if(programa != null){ %><%=programa%><% } %></td> 
                                <td><% if(email != null){ %><%=email%><% } %></td>
                            </tr>

                            <%    
                            }  
                            %> 
                        </tbody>
                    </table>
                    <%
                            }
                    %>    
                </div>
            </div>
    </body>
</html>
