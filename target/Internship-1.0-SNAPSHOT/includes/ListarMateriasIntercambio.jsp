<%-- 
    Document   : ListarMateriasIntercambio
    Created on : 13-mar-2015, 10:46:14
    Author     : Joaquin Davids Hernández Cárdenas
--%>
<%@page import="udea.drai.intercambio.dto.MateriaIntercambio"%>
<%@page import="java.util.List"%>
<%
    List materias = (List) request.getAttribute("materias");
    String nombre = request.getParameter("nombre");
    String apellido = request.getParameter("apellido");
    String nombreCompleto = nombre +" "+ apellido;
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Materias Intermcabio</title>
    </head>
    <body>
        <div class="container">
                        <h1 class="text-center login-title"><%=nombreCompleto%></h1>
                                <div class="account-wall">
                        <br><br>
                     <% 
                            if(materias==null){
                                %>
                                <div class="col-sm-6 col-md-4 col-md-offset-4">      
                                 <p class="bg-info">Ocurrio un erro al recuperar la información de las Materias, intente Luego </p>
                                 </div>
                            <%
                            } else  if(materias.size()==0){
                                %>
                                 <div class="col-sm-6 col-md-4 col-md-offset-4">    
                                     <p class="bg-info">No existen materias registradas en este Intercambio </p>
                                 </div>
                        <%
                            }else{
                        %>   
                    <table class="table table-bordered">
                        <thead>
                            <tr> 
                                <th>codigo Materia</th> 
                                <th>Materia Local</th> 
                                <th>Codigo Materia Extranjera</th> 
                                <th>Materia Extrangera</th>
                                <th>Semestre</th>
                            </tr>
                        </thead> 
                        <tbody>
                            <%
                              for(int i=0;i<materias.size();i++){
                                MateriaIntercambio nueva=(MateriaIntercambio)materias.get(i);
                                String codigoLocal=nueva.getCodLocal();
                                String materiaLocal=nueva.getMateriaLocal();
                                String codigoExtrajera = nueva.getCodExtrangera();
                                String materiaExtranjera = nueva.getMateriaExtrangera();
                                String semestre = nueva.getSemestre();
                                
                                

                            %>
                             <tr class="active">
                            <td><% if(codigoLocal != null){ %><%=codigoLocal%><% } %></td> 
                                <td><% if(materiaLocal != null){ %><%=materiaLocal%><% } %></td> 
                                <td><% if(codigoExtrajera != null){ %><%=codigoExtrajera%><% } %></td> 
                                <td><% if(materiaExtranjera != null){ %><%=materiaExtranjera%><% } %></td>
                                <td><% if(semestre != null){ %><%=semestre%><% } %></td>
                            </tr>

                            <%    
                            }  
                            %> 
                        </tbody>
                    </table>
                        <%
                            }
                        %>
                </div>
            </div>
    </body>
</html>
