<%-- 
    Document   : EliminarInstitucion
    Created on : 19-jun-2014, 9:15:33
    Author     : Usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<% String codigo = request.getParameter("codigo");%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Eliminar Institución</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <br><br>
                    <h1 class="text-center login-title">Eliminar Institución</h1>
                        <div class="account-wall">
                                <br>
                                <form name="eliminar" class="form-signin" accept-charset="UTF-8" action="EliminarInstitucionServlet" method="post">
                                    <input type="text" class="form-control" placeholder="Codigo de la Institucion" value="<%if(codigo!=null){%><%=codigo%><%}%>" name="codigo" id="codigo" required>
                                     <br>  
                                  <button class="btn btn-lg btn-primary btn-block" type="submit">
                                     Eliminar Institución</button>
                               </form>
                             </div>
                    </div>
                </div>
            </div>
    </body>
</html>
