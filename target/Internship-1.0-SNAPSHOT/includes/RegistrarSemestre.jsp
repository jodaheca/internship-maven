<%-- 
    Document   : RegistrarSemestre
    Created on : 28-jul-2014, 11:48:04
    Author     : Usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registrar Programa</title>
    </head>

    <body>

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <h1 class="text-center login-title">Registrar Semestre</h1>
                    <div class="account-wall">
                        <br>
                        <form name="formulario" class="form-signin" accept-charset="UTF-8" action="IngresarSemestreServlet" method="POST">
                            <input type="text" class="form-control" placeholder="año: Ejemplo (2014)" name="anio" id="anio" required>
                            <br>
                            <select class="form-control" name="periodo" id="periodo" required="true">
                                <option value=""> Seleccione Periodo</option>
                                <option value="1"> 1 </option>
                                <option value="2"> 2 </option>
                            </select>
                            <br>  
                            <button class="btn btn-lg btn-primary btn-block" type="submit">
                                Agregar Semestre</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
