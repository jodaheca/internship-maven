<%-- 
    Document   : ListarEstudianteAdmin
    Created on : 20/01/2015, 11:21:16 AM
    Author     : Joaquin David
--%>

<%@page import="udea.drai.intercambio.dto.Persona"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%     
    List estudiantes=(List) request.getAttribute("estudiantes");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista de Estudiantes</title>
    </head>
    <body>
        <div class="container">
                        <h1 class="text-center login-title"> Estudiantes Registrados</h1>
                                <div class="account-wall">
                        <br><br>
                        <% 
                            if(estudiantes==null){
                                %>
                                <div class="col-sm-6 col-md-4 col-md-offset-4">      
                                    <p class="bg-info ">Ocurrio un erro al recuperar los Estudiantes, intente Luego </p>
                                </div>
                         <%
                            } else  if(estudiantes.size()==0){
                                %>
                                <div class="col-sm-6 col-md-4 col-md-offset-4">        
                                    <p class="bg-info">No existen Estudiantes registrados </p>
                                    </div>
                            <%
                            }else{
                        %>
                    <table class="table table-bordered">
                        <thead>
                            <tr> 
                                <th>Cedula</th> 
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Programa</th>
                                <th>Email</th>
                                <th>Intercambio</th>
                            </tr>
                        </thead> 
                        <tbody>
                            <%
                              for(int i=0;i<estudiantes.size();i++){
                                Persona nueva=(Persona) estudiantes.get(i);
                                String cedula=nueva.getCedula();
                                String nombre=nueva.getNombre();
                                String apellido = nueva.getApellido();
                                String programa=nueva.getPrograma();
                                String email=nueva.getEmail();
                                String tipoIntermcabio=nueva.getNombreIntercambio();

                            %>
                             <tr class="active">
                                <td><% if (cedula != null){ %> <%=cedula%> <% } %></td> 
                                <td><% if (nombre != null){ %><%=nombre%><% } %></td>
                                <td><% if (apellido != null){ %><%=apellido%><% } %></td>
                                <td><% if (programa != null){ %><%=programa%><% } %></td>
                                <td><% if (email != null){ %><%=email%><% } %></td>
                                 <td><% if (tipoIntermcabio != null){ %><%=tipoIntermcabio%><% } %></td>
<!--                                <td><form  accept-charset="UTF-8" action="PrincipalAdmin.jsp?modificarEstudiante" method="POST">
                                         <input type="hidden" id="identificacion" name="identificacion" value="<%=cedula%>">
                                         <input type="hidden" id="nombre" name="nombre" value="<%=nombre%>">
                                         <input type="hidden"  id="apellido" name="apellido" value="<%=apellido%>">
                                         <input type="hidden" id="email" name="email" value="<%=email%>">
                                        <button class="btn btn-lg btn-primary btn-xs" type="submit">
                                     Modificar</button></form></td>
                                     <td><form  accept-charset="UTF-8" action="PrincipalAdmin.jsp?eliminarEstudiante" method="POST">
                                         <input type="hidden" id="id" name="id" value="<%=cedula%>">
                                        <button class="btn btn-lg btn-primary btn-xs" type="submit">
                                     Eliminar</button></form></td>-->
                            </tr>

                            <%    
                            }  
                            %> 
                        </tbody>
                    </table>
                    <%
                            }
                    %>    
                </div>
            </div>
    </body>
</html>

