<%-- 
    Document   : ModificarProfesor
    Created on : 25-nov-2014, 9:38:05
    Author     : Usuario
--%>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%--<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>--%>
<%@page import="udea.drai.intercambio.dto.Persona"%>
<!DOCTYPE html>
<html >
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Modificar Profesor</title>
    </head>
  <%
    String nombre = request.getParameter("nombre");
    String cedula = request.getParameter("identificacion");
    String apellido = request.getParameter("apellido");
    String email = request.getParameter("email");
%>
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <h1 class="text-center login-title">Modificar Profesor</h1>
                        <div class="account-wall">
                                <br>
                                <form name="formulario" class="form-signin" accept-charset="UTF-8" action="ModificarProfesorServlet" method="post">
                                    
                                    <input type="hidden" value="<%=cedula%>" id="identificacion">
                                    <input type="text" class="form-control" value="<%=nombre%>" placeholder="Nombre" name="nombre" id="nombre" required>
                                     <br>
                                     <input type="text" class="form-control" value="<%=apellido%>" placeholder="Apellido" name="apellido" id="apellido" required>
                                     <br>
                                     <input type="email" class="form-control" value="<%=email%>" placeholder="Email" name="email" id="email" required>
                                     <br> 
                                     <input type="hidden" value="<%=cedula%>" name="id" id="cedula">
                                  <button class="btn btn-lg btn-primary btn-block" type="submit">
                                     Modificar</button>
                               </form>
                             </div>
                    </div>
                </div>
            </div>
    </body>
</html>
