<%-- 
    Document   : Intercambio
    Created on : 09-jun-2014, 10:04:20
    Author     : Usuario
--%>
<%@page import="udea.drai.intercambio.dto.InfoCompromiso"%>
<%@page import="udea.drai.intercambio.dto.Persona"%>
<%@page import="udea.drai.intercambio.dao.subirDAO"%>
<%@page import="udea.drai.intercambio.dao.HomologacionDAO"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="udea.drai.intercambio.dao.IntercambioDAO"%>
<%@page import="udea.drai.intercambio.dto.Intercambio"%>
<%@page import="udea.drai.intercambio.dto.Semestre"%>
<%@page import="udea.drai.intercambio.dto.Institucion"%>
<%@page import="udea.drai.intercambio.dto.TipoIntercambio"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="udea.drai.intercambio.dao.OpcionIntercambioDAO"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Nuevo intercambio</title>



    </head>

    <%
        try {
            boolean permiso = true;
            //HttpSession sesion=request.getSession();
            Persona p1 = (Persona) session.getAttribute("persona");
            Intercambio id = new Intercambio();
            IntercambioDAO iDAO = new IntercambioDAO();
            if (request.getParameter("intercambios").equals("new")) {
                id.setCodigo(0);
                sesion.setAttribute("temporal", 0);
            } else {
                if (request.getParameter("intercambios") != "" && request.getParameter("intercambios") != null) {
                    int cod = Integer.parseInt(request.getParameter("intercambios").toString());
                    id.setCodigo(cod);
                    iDAO.cargarIntercambio(id);
                    if (!id.getEstudiante().equals(p1.getCedula())) {
    %>
    <h2>No tiene permisos para acceder a este sitio!</h2>
    <%
                    permiso = false;
                }
                sesion.setAttribute("temporal", Integer.toString(id.getCodigo()));
            } else if (sesion.getAttribute("temporal") != null && sesion.getAttribute("temporal") != "") {
                int cod = Integer.parseInt(sesion.getAttribute("temporal").toString());
                id.setCodigo(cod);
                iDAO.cargarIntercambio(id);
            }
        }
        if (permiso) {
            OpcionIntercambioDAO tipos = new OpcionIntercambioDAO();
            ArrayList lista = (ArrayList) tipos.cargarTipos();
            ArrayList listaInst = (ArrayList) tipos.cargarInstituciones();
            ArrayList periodos = (ArrayList) tipos.cargarPeriodos();
            Intercambio inter = new Intercambio();
            inter.setCodigo(id.getCodigo());
            iDAO.cargarIntercambio(inter);
            InfoCompromiso info = iDAO.compromiso(inter);
            sesion.setAttribute("compromiso", info);
    %>
    <body>

        <div class="container" align="center"  >
            <%
                if (request.getParameter("upload") != null) {
            %>
            <span style="color: #009900;" >Archivo almacenado correctamente!</span>
            <%    }
                if (request.getParameter("uploadFail") != null) {
            %>
            <span style="color: #ff0000;" >Problemas subiendo el archivo</span>
            <%    }
                if (request.getParameter("ingresada") != null) {
            %>
            <span style="color: #009900;" >Matr�cula registrada correctamente</span>
            <%    }
                if (request.getParameter("homologacionFail") != null) {
            %>
            <span style="color: #ff0000;" >Problemas registrando homologaci�n</span>
            <%    }
                if (request.getParameter("modificacionFail") != null) {
            %>
            <span style="color: #ff0000;" >Problemas realizando las modificaciones</span>
            <%    }
                if (request.getParameter("modificada") != null) {
            %>
            <span style="color: #009900;" >Homologaci�n cambiada correctamente</span>
            <%    }
                if (request.getParameter("mensaje") != null) {
                    if (request.getParameter("mensaje").equals("1")) {%>
            <span style="color: #009900;" >Informaci�n de intercambio ingresado correctamente </span>

            <%                } else if (request.getParameter("mensaje").equals("3")) {
            %>
            <span style="color: #ff0000;" >!Asegurese que la informaci�n de los semestres sea consistente! </span>

            <%                } else {%>



            <span style="color: #ff0000;" >Problemas ingresando intercambio</span>
            <%        }
                }
            %>
            <br />
            <span style="font-weight: bold; font-size: 20px;">Informaci�n de intercambio</span>
            <%
                if (sesion.getAttribute("temporal") != null && sesion.getAttribute("temporal") != "" && !request.getParameter("intercambios").equals("new")) {
            %>
            <p><a href="PDFServlet" target="_blank"> Ver compromiso</a></p>
            <%    }
            %>

            <div class="panel" id="info" style="background:#F8F8F8;  width: auto;  margin-right: 20px; margin-left:20px">
                <form id="intercambio" accept-charset="UTF-8" action="intercambioServlet" name="intercambio" method="POST">

                    <% if (id.getCodigo() != 0) {
                    %>
                    <input type="hidden" name="intercambios" id="intercambios" value="<%=id.getCodigo()%>" />
                    <%
                        }

                    %>
                    <div class="row" style="width:auto">
                        <div class="col-sm-3">
                            <p>Tipo de intercambio:</p>
                            <select class="form-control" style=" margin:20px" name="tipo" id="tipo" >
                                <%                    Iterator i = lista.iterator();
                                    while (i.hasNext()) {
                                        TipoIntercambio tipo = (TipoIntercambio) i.next();
                                        int idgetTipo = id.getTipo();
                                %>
                                <option value="<%=tipo.getCodigo()%>" <% if (idgetTipo == Integer.parseInt(tipo.getCodigo())) {%> selected <% }%> >
                                    <%=tipo.getNombre()%>
                                </option>
                                <%
                                    }
                                %>
                            </select>
                        </div>
                        <div class="col-sm-4" >
                            <p>Universidad de origen:</p>
                            <select class="form-control" style="margin:20px"  name="origen"  >
                                <%
                                    i = listaInst.iterator();
                                    while (i.hasNext()) {
                                        Institucion tipo = (Institucion) i.next();
                                        int idGetOrigen = id.getOrigen();
                                %>
                                <option value="<%=tipo.getCodigo()%>" <% if (idGetOrigen == Integer.parseInt(tipo.getCodigo())) {%> selected <%} else if (idGetOrigen == 0 && Integer.parseInt(tipo.getCodigo()) == 50) {%> selected<% }%>>
                                    <%=tipo.getNombre()%> - <%=tipo.getPais()%>
                                </option>
                                <%
                                    }
                                %>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <p>Universidad de destino:</p>
                            <select class="form-control" style="margin:20px"  name="destino" >
                                <%
                                    i = listaInst.iterator();
                                    while (i.hasNext()) {
                                        Institucion tipo = (Institucion) i.next();
                                        int idGetDestino = id.getDestino();
                                %>
                                <option value="<%=tipo.getCodigo()%>" <% if (idGetDestino == Integer.parseInt(tipo.getCodigo())) {%> selected <% }%>>
                                    <%=tipo.getNombre()%> - <%=tipo.getPais()%>
                                </option>
                                <%
                                    }
                                %>
                            </select>
                        </div>

                        <br />
                        <div class="col-lg-1" style=" width: auto;">
                            Semestre(s): <br>
                                <span class="col-sm-1" >Inicio</span>

                                <select name="semestreInicio" class="form-control" >
                                    <%
                                        i = periodos.iterator();

                                        while (i.hasNext()) {
                                            Semestre tipo = (Semestre) i.next();
                                    %>
                                    <option   value="<%=tipo.getCodigo()%>" <% if (id.getInicio() == tipo.getCodigo()) {%> selected <% }%> required >
                                        <%=tipo.getAno()%> - <%=tipo.getPeriodo()%>
                                    </option>


                                    <%
                                        }
                                    %>
                                </select>
                                <span class="col-sm-1" >Fin</span>
                                <select name="semestreFin"  class="form-control" >
                                    <%
                                        i = periodos.iterator();

                                        while (i.hasNext()) {
                                            Semestre tipo = (Semestre) i.next();
                                    %>
                                    <option   value="<%=tipo.getCodigo()%>" <% if (id.getFin() == tipo.getCodigo()) {%> selected <% }%> required >
                                        <%=tipo.getAno()%> - <%=tipo.getPeriodo()%>
                                    </option>


                                    <%
                                        }
                                    %>
                                </select>
                        </div>


                        <!--
                <div class="col-lg-1" style=" width: 200px;">
                    Semestre(s): <br>
                    <span class="col-sm-1" >Inicio</span>   Fin <br/>
                        <%
                            i = periodos.iterator();

                            while (i.hasNext()) {
                                Semestre tipo = (Semestre) i.next();
                        %>
                        <input type="radio" name="semestreInicio"  value="<%=tipo.getCodigo()%>" <% if (id.getInicio() == tipo.getCodigo()) {%> checked <% }%> required >
                        <%=tipo.getAno()%> - <%=tipo.getPeriodo()%>
                    </input>

                    <input type="radio" name="semestreFin"  value="<%=tipo.getCodigo()%>" <% if (id.getFin() == tipo.getCodigo()) {%> checked <% }%> required >
                        <%=tipo.getAno()%> - <%=tipo.getPeriodo()%>
                    </input> </br>

                        <%
                            }
                        %>

                    </div>-->

                        </br>
                        <div class="row">

                            <div class="col-lg-3">
                                Fecha de viaje: <br />
                                <input type="text" name="fechaViaje" id="fechaViaje" size="15" class="fecha"    <% if (id.getIda() != null) {%> value="<%=id.getIda()%>" <% }%> required />
                            </div>
                            <div class="col-lg-3">
                                Fecha Regreso: <br />
                                <input type="text" name="fechaRegreso" id="fechaRegreso" size="15" class="fecha"   <% if (id.getRegreso() != null) {%> value="<%=id.getRegreso()%>" <% }%> required />
                            </div>
                            <%    if (request.getParameter("intercambios").equals("new") || (request.getParameter("mensaje") != null && request.getParameter("mensaje").equals("2"))) {

                            %>
                            }
                            <input type="submit" class="btn btn-primary " value="Guardar informaci�n b�sica"/>
                            <% } %>
                        </div>
                    </div>
                </form>
            </div>

        </div>
        <%
            if (request.getParameter("intercambios").equals("new") || (request.getParameter("mensaje") != null && request.getParameter("mensaje").equals("2"))) {
        %>
        <span style="color: crimson;" ><p>!Debes guardar primero la informaci�n del intercambio!</p></span>
        <%        } else {
        %>

        <div class="container" align="center" style="width:auto">

            <span style="font-weight: bold; font-size: 20px;">Materias a Matricular</span>
            <br />

            <div class="panel" id="info" style="background:#F8F8F8;  width: auto; margin-left:30px; margin-right:30px;">

                <%
                    if (request.getParameter("add") != null) {
                        String homo = request.getParameter("consult");
                %>
                <%@include file="Materia.jsp" %>
                <%
                } else if (request.getParameter("consultar") != null) {
                } else {
                    HashMap a = new HashMap();
                    HomologacionDAO hDAO = new HomologacionDAO();
                    a = hDAO.cargarHomologaciones(id.getCodigo());
                    if (a != null && !a.isEmpty()) {
                        Iterator it = a.entrySet().iterator();
                %>
                <form name="homologaciones" method="POST" accept-charset="UTF-8" action="PrincipalEstudiante.jsp?intercambios&add" style="margin: 15px;" >

                    <select name="consult" style="text-align:center" class="form-control" id="consult">
                        <%
                            while (it.hasNext()) {
                                Map.Entry e = (Map.Entry) it.next();
                        %>
                        <option value="<%=e.getKey()%>" > <%=e.getValue()%></option>
                        <%
                            }
                        %>
                    </select>
                    <input type="submit" class="btn btn-primary " value="Consultar" />
                </form>
                <%
                    }
                %>
                <a style="margin-left: 20px; font-size: 25px; "  href="PrincipalEstudiante.jsp?intercambios&add">Agregar materia</a><br />
                <!-- <a href="PrincipalEstudiante.jsp?Bitacora">Ver bit�cora de modificaciones</a>-->
                <%
                    }
                %>
                </br>
                <span style="font-weight: 400; font-size: 15px; color: #ff0000;">*Recuerde revisar bien la informaci�n que ser� suministrada los c�digos y nombres deben ser precisos</span>
            </div>
            <br />
            <br />
            <span style="font-weight: bold; font-size: 20px; ">Soportes  </span>
            <div class="panel" id="info" style="background:#F8F8F8;  width: auto;  margin-left:30px; margin-right:30px;">
                <br />


                <%
                    if (request.getParameter("Nota") != null) {
                %>
                <%@include file="SubirNota.jsp" %>

                <%                } else {

                    subirDAO sDAO = new subirDAO();
                    HashMap hm1 = sDAO.listarArchivos(id.getCodigo());

                    if (!hm1.isEmpty()) {
                        Iterator ihm = hm1.entrySet().iterator();
                %>
                <form method="POST" id="descargar" name="descargar" accept-charset="UTF-8" action="descargaServlet">
                    <select id="archivos" style="text-align:center" class="form-control" name="archivos">
                        <%
                            while (ihm.hasNext()) {
                                Map.Entry e = (Map.Entry) ihm.next();
                        %>
                        <option value="<%=e.getKey()%>" ><%=e.getValue()%></option>


                        <%
                            }
                        %>
                    </select>

                    <input type="submit" class="btn btn-primary " value="Descargar" />
                </form>
                <br />

                <%
                    }

                %>
                <a style="font-size: 25px;" href="PrincipalEstudiante.jsp?intercambios&Nota">Nuevo registro</a>
                <%           }
                %>

                </br>
                <span style="font-weight: 400; font-size: 13px; color: #ff0000;">*Atenci�n!, debe revisar bien la informaci�n que suministrar�, los archivos de soporte deben ser cargados en formato (.PDF)</span>
            </div>

        </div>
        <%
                }
            }
        } catch (Exception e) {
        %>
        <h2>Intercambio solicitado es inv�lido</h2>
        <% }
        %>


    </body>

</html>

