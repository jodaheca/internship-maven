<%-- 
    Document   : ModificarEstudiante
    Created on : 11/12/2014, 10:41:47 AM
    Author     : Joaquin David
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String nombre = request.getParameter("nombre");
    String cedula = request.getParameter("identificacion");
    String apellido = request.getParameter("apellido");
    String email = request.getParameter("email");
    String porDefecto = "hola";
    System.out.println("Apellido que llega: " + apellido);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Modificar Estudiante</title>
    </head>
  
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <h1 class="text-center login-title">Modificar Estudiante</h1>
                        <div class="account-wall">
                            <br>
                                <form action="ModificarEstudianteServlet"  class="form-signin" accept-charset="UTF-8" method="post">
                                    
                                    <input type="hidden" value="<%=cedula%>" id="identificacion">
                                    <input type="text" class="form-control" value="<%if(nombre!=null){%><%=nombre%><%}else{%><%=porDefecto%><%}%>" placeholder="Nombre" name="nombre" id="nombre" required>
                                     <br>
                                     <input type="text" class="form-control" value="<%if(apellido!=null){%><%=apellido%><%}%>" placeholder="Apellido" name="apellido" id="apellido" required>
                                     <br>
                                     <input type="email" class="form-control" value="<%if(email!=null){%><%=email%><%}%>" placeholder="Email" name="email" id="email" required>
                                     <br> 
                                     <input type="hidden" value="<%=cedula%>" name="id" id="cedula">
                                  <button class="btn btn-lg btn-primary btn-block" type="submit">
                                     Modificar</button>
                               </form>
                             </div>
                    </div>
                </div>
            </div>
    </body>
</html>

