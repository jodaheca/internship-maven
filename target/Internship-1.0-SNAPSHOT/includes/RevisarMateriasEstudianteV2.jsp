<%-- 
    Document   : RevisarMateriasEstudianteV2
    Created on : 02-jul-2015, 9:19:13
    Author     : Joaquin David Hernández <jdavidhc94@gmail.com>
--%>


<%@page import="udea.drai.intercambio.dto.Semestre"%>
<%@page import="udea.drai.intercambio.dto.MateriasPorSemestre"%>
<%@page import="udea.drai.intercambio.dto.Homologacion"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List materiasPorSemestre = (List) request.getAttribute("materias");
    String modificacion = (String) request.getAttribute("modificacion");
    String inVigente = (String) request.getAttribute("inVigente");
    String nombreEstudiante = (String) request.getAttribute("nEstudiante");
    String ifIntercambio = (String) request.getAttribute("ifIntercambio");
    String idEstudiante = (String) request.getAttribute("idEstudiante");

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Revisar Materias</title>
    </head>
    <body>
        <div class="container">
            <h2 class="text-center "><%=nombreEstudiante%></h2>
            <%if (inVigente == "false") {
            %> 
            <h2 class="text-center" style="color: #d9534f">Intercambio vencido</h2>
            <h2 class="text-center" style="color: #d9534f"><%=ifIntercambio%></h2>

            <%
            } else {%>
            <h2 class="text-center"><%=ifIntercambio%></h2>
            <%}%>
            <br>

            <form action="Principal.jsp?ListarEstudiantes" method="POST">
                <button class="btn btn-lg btn-primary btn-warning" type="submit">
                    Atrás </button>
            </form>
            <br>
            <%
                if (modificacion == "true") {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">    
                <p class="bg-success">Modificacion registrada Exitosamente</p>
            </div>
            <br><br>
            <%
                }
            %>
            <%
                if (materiasPorSemestre == null || materiasPorSemestre.isEmpty() == true) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">   
                <p class="bg-danger">No hay materias registradas en este intercambio</p>
            </div>
            <%
            } else {
                for (int i = 0; i < materiasPorSemestre.size(); i++) {
                    MateriasPorSemestre mps = (MateriasPorSemestre) materiasPorSemestre.get(i);
                    Semestre semestre = mps.getSemestre();
                    String nSemestre = semestre.getAno() + "-" + semestre.getPeriodo();
                    List materias = mps.getMaterias();

            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">   
                <p class="bg-info"><%=nSemestre%></p>
            </div>
            <%
                if (materias.isEmpty() || materias == null) {

            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">   
                <p class="bg-danger">No hay materias registradas en este semestre</p>
            </div>
            <%                                        } else {
            %>
            <table class="table table-bordered">
                <thead>
                    <tr> 
                        <th>Codigo origen</th> 
                        <th>nombre origen</th> 
                        <th>Codigo destino</th> 
                        <th>Nombre destino</th> 
                        <th>Revision</th>
                        <th>Nota</th>
                        <th>Comentario</th>
                        <th>Operaciones</th>
                    </tr>
                </thead> 
                <tbody>
                    <%
                        for (int j = 0; j < materias.size(); j++) {
                            Homologacion nueva = (Homologacion) materias.get(j);
                            String nombreOrigen = nueva.getMateriaLocal();
                            String nombreDestino = nueva.getMateriaExtranjero();
                            String codOrigen = nueva.getCodLocal();
                            String codDestino = nueva.getCodExtranjero();
                            char revision = nueva.getRevision();
                            String nota = nueva.getNota();
                            int cod = nueva.getCodigo();
                            String comentario = nueva.getComentario();
                            if (comentario == null) {
                                comentario = "";
                            }

                    %>
                <form accept-charset="UTF-8" action="ModificarHomologacionV2" method="POST">   
                    <tr class="active" > 
                        <td><%=codOrigen%></td> 
                        <td><%=nombreOrigen%></td> 
                        <td><%=codDestino%></td> 
                        <td><%=nombreDestino%></td> 
                        <td>
                            <select id="estado" name="estado"  <% if (revision == 'R') { %> style="color: #009900" <% } else if (revision == 'P') { %>style="color: #d9534f" <%} else if (revision == 'N') { %>style="color: #000" <%} else if (revision == 'I') { %>style="color: #6e0dff" <%}%>>
                                <option value="R" style="color: #009900" <% if (revision == 'R') { %> selected="selected" <% } %> >Revisado</option>
                                <option value="P" style="color: #d9534f"<% if (revision == 'P') { %> selected="selected" <% } %>>Pendiente</option>
                                <option value="N" style="color: #000"<% if (revision == 'N') { %> selected="selected" <% }%>>No Revisado</option>
                                <option value="I" style="color: #6e0dff"<% if (revision == 'I') { %> selected="selected" <% }%>>Incompleto</option>
                            </select>

                        </td> 
                    <input type="hidden" value="<%=cod%>" name="codigo">
                    <input type="hidden" value="<%=idEstudiante%>" name="idEstudiante">
                    <td><input type="text" size="3" value="<%if (nota != null) {%><%=nota%><%}%>" id="nota" name="nota"></td>
                    <td><input type="text"   name="comentario" id="comentario" value="<%=comentario%>" /></td>
                    <td><input type="submit"  value="Modificar" name="Modificar" id="modificar"></td> 

                    </tr>
                </form>
                <%
                    }
                %>
                </tbody>
            </table>
            <%
                        }
                    }
                }
            %> 

        </div>
    </body>
</html>
