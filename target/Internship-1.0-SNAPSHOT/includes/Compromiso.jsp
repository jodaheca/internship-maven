<%-- 
    Document   : compromiso
    Created on : 25-jul-2014, 16:37:00
    Author     : Usuario
--%>

<%@page import="udea.drai.intercambio.dto.Semestre"%>
<%@page import="udea.drai.intercambio.dao.OpcionIntercambioDAO"%>
<%@page import="java.util.Map"%>
<%@page import="udea.drai.intercambio.dto.Homologacion"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.HashMap"%>
<%@page import="udea.drai.intercambio.dao.HomologacionDAO"%>
<%@page import="udea.drai.intercambio.dto.InfoCompromiso"%>
<%@page import="udea.drai.intercambio.dto.Intercambio"%>
<%@page import="udea.drai.intercambio.dao.IntercambioDAO"%>
<%@page import="udea.drai.intercambio.dao.SemestreDAO"%>
<%@page import="udea.drai.intercambio.dto.Persona"%>



<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
     
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>
    #watermark{
   width: 100px;
   position: relative;
   left: 0;
   margin-left: 5% ;
   background-image: url(../images/fondo2.png);
   background-repeat: repeat-y;
   background-position-y: 10%;
}
#image{
   opacity: .3;
  -moz-opacity: .3;
   width: 100px;
   height: 100px;
   position: relative;
   top: 300px;
   left: 0;

 
}
        </style>
        <title>Homologacion</title>
        <%
            HttpSession sesion = request.getSession();
            Persona p = (Persona) sesion.getAttribute("persona");
            int in = Integer.parseInt(request.getParameter("intercambio"));
            IntercambioDAO iDAO = new IntercambioDAO();
            HomologacionDAO hDAO = new HomologacionDAO();
            Intercambio inter = new Intercambio();
            inter.setCodigo(in);
            iDAO.cargarIntercambio(inter);
            if(!p.getCedula().equals(inter.getEstudiante())){
                %>
                <h2>No tiene permisos para acceder a este sitio!</h2>
                <a href="../PrincipalEstudiante.jsp">Volver a mi página principal</a>
                <%
                return;
            }
            //InfoCompromiso info = new InfoCompromiso();
            InfoCompromiso info= iDAO.compromiso(inter);
           
            %>
    </head>
    <body id="watermark" style="font-weight: 700" >
        
        <div id="image" > 
        </div>
        <div   id="registroCompr"  style="width: 900px; font-family: sans-serif;" align="center" >
        <table id="compromiso" style="width: 600px;">
            <tr>
                <th colspan="2" style="font-size: 15px; text-align: left;" > <span><h1>Universidad de Antioquia</h1>
                        <h2>Facultad de Ingeniería</h2>
                        <h2>Programa de Internacionalización</h2></span>
            <hr style="width: 100%; height: 10px; margin-left: 0px; background-color: #0b6c3c; border-radius: 30px;">
                </th>
          
            </tr>
         
            <tr>
                <td>Programa:</td><td><%=info.getPrograma() %> </td>
            </tr>
            <tr>
                <td>Nombre del Estudiante:</td><td><%=info.getEstudiante()%></td>
            </tr>
            <tr>
                <td>Identificación del Estudiante:</td><td><%=info.getIdEstudiante()%></td>
            </tr>
            <tr>
                <td>Email del Estudiante:</td><td><%=info.getEmail()%></td>
            </tr>
            <tr>
                <td>Universidad Origen</td><td><%=info.getInstOrigen() %></td>
            </tr>
            <tr>
                <td>Universidad Destino:</td><td><%=info.getInstDestino()%></td>
            </tr>
            <tr>
                <td>Semestre Inicio:</td><td><%=info.getSemInicio()%></td>
            </tr>
             <tr>
                <td>Semestre Fin:</td><td><%=info.getSemFin() %></td>
            </tr>
             <tr>
                <td>Fecha Viaje:</td><td><%=info.getIda() %></td>
            </tr>
              <tr>
                <td>Fecha Retorno:</td><td><%=info.getVuelta() %></td>
              </tr>
</table>
              <div style="width: 600px;">
              
              </div>
            <%
            
            SemestreDAO sDAO =new  SemestreDAO();
             HashMap hm = hDAO.cargarHomologaciones(in);
                Iterator it = hm.entrySet().iterator();
                String fechTemp="";
                int temp=0,sem=0;
                while(it.hasNext()){
                    Map.Entry mp = (Map.Entry) it.next();
                    Homologacion hh = new Homologacion();
                    hh.setCodigo(Integer.parseInt(mp.getKey().toString()));
                    hDAO.cargarHomologacion(hh);
                       if(hh.getActa() != temp && temp != 0){
                            %>
                            <tr><th colspan="2" style="font-size: 15px;">Acta – Resolución Comité de Carrera (Fecha: <%=hh.getFechaActa().toString() %> No: <%=hh.getActa() %> )
                                </th></tr>
                            
                            <%
                    }       
                    if(hh.getSemestre() != sem){
                        Semestre se = new Semestre();
                 
                            se.setCodigo(hh.getSemestre());
                            sDAO.cargarSemestre(se);
                            
                          
                            if(sem != 0){ %>
                                 </table>
                           <%  } %>
                           <br />
                            <table style="width: 800px;" align="center"  >
                            <tr>
                                <th colspan="2"  style="font-size: 25px; text-align: left;">Materias Homologables <%=se.getAno() %> - <%=se.getPeriodo() %>
                                <hr style="width: 100%; height: 10px; margin-left: 0px; background-color: #0b6c3c; border-radius: 30px;">
                                </th></tr>
                                <tr><th>Universidad Origen</th><th>  Universidad Destino</th></tr>
                                <tr>
                                <th style="font-size: 20px;">Codigo -  Nombre </th><th  style="font-size: 20px;">Codigo - Nombre  </th>
                            </tr>
                        <%
                    }
                    %>
                   
                    <tr>
                        <td align="center"><%=hh.getCodLocal() %> - <%=hh.getMateriaLocal() %></td>
                        <td align="center"><%=hh.getCodExtranjero()%> - <%=hh.getMateriaExtranjero()%></td>
                    </tr>
                    <%  
                    temp = hh.getActa();
                    fechTemp= hh.getFechaActa().toString();
                    sem = hh.getSemestre();
                }
            %>
                            <tr><th colspan="2">Acta – Resolución Comité de Carrera (Fecha: <%=fechTemp %> No: <%=temp %> )</th></tr>
                            </table>
            <br />
            <table style="width: 800px;" align="center" >
        <th colspan="2" style="font-size: 25px; text-align: left;" >Compromiso
        <hr style="width: 100%; height: 10px; margin-left: 0px; background-color: #0b6c3c; border-radius: 30px;">
        </th>
            <tr>
                <td colspan="2">
                    <div style="width: 800px; text-align: justify" >
        
                        <p>- Enviar en las fechas estipuladas por el comité de Internacionalización las notas de los 

                        cursos asistidos en la Universidad de Destino. 

                        Basado en Circular XX al inicio del semestre XX
                        </p>

                        <p>- Informar oportunamente (5 días calendario de sucedido el evento) cualquier novedad 

                            ocurrida, referente a los cursos matriculados en la Universidad e destino, tales como 

                            cancelaciones, perdidas de materias, entre otras.
                        </p>
                        <p>- Definir cada semestre claramente las materias que se van a matricular en la 

                            Universidad Origen, basado en posibles cambios en la oferta de materias en la 

                            Universidad de Destino (según fechas establecidas en el calendario académico de 

                            la Facultad de Ingeniería de la Universidad de Antioquia 
                        </p>
                        <p>– Será responsabilidad el estudiante estar pendiente de dichas fechas y comunicación). El no envío de dicha 

                            información puede ocasionar que el estudiante quede como desertor de la Universidad 

                            de Antioquia y en ningún motivo será responsabilidad del departamento o del 

                            coordinador de Internacionalización tal novedad. 
                        </p>

                        <p>- Es responsabilidad del coordinar de Internacionalización dar respuesta en 5 días 

                            hábiles a los comentarios, dudas y sugerencias de los estudiantes en pasantía o doble 

                            titulación. En algunos casos dichas respuestas pueden tomar más tiempo en caso que 

                            las consultas deban esperar reuniones del comité de carrera o consejo de Facultad. 
                        </p>

                        <p>- Es responsabilidad del coordinador de Internacionalización matricular oportunamente 

                            los estudiantes que envíen a tiempo (Mínimo 2 días antes del cierre de matrículas en 

                            el sistema MARES) la oferta de matrículas e igualmente ingresar las notas al sistema 

                            mares en los plazos establecidos, siempre y cuando el estudiante envíe dichas notas 

                            en los plazos estipulados para ello. 
                        </p>
                        <br />
                         Firmas
                    </div>
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                        Por el Departamento 
                        <br />
                        <br />
                        <br />
                </td>
                <td>
                        El Estudiante
                        <br />
                        <br />
                        <br />
                </td>
            </tr>
            <tr>
                <td>
                    ________________________
                </td>
                <td>
                    ________________________
                </td>
                
            </tr>
            <tr>
                <td>
                        Mario A. Muñoz G
                </td>
                <td>
                    <%=info.getEstudiante() %>
                </td>
            </tr>
            <tr>
                <td>
                    C.C.
                </td>
                <td>
                    C.C.
                </td>
            </tr>
            <tr>
                <td>
                     Coordinador Internacionalización
                </td>
                <td>
                    Estudiante
                </td>
            </tr>
        </table>
                <input type="button" class="button" id="imprimir" name="imprimir" value="Imprimir" onclick="document.getElementById('imprimir').style.visibility= 'hidden'; window.print(); document.getElementById('imprimir').style.visibility= 'visible';" />
         
        
    </div>

    </body>
    
</html>
