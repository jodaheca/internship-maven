<%-- 
    Document   : EnviarMensaje
    Created on : 02-dic-2014, 15:46:49
    Author     : Usuario
--%>

<%@page import="udea.drai.intercambio.dto.Persona"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form method="post" accept-charset="UTF-8" action="MensajeServlet"  style="width: auto;">
            <table>
                <input type="hidden" name="programa" value="<%=persona.getPrograma()%>" />
                <tr><td>Asunto:</td><td><input type="text" class="form-control" name="asunto" /></td>
                <tr><td><br/></td></tr>
                <tr><td>Mensaje: </td><td>
                        <textarea rows="10" cols="50" class="form-control" name="mensaje" style="resize: none;"  ></textarea></td>
                  <tr><td><br/></td></tr>
                <tr><td></td><td  style="margin-left: auto; margin-right: auto;"><input type="submit" class="btn btn-primary "  value="Enviar mensaje" /></td></tr>
                 <tr><td><br/></td></tr>
            </table>
        </form>
    </body>
</html>
