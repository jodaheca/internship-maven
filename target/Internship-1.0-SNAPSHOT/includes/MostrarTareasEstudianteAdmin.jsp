<%-- 
    Document   : MostrarTareasEstudianteAdmin
    Created on : 14-ago-2014, 9:20:55
    Author     : Usuario
--%>

<%@page import="udea.drai.intercambio.dto.RevisionCompleta"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%     
    List tareas=(List) request.getAttribute("revisiones");
    String modificacion=(String)request.getAttribute("modificacion");
    RevisionCompleta sacar=(RevisionCompleta)tareas.get(0);
    String nombreEstudiante=sacar.getNombreEstudiante();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>Revisar Tareas</title>
    </head>
   <body>
        <div class="container">
                        <h1 class="text-center login-title"> Revisar Tareas</h1>
                        <h2 class="text-center "><%=nombreEstudiante%></h2>
                         <div class=" account-wall">
                        <br><br>
                        <% 
                            if(tareas==null || tareas.isEmpty()==true){
                                %>
                                <div class="col-sm-6 col-md-4 col-md-offset-4">   
                                    <p class="bg-info">No hay tareas registradas en este Intercambio</p>
                                 </div>
                                 <%
                            }else{
                            if(modificacion=="true"){
                                %>
                                <div class="col-sm-6 col-md-4 col-md-offset-4">
                                <br>
                                 <p class="bg-success">Modificación registrada exitosamente.</p>
                                </div>
                        <%
                            }    
                        %> 
                    <table class="table table-bordered">
                        <thead>
                            <tr> 
                                <th>Nombre</th> 
                                <th>Estado</th>
                                <th>comentrario</th>
                            </tr>
                        </thead> 
                        <tbody>
                            <%
                              for(int i=0;i<tareas.size();i++){
                                RevisionCompleta nueva=(RevisionCompleta) tareas.get(i);
                                int codigo=nueva.getCodigo();
                                String estudiante=nueva.getEstudiante();
                                int intercambio= nueva.getIntercambio();
                                String nombre= nueva.getNombre();
                                String  comentario = nueva.getComentario();
                                String estado=nueva.getEstado();
                                char[] es=estado.toCharArray();
                                char esta=es[0];
                            %> 
                            <tr class="active" > 
                                <td><%=nombre%></td>
                                <%
                                if(esta=='R'){
                                    %>
                                <td>Revisado</td> 
                                <%
                                }else if(esta=='P'){
                                
                                %>
                                <td>Pendiente</td>
                                <%    
                                }else if (esta=='N'){
                                    %>
                                <td>No Aplica</td>
                                <%
                                }
                                %>
                                <td><%=comentario%></td> 

                            </tr>
                            <%    
                            }
                        }
                            %> 
                        </tbody>
                    </table>
                </div>
            </div>
    </body>
</html>