<%-- 
    Document   : MostrarEstudiante
    Created on : 16-oct-2014, 9:37:36
    Author     : Usuario
--%>

<%@page import="udea.drai.intercambio.dto.Persona"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%     
    List estudiante=(List) request.getAttribute("estudianteCedula");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Estudiante</title>
    </head>
    <body>
        <div class="container">
                        <h1 class="text-center login-title"> Información</h1>
                                <div class="account-wall">
                        <br><br>
                        <% 
                            if(estudiante==null){
                                %>
                                <div class="col-sm-6 col-md-4 col-md-offset-4">      
                                    <p class="bg-info ">Ocurrio un erro al recuperar la información, intente Luego </p>
                                </div>
                         <%
                            } else  if(estudiante.size()==0){
                                %>
                                <div class="col-sm-6 col-md-4 col-md-offset-4">        
                                    <p class="bg-info">No existen Estudiante con la identificación ingresada </p>
                                    </div>
                            <%
                            }else{
                        %>
                    <table class="table table-bordered">
                        <thead>
                            <tr> 
                                <th>Cedula</th> 
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Programa</th>
                                <th>Email</th>
                            </tr>
                        </thead> 
                        <tbody>
                            <%
                              for(int i=0;i<estudiante.size();i++){
                                Persona nueva=(Persona) estudiante.get(i);
                                String cedula=nueva.getCedula();
                                String nombre=nueva.getNombre();
                                String apellido = nueva.getApellido();
                                String programa=nueva.getPrograma();
                                String email=nueva.getEmail();

                            %>
                             <tr class="active">
                                <td><%=cedula%></td> 
                                <td><%=nombre%></td>
                                <td><%=apellido%></td>
                                <td><%=programa%></td>
                                <td><%=email%></td>
                            </tr>

                            <%    
                            }  
                            %> 
                        </tbody>
                    </table>
                    <%
                            }
                    %>    
                </div>
            </div>
    </body>
</html>

