<%-- 
    Document   : EliminarProfesor
    Created on : 09-sep-2014, 14:19:40
    Author     : Usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<% String id = request.getParameter("id");%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Eliminar Profesor</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <br><br>
                    <h1 class="text-center login-title">Eliminar Profesor</h1>
                        <div class="account-wall">
                                <br>
                                <form name="eliminar" class="form-signin" accept-charset="UTF-8" action="EliminarProfesorServlet" method="post">
                                    <input type="text" class="form-control" placeholder="Identificación del Profesor" value="<% if(id != null){ %><%=id%> <% } %>" name="id" id="id" required>
                                     <br>  
                                  <button class="btn btn-lg btn-primary btn-block" type="submit">
                                     Eliminar Profesor</button>
                               </form>
                             </div>
                    </div>
                </div>
            </div>
    </body>
</html>