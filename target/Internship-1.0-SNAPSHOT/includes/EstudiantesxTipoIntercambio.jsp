<%-- 
    Document   : EstudiantesxTipoIntercambio
    Created on : 15-oct-2014, 15:49:16
    Author     : Usuario
--%>

<%@page import="udea.drai.intercambio.dto.TipoIntercambio"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<TipoIntercambio> tiposIntercambio = (List) request.getAttribute("tiposIntercambio");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Estudiantes x T.Intercambio</title>
    </head>
    <body>

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <h2 class="text-center login-title">Estudiantes x T.Intercambio</h2>
                    <div class="account-wall">
                        <br>
                        <form name="formulario" class="form-signin" accept-charset="UTF-8" action="RecuperarEstudiantesxTIntercambio" method="POST">
                            <select class="form-control" name="tipoIntercambio" id="tipoIntercambio"  required="true">
                                <option value=""> Seleccione Tipo Intercambio</option>
                                <%
                                    for (int i = 0; i < tiposIntercambio.size(); i++) {
                                        TipoIntercambio tipo = (TipoIntercambio) tiposIntercambio.get(i);
                                        String codigo = tipo.getCodigo();
                                        String nombre = tipo.getNombre();

                                %>
                                <option value= <%=codigo%> >
                                    <%=nombre%>
                                </option>
                                <%    }%>
                            </select>
                            <br>
                            <button class="btn btn-lg btn-primary btn-block" type="submit">
                                Revisar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>
