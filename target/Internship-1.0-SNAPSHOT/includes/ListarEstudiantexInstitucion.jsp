<%-- 
    Document   : ListarEstudiantexInstitucion
    Created on : 06-oct-2014, 15:49:51
    Author     : Usuario
--%>

<%@page import="java.sql.Date"%>
<%@page import="udea.drai.intercambio.dto.EstudianteIntercambio2"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%     
    List datos=(List) request.getAttribute("estudianteInstitucion");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>EstudiantesxInstitucion</title>
    </head>
    <body>
        <div class="container">
                        <h1 class="text-center login-title"> EstudiantesxInstitución</h1>
                                <div class="account-wall">
                        <br><br>
                     <% 
                            if(datos==null){
                                %>
                                <div class="col-sm-6 col-md-4 col-md-offset-4">      
                                 <p class="bg-info">Ocurrio un erro al recuperar la información de los estudiantes, intente Luego </p>
                                 </div>
                            <%
                            } else  if(datos.size()==0){
                                %>
                                 <div class="col-sm-6 col-md-4 col-md-offset-4">    
                                     <p class="bg-info">No existen estudiantes en esta institución </p>
                                 </div>
                        <%
                            }else{
                        %>   
                    <table class="table table-bordered">
                        <thead>
                            <tr> 
                                <th>Cedula</th> 
                                <th>Nombre</th> 
                                <th>Apellido</th> 
                                <th>Programa</th>
                                <th>Tipo Intercambio</th>
                                <th>Fecha de Ida</th>
                                <th>Fecha Regreso</th>
                                <th>Semestre Inicio</th>
                                <th>Semestre Fin</th>
                            </tr>
                        </thead> 
                        <tbody>
                            <%
                              for(int i=0;i<datos.size();i++){
                                EstudianteIntercambio2 nueva=(EstudianteIntercambio2) datos.get(i);
                                String nombre=nueva.getNombre();
                                String cedula=nueva.getCedula();
                                String programa=nueva.getPrograma();
                                Date fechaIda = nueva.getFechaIda();
                                Date fechaRegreso = nueva.getFechaRegreso();
                                String semesInicio = nueva.getSemestreInicio();
                                String semesFin = nueva.getSemestreFin();
                                String tipoInter = nueva.getTipoIntercambio();
                                String apellido = nueva.getApellido();
                                
                                

                            %>
                             <tr class="active">
                                <td><% if(cedula != null){ %><%=cedula%><% } %></td> 
                                <td><% if(nombre != null){ %><%=nombre%><% } %></td> 
                                <td><% if(apellido != null){ %><%=apellido%><% } %></td> 
                                <td><% if(programa != null){ %><%=programa%><% } %></td> 
                                <td><% if(tipoInter != null){ %><%=tipoInter%><% } %></td>
                                <td><% if(fechaIda != null){ %><%=fechaIda%><% } %></td>
                                <td><% if(fechaRegreso != null){ %><%=fechaRegreso%><% } %></td>
                                <td><% if(semesInicio != null){ %><%=semesInicio%><% } %></td>
                                <td><% if(semesFin != null){ %><%=semesFin%><% } %></td>
                            </tr>

                            <%    
                            }  
                            %> 
                        </tbody>
                    </table>
                        <%
                            }
                        %>
                </div>
            </div>
    </body>
</html>
