<%-- 
    Document   : ListarEstudiantes
    Created on : 06-ago-2014, 9:04:11
    Author     : Joaquin David Hernandez <jdavidhc94@gmail.com>
--%>
<%@page import="udea.drai.intercambio.dto.Persona"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List estudiantes = (List) request.getAttribute("estudiantes");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista de Estudiantes</title>
    </head>
    <body>
        <div class="container">
            <h1 class="text-center login-title"> Estudiantes Registrados</h1>
            <div class="account-wall">
                <br><br>
                <%
                    if (estudiantes == null) {
                %>
                <div class="col-sm-6 col-md-4 col-md-offset-4">      
                    <p class="bg-info ">Ocurrio un erro al recuperar los Estudiantes, intente Luego </p>
                </div>
                <%
                } else if (estudiantes.size() == 0) {
                %>
                <div class="col-sm-6 col-md-4 col-md-offset-4">        
                    <p class="bg-info">No existen Estudiantes registrados </p>
                </div>
                <%
                } else {
                %>
                <table class="table table-bordered">
                    <thead>
                        <tr> 
                            <th>Cedula</th> 
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Programa</th>
                            <th>Email</th>
                            <th>Intercambio</th>
                            <th>Operaciones</th>
                        </tr>
                    </thead> 
                    <tbody>
                        <%
                            for (int i = 0; i < estudiantes.size(); i++) {
                                Persona nueva = (Persona) estudiantes.get(i);
                                String cedula = nueva.getCedula();
                                String nombre = nueva.getNombre();
                                String apellido = nueva.getApellido();
                                String programa = nueva.getPrograma();
                                String email = nueva.getEmail();
                                String intercambio = nueva.getNombreIntercambio();

                        %>
                        <tr class="active">
                            <td><% if (cedula != null) {%> <%=cedula%> <% } %></td> 
                            <td><% if (nombre != null) {%><%=nombre%><% } %></td>
                            <td><% if (apellido != null) {%><%=apellido%><% } %></td>
                            <td><% if (programa != null) {%><%=programa%><% } %></td>
                            <td><% if (email != null) {%><%=email%><% } %></td>
                            <td><% if (intercambio != null) {%><%=intercambio%><% }%></td>
                            <td><div class="dropdown">
                                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                                        Seleccione
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
<!--                                        <li> <form action="Principal.jsp?modificarEstudiante" accept-charset="UTF-8" method="POST">
                                                <input type="hidden" id="identificacion" name="identificacion" value="<%=cedula%>">
                                                <input type="hidden" id="nombre" name="nombre" value="<%=nombre%>">
                                                <input type="hidden"  id="apellido" name="apellido" value="<%=apellido%>">
                                                <input type="hidden" id="email" name="email" value="<%=email%>">
                                                <button class="btn btn-xs btn-link" type="submit">
                                                    Modificar información </button>
                                            </form>
                                        </li>-->
                                        <li> <form action="MostrarRequisitosServletV2" method="POST">
                                                <input type="hidden" id="identificacion" name="identificacion" value="<%=cedula%>">
                                                <button class="btn btn-xs btn-link" type="submit">
                                                    Revisar Tareas </button>
                                            </form>
                                        </li>
                                        <li> <form action="MostrarMateriasServletV2" method="POST">
                                                <input type="hidden" id="identificacion" name="identificacion" value="<%=cedula%>">
                                                <button class="btn btn-xs btn-link" type="submit">
                                                    Revisar Materias </button>
                                            </form>
                                        </li>
                                        <li role="presentation">
                                            <form action="Principal.jsp?EnviarMensaje" method="POST">
                                                <button class="btn btn-xs btn-link" type="submit">
                                                    Enviar mensaje </button>
                                            </form>
                                        </li>
                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Generar Reporte</a></li>
                                    </ul>
                                </div></td>
                        </tr>

                        <%
                            }
                        %> 
                    </tbody>
                </table>
                <%
                    }
                %>    
            </div>
        </div>
    </body>
</html>

