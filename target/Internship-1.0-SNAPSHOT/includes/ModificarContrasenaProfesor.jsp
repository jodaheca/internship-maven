<%-- 
    Document   : ModificarContrasenaProfesor
    Created on : 06-oct-2014, 8:32:59
    Author     : Usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Modificar Contraseña</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <br>
                    <h1 class="text-center login-title">Modificar Contraseña</h1>
                        <div class="account-wall">
                                <br>
                                <form name="modificarContrasena" class="form-signin" accept-charset="UTF-8" action="ModificarContrasenaProfesor" method="post" >
                                    <input type="password" class="form-control" placeholder="Contraseña Actual" name="contraActual" id="contraActual" required>
                                     <br> 
                                     <input type="password" class="form-control" placeholder="Nueva Contraseña " name="nuevaContrasena" onkeyup="ComprobarAcentos(this);" id="nuevaContrasena" required>
                                     <br>
                                     <input type="password" class="form-control" placeholder="Confirmar Contraseña " name="confirmaContrasena" onkeyup="ComprobarAcentos(this);" id="confirmaContrasena" required>
                                     <br>
                                  <button class="btn btn-lg btn-primary btn-block" type="submit">
                                     Modificar Contraseña</button>
                               </form>
                             </div>
                    </div>
                </div>
            </div>
        
    </body>
</html>

