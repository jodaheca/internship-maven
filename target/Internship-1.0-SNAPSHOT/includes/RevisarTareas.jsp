<%-- 
    Document   : RevisarTareas
    Created on : 30-jul-2014, 9:01:58
    Author     : Usuario
--%>

<%@page import="java.util.List"%>
<%@page import="udea.drai.intercambio.dto.Persona"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
 List<Persona> estudiantes=(List)request.getAttribute("estudiantes");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Revisar Tareas</title>
    </head>
  
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <h1 class="text-center login-title">Revisar Tareas</h1>
                        <div class="account-wall">
                                <br>
                                <form name="formulario" class="form-signin" accept-charset="UTF-8" action="MostrarRequisitosServlet" method="POST">
                                    <select class="form-control" name="estudiante" id="estudiante" onchange="obtenerIntercambios()" required="true">
                                                <option value=""> Seleccione estudiante</option>
                                                <%
                                                    for(int i=0; i<estudiantes.size();i++){
                                                        Persona estudiante=(Persona) estudiantes.get(i);
                                                        String cedula=estudiante.getCedula();
                                                        String nombre=estudiante.getNombre();
                                                        String apellido = estudiante.getApellido();
                                                        String mostrar= cedula+"  " + nombre+" "+apellido;
                                                %>
                                                <option value= <%=cedula%> onchange="obtenerIntercambios()">
                                                               <%=mostrar%>
                                                </option>
                                                <%
                                                 }%>
                                        </select>
                                     <br>
                                     <select id="intercambios" class="form-control" name="intercambio"  required="true">
                                            <option value="">
                                            Seleccionar Intercambio
                                     </option>
                                     </select>
                                     <br>
                                  <button class="btn btn-lg btn-primary btn-block" type="submit">
                                     Revisar</button>
                               </form>
                             </div>
                    </div>
                </div>
            </div>

    </body>
</html>
