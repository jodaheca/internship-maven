<%-- 
    Document   : Principal
    Created on : 09-jun-2014, 10:14:40
    Author     : Joaquin David Hernández Cárdenas
    Comentario : Esta es la vista principal del Profesor, desde aqui podrá realizar todas las operaciones que está autorizado
    a realizar.
--%>


<%@page import="udea.drai.intercambio.dao.TipoIntercambioDAO"%>
<%@page import="udea.drai.intercambio.dao.SemestreDAO"%>
<%@page import="udea.drai.intercambio.dao.EstudianteDAO"%>
<%@page import="udea.drai.intercambio.dao.IntercambioDAO"%>
<%@page import="udea.drai.intercambio.dao.ProgramaDAO"%>
<%@page import="udea.drai.intercambio.dto.Persona"%>
<%@page import="java.util.List"%>
<%@page import="udea.drai.intercambio.dao.InstitucionDAO"%>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Persona persona = null;
    String identificacion = "";
     response.setContentType("text/html;charset=UTF-8");
     request.setCharacterEncoding("UTF-8");
    try {
        HttpSession sesionOk = request.getSession();
        String login = "LOGIN";
        System.out.println("LOGIN ANTES: " + login);
        login = (String) sesionOk.getAttribute("login");

        String tipoUsuario = (String) sesionOk.getAttribute("tipoUsuario");
        persona = (Persona) sesionOk.getAttribute("persona");
        identificacion = persona.getCedula();
        System.out.println("El estado del login es= " + login);
        System.out.println("El tipo de usuario es= " + tipoUsuario);
        if (login == null || tipoUsuario != "2") {
            System.out.println("LOGIN DESPUES: " + login);
            //redireccionamos a la pagina del login
%>
<jsp:forward page="index.jsp" />

<% }
} catch (Exception e) {
    System.out.println("Error leyendo el archivo Principal de los profesores " + e.toString());
%>
<jsp:forward page="index.jsp" />

<%
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Tangerine">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="css/logo-nav.css" rel="stylesheet" type="text/css" />
        <link href="css/bootstrap-checkbox.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <script src="js/jquery.js"></script>
        <script src="js/main.js"></script>
       <script src="js/diseno.js"></script>
        <script src="js/CargarIntercambioEstudiante.js"></script>
        <script src="js/ListarEstudiatesProfesor.js"></script>
        <script src="js/vendor/bootstrap.js"></script>
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/vendor/jquery-1.10.1.min.js"></script>
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <title>Principal Profesores</title>
</head>

<body style="margin-top: 0px; ">
    <div align="right">
        <%// out.println("Usuario: "+persona.getNombre());%>
        <!--            <a href="./logout"><img src="imagenes/out.png" alt="" width="15" height="20" style="border:none" /></a>-->
    </div>
    <!--Barra de Navegacion-->
    <div class="container" align="center" style="margin-bottom: 50px;" >
        <div class="row">
            <nav class="navbar navbar-default" style="width: 78%; font-size: 11px;">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Cambiar Navegacion</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="Principal.jsp?ListarEstudiantes" class="navbar-brand">Principal</a></div>

                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav">
                        <!--                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Semestres<b class="caret"></b></a>
                                                    <ul class="dropdown-menu">
                                                        <li><a href='Principal.jsp?agregarSemestre' >Agregar</a></li>
                                                        <li><a href="Principal.jsp?eliminarSemestre" >Eliminar</a></li>
                                                        <li><a href="Principal.jsp?ListarSemestres">Listar Semestres</a></li>
                                                    </ul>
                                                </li>-->
                        <!--                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Instituciones<b class="caret"></b></a>
                                                    <ul class="dropdown-menu">
                                                        <li><a href='Principal.jsp?agregarInstitucion' >Agregar</a></li>
                                                        <li><a href="Principal.jsp?eliminarInstitucion" >Eliminar</a></li>
                                                        <li><a href="Principal.jsp?ListarInstituciones">Listar Instituciones</a></li>
                                                    </ul>
                                                </li>-->
                        <!--                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Programas<b class="caret"></b></a>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="Principal.jsp?agregarPrograma">Agregar</a></li>
                                                        <li><a href="Principal.jsp?eliminarPrograma">Eliminar</a></li>
                                                        <li><a href="Principal.jsp?ListarProgramas">Listar Programas</a></li>
                                                    </ul>
                                                </li>-->

                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Estudiantes<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="Principal.jsp?agregarEstudiante">Ingresar estudiante</a></li>
                                <li><a href="Principal.jsp?revisarComprobante">Revisar Comprobante</a></li>
                                <li><a href="Principal.jsp?revisarTareas">Revisar Tareas</a></li>
                                <li><a href="Principal.jsp?revisarMaterias">Revisar Materias</a></li>
                                <li><a href="Principal.jsp?ListarEstudiantes">Listar Estudiantes</a></li>
                                <li><a href="Principal.jsp?EnviarMensaje">Enviar Mensaje</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Personal<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="Principal.jsp?modificarContrasena">Modificar Contraseña</a>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Consultas<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="Principal.jsp?estudiantexInstitucion">Estudiantes x Institución</a></li>
                                <li><a href="Principal.jsp?estudiantexPrograma">Estudiantes x Programa</a></li>
                                <li><a href="Principal.jsp?estudiantexTipoIntercambio">Estudiantes x T.Intercambio</a></li>
                                <li><a href="Principal.jsp?buscarEstudiante">Información Estudiante</a></li>
                            </ul>
                        </li>
                        <!--                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Tipos Intercambios<b class="caret"></b></a>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="Principal.jsp?agregarIntercambio">Agregar Tipo Intercambio</a></li>
                                                        <li><a href="Principal.jsp?eliminarIntercambio">Eliminar Tipo Intercambio</a></li>
                                                        <li><a href="Principal.jsp?ListarTipoIntercambios">Listar Tipo Intercambios</a></li>
                                                    </ul>
                                                </li>-->
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="<%= request.getContextPath() + "/LogOutServlet"%>"><%=persona.getUsuario()%> - Cerrar Sesión</a></li>

                    </ul>
                </div>
            </nav>

            <%
                if (!(request.getParameter("agregarInstitucion") == null)) {
            %>
            <%@include file="includes/RegistrarInstitucion.jsp" %>
            <%
            } else if (!(request.getParameter("eliminarInstitucion") == null)) {
            %>
            <%@include  file="includes/EliminarInstitucion.jsp" %>
            <%
            } else if (request.getParameter("ListarInstituciones") != null) {
                InstitucionDAO nuevo = new InstitucionDAO();

                List insti = nuevo.getInstituciones();
                request.setAttribute("instituciones", insti);
            %>
            <%@include  file="includes/ListarInstituciones.jsp" %>
            <% } else if (!(request.getParameter("EICorrectamente") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-success">La institución se eliminó correctamente</p>
            </div>
            <% } else if (!(request.getParameter("EIError") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">Ocurrio un error al intentar eliminar la institución</p>
            </div>
            <% } else if (!(request.getParameter("IICorrectamente") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-success">La institución se agregó correctamente</p>
            </div>
            <% } else if (!(request.getParameter("IIError") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">La institución no se pudo agregar</p>
            </div>
            <% } else if (!(request.getParameter("agregarPrograma") == null)) {
            %>
            <%@include  file="includes/RegistrarPrograma.jsp" %>
            <% } else if (!(request.getParameter("IPCorrectamente") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-success">El programa se agregó correctamente</p>
            </div>
            <% } else if (!(request.getParameter("IPError") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">El programa no se pudo Ingresar</p>
            </div>
            <% } else if (!(request.getParameter("eliminarPrograma") == null)) {
            %>
            <%@include  file="includes/EliminarPrograma.jsp" %>
            <% } else if (!(request.getParameter("PECorrectamente") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-success">El programa se eliminó correctamente</p>
            </div>
            <% } else if (!(request.getParameter("PEError") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">El programa no se pudo eliminar</p>
            </div>
            <% } else if (request.getParameter("ListarProgramas") != null) {
                ProgramaDAO nuevo = new ProgramaDAO();
                List prog = nuevo.getProgramas(identificacion);
                request.setAttribute("programas", prog);
            %>
            <%@include  file="includes/ListarProgramas.jsp" %>
            <% } else if (!(request.getParameter("agregarIntercambio") == null)) {
            %>
            <%@include  file="includes/RegistrarIntercambio.jsp" %>
            <% } else if (!(request.getParameter("INTCorrectamente") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-success">El intercambio se agregó correctamente</p>
            </div>
            <% } else if (!(request.getParameter("INTError") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">El Intercambio no se pudo Agregar</p>
            </div>
            <% } else if (!(request.getParameter("eliminarIntercambio") == null)) {
            %>
            <%@include  file="includes/EliminarTipoIntercambio.jsp" %>
            <% } else if (!(request.getParameter("INECorrectamente") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-success">El intercambio se eliminó correctamente</p>
            </div>
            <% } else if (!(request.getParameter("INError") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">El Intercambio no se pudo Eliminar</p>
            </div>
            <% } else if (request.getParameter("ListarTipoIntercambios") != null) {
                IntercambioDAO nuevo = new IntercambioDAO();
                List inter = nuevo.getTipoIntercambios();
                request.setAttribute("intercambios", inter);
            %>
            <%@include  file="includes/ListarTipoIntercambios.jsp" %>
            <% } else if (!(request.getParameter("agregarEstudiante") == null)) {
                ProgramaDAO nuevo = new ProgramaDAO();
                List progr = nuevo.getProgramas(identificacion);
                request.setAttribute("programas", progr);
            %>
            <%@include  file="includes/IngresarEstudiante.jsp" %>
            <% } else if (!(request.getParameter("EstudianteCorrectamente") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-success">Estudiante Ingresado Correctamente</p>
            </div>
            <% } else if (!(request.getParameter("EstudianteError") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">Ocurrio un error al ingresar el estudiante, intente luego</p>
            </div>
            <% } else if (!(request.getParameter("faltanCampos") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">Ingrese Correctamente todos los campos del formulario, solo se aceptan números</p>
            </div>
            <% } else if (!(request.getParameter("revisarMaterias") == null)) {
                EstudianteDAO managerEstudiante = new EstudianteDAO();
                List student = managerEstudiante.getEstudianteXProfesor2(identificacion);
                request.setAttribute("estudiantes", student);
            %>
            <%@include  file="includes/RevisarMateria.jsp" %>
            <% } else if (!(request.getParameter("homologaciones") == null)) {
                //List homologaciones=(List)request.getAttribute("homologaciones");
                HttpSession sesionOk = request.getSession();
                List homologaciones = (List) sesionOk.getAttribute("materias");
                String modifi = (String) sesionOk.getAttribute("modificacion");
                if (modifi == "true") {
                    request.setAttribute("modificacion", "true");
                } else {
                    request.setAttribute("modificacion", "false");
                }
                request.setAttribute("materias", homologaciones);
            %>
            <%@include  file="includes/RevisarMateriasEstudiante.jsp" %>
            <% } else if (!(request.getParameter("homologacionesV2") == null)) {
                HttpSession sesionOk = request.getSession();
                List homologaciones = (List) sesionOk.getAttribute("materias");
                String modifi = (String) sesionOk.getAttribute("modificacion");
                String nEstudiante = (String) sesionOk.getAttribute("nombreEstudiante");
                String iIntercambio = (String) sesionOk.getAttribute("infoIntercambio");
                String dEstudiante = (String) sesionOk.getAttribute("idEstudiante");
                String iVigente = (String) sesionOk.getAttribute("interVigente");
                
                if (modifi == "true") {
                    request.setAttribute("modificacion", "true");
                } else {
                    request.setAttribute("modificacion", "false");
                }
                
                 if (iVigente == "true") {
                    request.setAttribute("inVigente", "true");
                } else {
                    request.setAttribute("inVigente", "false");
                }
                request.setAttribute("materias", homologaciones);
                request.setAttribute("idEstudiante", dEstudiante);
                request.setAttribute("nEstudiante", nEstudiante);
                request.setAttribute("ifIntercambio", iIntercambio);
            %>
            <%@include  file="includes/RevisarMateriasEstudianteV2.jsp" %>
            <% } else if (!(request.getParameter("agregarSemestre") == null)) {
            %>
            <%@include  file="includes/RegistrarSemestre.jsp" %>
            <% } else if (!(request.getParameter("SemestreCorrectamente") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-success">Semestre Ingresado Correctamente</p>
            </div>
            <% } else if (!(request.getParameter("SemestreError") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">El semestre no pudo ser ingresado</p>
            </div>
            <% } else if (request.getParameter("ListarSemestres") != null) {
                SemestreDAO managerSemestre = new SemestreDAO();

                List semest = managerSemestre.obtenerSemestres();
                request.setAttribute("semestres", semest);
            %>
            <%@include  file="includes/ListarSemestres.jsp" %>
            <% } else if (!(request.getParameter("eliminarSemestre") == null)) {
            %>
            <%@include  file="includes/EliminarSemestre.jsp" %>
            <% } else if (!(request.getParameter("ISECorrectamente") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-success">Semestre Eliminado Correctamente</p>
            </div>
            <% } else if (!(request.getParameter("PEError") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">El semestre no pudo Eliminar</p>
            </div>
            <% } else if (!(request.getParameter("revisarTareas") == null)) {

                EstudianteDAO managerEstudiante = new EstudianteDAO();
                List students = managerEstudiante.getEstudianteXProfesor2(identificacion);
                request.setAttribute("estudiantes", students);
            %>

            <%@include  file="includes/RevisarTareas.jsp" %>
            <% } else if (!(request.getParameter("mostrarRevisiones") == null)) {
                HttpSession sesionOk = request.getSession();
                List revisiones = (List) sesionOk.getAttribute("revisiones");
                String modificaciones = (String) sesionOk.getAttribute("modificaRevisiones");
                if (modificaciones == "true") {
                    request.setAttribute("modificacion", "true");
                } else {
                    request.setAttribute("modificacion", "false");
                }
                request.setAttribute("revisiones", revisiones);
            %>

            <%@include  file="includes/MostrarTareasEstudiante.jsp" %>
            <% }else if (!(request.getParameter("mostrarRevisionesv2") == null)) {
                HttpSession sesionOk = request.getSession();
                List revisiones = (List) sesionOk.getAttribute("revisiones");
                String info = (String) sesionOk.getAttribute("infoIntercambio");
                String modificaciones = (String) sesionOk.getAttribute("modificaRevisiones");
                String vigente = (String) sesionOk.getAttribute("interVigente");
               
                if (modificaciones == "true") {
                    request.setAttribute("modificacion", "true");
                } else {
                    request.setAttribute("modificacion", "false");
                }
                
                 if (vigente == "true") {
                    request.setAttribute("interVigente", "true");
                } else {
                    request.setAttribute("interVigente", "false");
                }
                 request.setAttribute("infoIntercambio", info); 
                request.setAttribute("revisiones", revisiones);
                
            %>

            <%@include  file="includes/MostrarTareasEstudianteV2.jsp" %>
            <% } else if (!(request.getParameter("ErrorMostrarTareas") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">Lo sentimos ocurrió un error sacando la información.</p>
            </div>
            <% } else if (!(request.getParameter("NoTieneIntercambio") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">El estudiante no tiene intercambio.</p>
               
            <form action="Principal.jsp?ListarEstudiantes" method="POST">
                <button class="btn btn-lg btn-primary btn-warning" type="submit">
                    Atrás </button>
            </form>
            </div>
            <% } else if (!(request.getParameter("NoHayConexion") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">En este momento estamos en mantenimiento intente luego.</p>
               
            <form action="Principal.jsp?ListarEstudiantes" method="POST">
                <button class="btn btn-lg btn-primary btn-warning" type="submit">
                    Atrás </button>
            </form>
            </div>
            
            <% } else if (request.getParameter("ListarEstudiantes") != null) {
                EstudianteDAO managerEstudiante = new EstudianteDAO();

                List students = managerEstudiante.getEstudianteXProfesor(identificacion);
                request.setAttribute("estudiantes", students);
            %>
            <%@include  file="includes/ListarEstudiantes.jsp" %>
            <% } else if (!(request.getParameter("codPrograma") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">El codigo del programa debe ser un valor numérico</p>
            </div>
            <% } else if (!(request.getParameter("revisarComprobante") == null)) {
                EstudianteDAO managerEstudiante = new EstudianteDAO();
                List student = managerEstudiante.getEstudianteXProfesor2(identificacion);
                request.setAttribute("estudiantes", student);
            %>
            <%@include  file="includes/MostrarComprobante.jsp" %>
            <% } else if (!(request.getParameter("cedulaInvalida") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">La identificación del estudiante debe ser un valor numérico</p>
            </div>
            <% } else if (!(request.getParameter("estudianteExiste") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-info">Ya existe un estudiante con la identifcación ingresada.Verifique</p>
            </div>
            <% } else if (!(request.getParameter("usuarioAsignado") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-info">El usuario utilizado ya fué asignado a otra persona,debe asignar otro usuario para registrar el nuevo estudiante</p>
            </div>
            <% } else if (!(request.getParameter("modificarContrasena") == null)) {
            %>
            <%@include  file="includes/ModificarContrasenaProfesor.jsp" %>
            <% } else if (!(request.getParameter("contrasenaErronea") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">Ingrese correctamente la contraseña Actual</p>
            </div> 
            <%} else if (!(request.getParameter("nuevaContraNoCoincide") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">Las contraseñas ingresadas no coinciden</p>
            </div>
            <% } else if (!(request.getParameter("ErrorModificandoContrasena") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">Error modificando la Contraseña</p>
            </div>
            <% } else if (!(request.getParameter("ExitoModificandoContrasena") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-success">La contraseña se actualizó exitosamente</p>
            </div>
            <% } else if (!(request.getParameter("estudiantexInstitucion") == null)) {
                InstitucionDAO managerInstituciones = new InstitucionDAO();
                List insti = managerInstituciones.getInstituciones();
                request.setAttribute("instituciones", insti);
            %>
            <%@include  file="includes/EstudiantexInstitucion.jsp" %>
            <% }else if (!(request.getParameter("MostrarEstudiantesIntitucion") == null)) {
                HttpSession sesionOk = request.getSession();
                List estudianteInstitucion = (List) sesionOk.getAttribute("ListEstudianteIntercambio");
                
                request.setAttribute("estudianteInstitucion", estudianteInstitucion);
            %>

            <%@include  file="includes/ListarEstudiantexInstitucion.jsp" %>
            <% } else if (!(request.getParameter("ErrorEstudiantesxInstitucion") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">Ocurrió un error retornando la información, notifiquelo al administrador</p>
            </div>
            <% }else if (!(request.getParameter("estudiantexPrograma") == null)) {
                ProgramaDAO managerPrograma = new ProgramaDAO();
                List progra = managerPrograma.getProgramas(identificacion);
                request.setAttribute("programas", progra);
            %>
            <%@include  file="includes/EstudiantexPrograma.jsp" %>
            <% }else if (!(request.getParameter("ErrorEstudiantesxPrograma") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">Ocurrió un error retornando la información, notifiquelo al administrador</p>
            </div>
            <% }else if (!(request.getParameter("MostrarEstudiantesPrograma") == null)) {
                HttpSession sesionOk = request.getSession();
                List estudiantePrograma = (List) sesionOk.getAttribute("ListEstudiantePrograma");
                
                request.setAttribute("estudiantePrograma", estudiantePrograma);
            %>

            <%@include  file="includes/ListarEstudiantexPrograma.jsp" %>
            <% }else if (!(request.getParameter("estudiantexTipoIntercambio") == null)) {
               TipoIntercambioDAO managerTipo = new TipoIntercambioDAO();
               List tipos = managerTipo.obtenerTipoIntercambios();
                request.setAttribute("tiposIntercambio", tipos);
            %>

            <%@include  file="includes/EstudiantesxTipoIntercambio.jsp" %>
            <% }else if (!(request.getParameter("ErrorEstudiantesxTipo") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">Ocurrió un error retornando la información, notifiquelo al administrador</p>
            </div>
            <% }else if (!(request.getParameter("MostrarEstudiantesTipo") == null)) {
                HttpSession sesionOk = request.getSession();
                List estudianteTipo = (List) sesionOk.getAttribute("ListEstudianteTipo");
                
                request.setAttribute("estudianteTipo", estudianteTipo);
            %>

            <%@include  file="includes/ListarEstudiantexTipoIntermcambio.jsp" %>
            
            <% }else if (!(request.getParameter("buscarEstudiante") == null)) {
            %>

            <%@include  file="includes/BuscarEstudiante.jsp" %>
            <% }else if (!(request.getParameter("ErrorEstudiantesxCedula") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">Ocurrió un error retornando la información, notifiquelo al administrador</p>
            </div>
            <% }else if (!(request.getParameter("MostrarEstudiantesCedula") == null)) {
                HttpSession sesionOk = request.getSession();
                List estudianteCedula = (List) sesionOk.getAttribute("estudianteCedula");
                
                request.setAttribute("estudianteCedula", estudianteCedula);
            %>

            <%@include  file="includes/MostrarEstudiante.jsp" %>
            <% }else if (!(request.getParameter("NoExisteEstudiantesCedula") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-success">No existe estudiante con la identificación ingresada</p>
            </div>
            <% }else if (!(request.getParameter("semestreExiste") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">Ya existe un semestre con los datos ingresados </p>
            </div>
            <% } else if (!(request.getParameter("modificarEstudiante") == null)) {
               
                String ls = request.getParameter("apellido");
                System.out.println("Apellido que pasa por Principal: " + ls);
            %>

            <%@include  file="includes/ModificarEstudiante.jsp" %>
            <% }else if (!(request.getParameter("errorModificarEstudiante") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">Ocurrió un error modificando la información del Estudiante, Intente en otro momento </p>
            </div>
            <%} else if (!(request.getParameter("EnviarMensaje") == null)) {
                EstudianteDAO managerEstudiante = new EstudianteDAO();
                List student = managerEstudiante.getEstudianteXProfesor2(identificacion);
                request.setAttribute("estudiantes", student);
                request.setAttribute("usuario", identificacion);
            %>
            <%@include  file="includes/EnviarMensajeEstudiantes.jsp" %>
            <% } else if (request.getParameter("Send") != null) {

                if (request.getParameter("Send").equalsIgnoreCase("yes")) {
            %>
             <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-success">Mensaje Enviado correctamente, Se le ha enviado una copia a su correo electrónico</p>
            </div>
            <% } else { %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">El mensaje no pudo ser Enviado.</p>
            </div>
            <%
                    }
                }  else if (!(request.getParameter("NoSeleccionoEstudiantes") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-info">No seleccionó ningún estudiante para enviar el correo </p>
            </div>
            <% } else if (!(request.getParameter("ListarSemestresInt") == null)) {
                String codIntercambio = request.getParameter("codIntercambio");
                IntercambioDAO managerIntercambio = new IntercambioDAO();
                List materiasIntercambio = managerIntercambio.obtenerMateriasIntercabios(Integer.parseInt(codIntercambio));
                request.setAttribute("materias", materiasIntercambio);
            %>

            <%@include  file="includes/ListarMateriasIntercambio.jsp" %>
            <% } else {%>
                <div style="height: 300px;">
                
            </div>
            <% } %>

        </div>
    </div>    

    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>');</script>
<!--    <script src="js/jquery.js"></script>-->
    <script src="js/bootstrap-checkbox.js"></script>
    <script src="js/vendor/bootstrap.js"></script>
    <script src="js/main.js"></script>
    <script src="js/coloresCheckBox.js"></script>
</body>
</html>
