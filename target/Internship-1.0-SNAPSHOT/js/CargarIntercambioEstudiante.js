/*
 Metodo para obtenert los intercambios, via AJAX, elgido el numero de cedula de un estudiante
 */
function obtenerIntercambios()
{
//Add a comment to this line
    cedula = $("#estudiante")[0].value;

    $.ajax({
        url: "RecuperarIntercambioEstudianteServlet",
        type: "POST",
        data: "cedula=" + cedula,
        success: procesarObtenerIntermcabio,
        error: error
    });
}

/*
 M�todo para procesar lo que se recibe del servidor en relaci�n a los recursos que
 pertenecen al mismo tipo.
 */
function procesarObtenerIntermcabio(respuesta)
{
    $("#intercambios").html(respuesta);
}

function error(respuesta, error)
{
    $("#cargador").hide();
    $("#formularioRegistro").fadeIn();
    alert(respuesta.responseText);
}

/*
 Metodo para obtenert los Semestres de un intercambio, via AJAX pasando como parametro el codigo del Intercambio Seleccionado
 */
function obtenerSemestres()
{
//Add a comment to this line
    intercambio = $("#intercambios")[0].value;

    $.ajax({
        url: "RecuperarSemestresEstudianteServlet",
        type: "POST",
        data: "intercambio=" + intercambio,
        success: procesarObtenerSemestre,
        error: error
    });
}

function procesarObtenerSemestre(respuesta)
{
    $("#semestres").html(respuesta);
}

function obtenerComprobantes() {
    intercambio = $("#intercambios")[0].value;

    $.ajax({
        url: "MostrarComprobanteServlet",
        type: "POST",
        data: "intercambio=" + intercambio,
        success: procesarObtenerComprobante,
        error: error
    });
}

function procesarObtenerComprobante(respuesta)
{
    $("#archivos").html(respuesta);
}
