/*
 Metodo para obtenert los estudiantes, via AJAX, eligiendo la institucion que se desea consultar
 */
function obtenerEstudiantes()
{
//Add a comment to this line
    institucion = $("#institucion")[0].value;

    $.ajax({
        url: "RecuperarEstudiantesxInstitucion",
        type: "POST",
        data: "institucion=" + institucion,
        success: procesarObtenerEstudiantes,
        error: error
    });
}

/*
 M�todo para procesar lo que se recibe del servidor en relaci�n a los recursos que
 pertenecen al mismo tipo.
 */
function procesarObtenerEstudiantes(respuesta)
{
    $("#estudiantes").html(respuesta);
}

function error(respuesta, error)
{
    $("#cargador").hide();
    $("#formularioRegistro").fadeIn();
    alert(respuesta.responseText);
}

