<%-- 
    Document   : IndexEstudiante
    Created on : 04-jun-2014, 16:25:22
    Author     : Usuario
--%>    
<%@page import="udea.drai.intercambio.dto.Persona"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="shortcut icon" href="../../assets/ico/favicon.ico" />
        <title>Información Personal</title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="css/logo-nav.css" rel="stylesheet" type="text/css" />
        <!--  <link href="../../dist/css/bootstrap.min.css" rel="stylesheet" />
         <link href="signin.css" rel="stylesheet">-->
          <script src="js/diseno.js"></script>
        <% String ms = "";
            ms = (String) request.getAttribute("mensaje");
            HttpSession sesionOk = request.getSession();
            System.out.println("Mensaje Error en el Index: " + ms);
            sesionOk.setAttribute("archTem", "N");
            sesionOk.setAttribute("arch", null);
            Boolean t = sesionOk.isNew();
            String login = (String) sesionOk.getAttribute("login");
            if (login != null && login.equals("true")) {
                String e = (String) sesionOk.getAttribute("tipoUsuario");
                if (e.equals("3")) {
                    response.sendRedirect("PrincipalEstudiante.jsp");
                }

            }
        %> 


    </head>
    <body style="margin-top: 0px; height: 100%; ">

        <div class="container">

            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <h1 class="text-center login-title">Login</h1>
                    <div class="account-wall">
                        <!--                        <img class="profile-img" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120"
                                                     accesskey="" alt="">-->
                        <br>
                            <%
                                if (ms != null) {
                            %>
                            <p> <%=ms%></p>
                            <%
                                }
                            %>
                            <form class="form-signin" accept-charset="UTF-8" action="loginServlet" method="POST">
                                <input type="text" class="form-control" placeholder="Usuario" name="usuario" id="usuario" value="" required autofocus>
                                    <br>
                                        <input type="password" class="form-control" placeholder="Contraseña" name="contrasena" id="contrasena" value=""  required>
                                            <br>
                                                <button class="btn btn-lg btn-primary btn-block" type="submit">
                                                    Enviar</button>
                                                </form>
                                                </div>
                                                </div>
                                                </div>
                                                </div>
   </body>

    </html>

