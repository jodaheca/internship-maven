<%-- 
    Document   : RevisarMateriasEstudianteAdmin
    Created on : 14-ago-2014, 9:30:01
    Author     : Usuario
--%>

<%@page import="udea.drai.intercambio.dto.Homologacion"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List materias = (List) request.getAttribute("materias");
    String modificacion = (String) request.getAttribute("modificacion");
    // List materias=null;
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Revisar Materias</title>
    </head>
    <body>
        <div class="container">
            <h1 class="text-center login-title"> Materias Cursadas</h1>
            <div class="account-wall">
                <br><br>
                <%
                    if (materias == null || materias.isEmpty() == true) {
                %>
                <div class="col-sm-6 col-md-4 col-md-offset-4">   
                    <p class="bg-info">No hay materias registradas en este semestre</p>
                </div>
                <%
                } else {
                %>
                <%
                    if (modificacion == "true") {
                %>
                <div class="col-sm-6 col-md-4 col-md-offset-4">    
                    <p class="bg-info">Modificacion registrada Exitosamente</p>
                </div>
                <%
                    }
                %>  
                <table class="table table-bordered">
                    <thead>
                        <tr> 
                            <th>Codigo origen</th> 
                            <th>nombre origen</th> 
                            <th>Codigo destino</th> 
                            <th>Nombre destino</th> 
                            <th>Revision</th>
                            <th>Nota</th>
                        </tr>
                    </thead> 
                    <tbody>
                        <%
                            for (int i = 0; i < materias.size(); i++) {
                                Homologacion nueva = (Homologacion) materias.get(i);
                                String nombreOrigen = nueva.getMateriaLocal();
                                String nombreDestino = nueva.getMateriaExtranjero();
                                String codOrigen = nueva.getCodLocal();
                                String codDestino = nueva.getCodExtranjero();
                                char revision = nueva.getRevision();
                                String nota = nueva.getNota();
                                int cod = nueva.getCodigo();

                        %>
                        <tr class="active" > 
                            <td><%=codOrigen%></td> 
                            <td><%=nombreOrigen%></td> 
                            <td><%=codDestino%></td> 
                            <td><%=nombreDestino%></td> 
                                <%
                                    if (revision == 'R') {
                                %>
                            <td>Revisado</td> 
                            <%
                            } else if (revision == 'P') {

                            %>
                            <td>Pendiente</td>
                            <% } else if (revision == 'N') {
                            %>
                            <td>No Aplica</td>
                            <%
                                }
                            %>
                            <td><%=nota%></td>
                        </tr>
                        <%
                                }
                            }
                        %> 
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
