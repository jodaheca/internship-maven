<%-- 
    Document   : Materia
    Created on : 09-jun-2014, 10:04:54
    Author     : Usuario
--%>

<%@page import="udea.drai.intercambio.dao.TipoIntercambioDAO"%>
<%@page import="udea.drai.intercambio.dto.TipoIntercambio"%>
<%@page import="udea.drai.intercambio.dto.Intercambio"%>
<%@page import="udea.drai.intercambio.dao.HomologacionDAO"%>
<%@page import="udea.drai.intercambio.dto.Homologacion"%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Homologacion</title>
<link href="../css/logo-nav.css" rel="stylesheet" type="text/css" />

    </head>
    <body>
        <%
            Homologacion   hm = new Homologacion();
            if(homo != null){
                hm.setCodigo(Integer.parseInt(homo));
                HomologacionDAO hmDAO = new HomologacionDAO();
                hmDAO.cargarHomologacion(hm);
                
            }
            
        
            %>
            <div class="panel" id="info1" style="background:#F8F8F8;  width:auto; margin-left:40px;  " >
                <a class="col-lg-1" href="PrincipalEstudiante.jsp?intercambios">[X]</a>
                <br />
                <form method="post" accept-charset="UTF-8" action="materiaServlet" >
             <div class="row" >
                 <input type="hidden" id="homologacion" name="homologacion" <% if (hm.getCodigo() != 0) { %> value="<%=hm.getCodigo() %>"  <% }%> />
                
                 
                 <div class="col-lg-3">
                     <p></p><br />

                 Semestre previsto: <br />
                 <select class="form-control"  id="semestre" name="semestre"  >
                            <%
                            Iterator itP = periodos.iterator();
                            while(itP.hasNext()){
                                Semestre tipo =(Semestre) itP.next();
                                %>
                                <option value="<%=tipo.getCodigo() %>" <% if(tipo.getCodigo() == hm.getSemestre()  ){ %> selected="selected" <% } %>><%=tipo.getAno() %> - <%=tipo.getPeriodo() %> </option>
                                <%
                            }
                                %>
                </select>
                <!-- <% if (hm.getCodigo() != 0) { %>
                  #Caso SSOFFI:
                  <input class="form-control" name="ssoffi" id="ssoffi" type="text" required="true" />
                 <% }%>-->
                 </div>
                    
                 <div class="col-lg-3">
                        <p>  Materia (Origen)</p>
                        Codigo<input type="text" class="form-control" name="codOrigen" <% if(inter.getTipo()>7){%>  <%	if(hm.getCodLocal() == null){%> value = "000" <%	}else{	%>value = "<%=hm.getCodLocal() %>"  <% }}else{%> required="true" <% }if(hm.getCodLocal() == null){	%> value = "" <%}else{%>value = "<%=hm.getCodLocal() %>" <%}%>/>
                        Nombre<input type="text" class="form-control" name="nombreOrigen" <% if(inter.getTipo()>7){%>  <%	if(hm.getMateriaLocal()== null){%> value = "Electiva internacionalización" <%	}else{	%>value = "<%=hm.getMateriaLocal()%>"  <% }}else{%> required="true" <% }if(hm.getCodLocal() == null){	%> value = "" <%}else{%>value = "<%=hm.getMateriaLocal()%>" <%}%> />
                    </div>
           
             
                    <div class="col-lg-3" >
                        <p>  Materia (Destino)</p>
                        Codigo<input type="text" class="form-control" name="codDestino" required="true" <% if(hm.getCodExtranjero() != null) { %> value="<%=hm.getCodExtranjero()%>"<% } %> />
                       Nombre<input type="text" class="form-control" name="nombreDestino" required="true" <% if(hm.getMateriaExtranjero()!= null) { %> value="<%=hm.getMateriaExtranjero() %>"<% } %> />
                   </div>
            
             </div>
                <br />
           <!--  <div class="row">
                 <p> Acta - Resolución comité de carrera (fecha: <input type="text" class="fecha" name="fecha" id="fecha" style=" width: 140px; height: 30px;"  <% if(hm.getFechaActa()!= null) { %> value="<%=hm.getFechaActa()%>"<% } %>  <% if (hm.getCodigo() != 0) { %> disabled="disabled" <% } %> /> Número: <input type="number" name="acta" id="acta" style=" width: 70px;" <% if(hm.getActa()!= 0) { %> value="<%=hm.getActa()%>"<% } %> <% if (hm.getCodigo() != 0) { %> disabled="disabled" <% } %> /> )</p>
             </div>-->
             <div class="row" style=" width: auto; margin:30px " >
                 <br />
                  <% // if (hm.getCodigo() != 0) { %>
                     <!-- Observación:<br />
                     <textArea id="observaciones" class="form-control" name="obervaciones" style=" resize:none;" ></textArea>
                     -->
                  <% //} %>
                <br />
                <input type="submit" class="btn btn-primary " value="Guardar matrícula" />
             </div>
      
            </form>
                </div>
       
    </body>
</html>

