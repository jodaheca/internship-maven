<%-- 
    Document   : CambiarContrasena
    Created on : 03-oct-2014, 8:40:39
    Author     : Usuario
--%>

<%@page import="udea.drai.intercambio.dto.Persona"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cambiar contraseña</title>
    </head>
    <body>
        <%
            Persona pers = (Persona) sesion.getAttribute("persona");
        %>
        <form id="cambiarPass" class="form-signin" name="cambiarPass" accept-charset="UTF-8" action="CambiarPassServlet" method="POST" onsubmit="" >
            <input type="hidden" id="user" name="user" value="<%=pers.getUsuario()%>" />
            <input type="password" class="form-control" style="width: 35%; text-align: center" placeholder="Contraseña anterior" name="passAnt" id="passAnt" required >
            <br />
            <input type="password" class="form-control"  style="width: 35%; text-align: center" placeholder="Contraseña Nueva" name="passNew" onkeyup="ComprobarAcentos(this);" id="contrasena" required  >
            <br />
            <input type="password" class="form-control" style="width: 35%; text-align: center" placeholder="Repita Contraseña Nueva"  name="passNew2"  onkeyup="ComprobarAcentos(this);"  id="contrasena2" required >
            <br/>         
            <input type="submit" class="btn btn-primary "  value="Modificar Contraseña" />

        </form>
        <script >function ComprobarAcentos(inputtext)
            {
                if (!inputtext)
                    return false;
                if (inputtext.value.match('[á,é,í,ó,ú]|[Á,É,Í,Ó,Ú]'))
                {

                    alert('No se permiten acentos en la casilla');
                    inputtext.value = "";
                    inputtext.focus();
                    return true;
                }
                return false;
            }</script>
    </body>
</html>
