<%-- 
    Document   : ModificarPrograma
    Created on : 03-dic-2014, 14:34:28
    Author     : Usuario
--%>

<%@page import="udea.drai.intercambio.dto.Persona"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    String nombre = request.getParameter("nombre");
    String director = request.getParameter("director");
    String nombreDirector = request.getParameter("direct");
    String codigo = request.getParameter("codigo");
    List profesores =(List) request.getAttribute("profesores");
%>
<html >
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Modificar Programa</title>
    </head>
  
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <h1 class="text-center login-title">Modificar Programa</h1>
                        <div class="account-wall">
                                <br>
                                <form name="formulario" class="form-signin" accept-charset="UTF-8" action="ModificarProgramaServlet" method="post">
                                    <select class="form-control" name="director" id="director" required="true">
                                        <option value= <%=director%> ><%=nombreDirector%></option>
                                                <%
                                                    for(int i=0; i<profesores.size();i++){
                                                        Persona profesor=(Persona) profesores.get(i);
                                                        String cedula=profesor.getCedula();
                                                        String name=profesor.getNombre();
                                                    
                                                %>
                                                <option value= <%=cedula%>>
                                                               <%=name%>
                                                </option>
                                                <%
                                                 }%>
                                        </select>
                                        <br>
                                        <input type="hidden" class="form-control" value="<%=codigo%>"name="codigo" id="codigo" required>
                                     <input type="text" class="form-control" value="<%=nombre%>" placeholder="Nombre" name="nombre" id="nombre" required>
                                     <br>
                                  <button class="btn btn-lg btn-primary btn-block" type="submit">
                                     Modificar</button>
                               </form>
                             </div>
                    </div>
                </div>
            </div>
    </body>
</html>