<%-- 
    Document   : BuscarEstudiante
    Created on : 16-oct-2014, 9:17:20
    Author     : Usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Buscar Estudiante</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <br><br>
                    <h1 class="text-center login-title">Buscar Estudiante</h1>
                        <div class="account-wall">
                                <br>
                                <form name="buscar" class="form-signin" accept-charset="UTF-8" action="BuscarEstudianteServlet" method="post">
                                    <input type="number" class="form-control" placeholder="Documento Estudiante" name="identificacion" id="identificacion" required>
                                     <br>  
                                  <button class="btn btn-lg btn-primary btn-block" type="submit">
                                     Buscar Estudiante</button>
                               </form>
                             </div>
                    </div>
                </div>
            </div>
    </body>
</html>