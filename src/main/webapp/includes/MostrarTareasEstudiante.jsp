<%-- 
    Document   : MostrarTareasEstudiante
    Created on : 04-ago-2014, 8:43:15
    Author     : Joaquin Hernández Cárdenas <jdavidhc94@gmail.com>
--%>

<%@page import="udea.drai.intercambio.dto.RevisionCompleta"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List tareas = (List) request.getAttribute("revisiones");
    String modificacion = (String) request.getAttribute("modificacion");
    RevisionCompleta sacar = (RevisionCompleta) tareas.get(0);
    String nombreEstudiante = sacar.getNombreEstudiante();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Revisar Tareas</title>
    </head>
    <body>
        <div class="container">
            <h2 class="text-center "><%=nombreEstudiante%></h2>
            <div class=" account-wall">
                 <form action="Principal.jsp?ListarEstudiantes" method="POST">
                    <button class="btn btn-lg btn-primary btn-warning" type="submit">
                        Atrás </button>
                  </form>
                <br><br>
                <%
                    if (tareas == null || tareas.isEmpty() == true) {
                %>
                <div class="col-sm-6 col-md-4 col-md-offset-4">   
                    <p class="bg-info">No hay tareas registradas en este Intercambio</p>
                </div>
                <%
                } else {
                    if (modificacion == "true") {
                %>
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <br>
                    <p class="bg-success">Modificación registrada exitosamente.</p>
                </div>
                <%
                    }
                %> 
                <table class="table table-bordered">
                    <thead>
                        <tr> 
                            <th>Nombre</th> 
                            <th>Estado</th>
                            <th>Comentario</th>
                        </tr>
                    </thead> 
                    <tbody>
                        <%
                            String comentario = "defecto lista";
                            for (int i = 0; i < tareas.size(); i++) {
                                RevisionCompleta nueva = (RevisionCompleta) tareas.get(i);
                                int codigo = nueva.getCodigo();
                                String estudiante = nueva.getEstudiante();
                                int intercambio = nueva.getIntercambio();
                                String nombre = nueva.getNombre();
                                String estado = nueva.getEstado();
                                comentario = nueva.getComentario();
                                char[] es = estado.toCharArray();
                                char esta = es[0];

                        %>

                    <form action="ModificarRevision"  accept-charset="UTF-8" method="POST">   
                        <tr class="active" > 
                            <td><%=nombre%></td>  
                            <td>
                                <select id="estado" name="estado" <% if (esta == 'R') { %> style="color: #009900" <% }else if(esta == 'P'){ %>style="color: #d9534f" <%}else if(esta == 'N'){ %>style="color: #000" <%} %>>
                                    <option  value="R" style="color: #009900" <% if (esta == 'R') { %> selected="selected" <%  } %> >Revisado</option>
                                    <option value="P" style="color: #d9534f"<% if (esta == 'P') { %> selected="selected" <% } %>>Pendiente</option>
                                    <option value="N"  style="color: #000"<% if (esta == 'N') { %> selected="selected" <% }%>>No Aplica</option>
                                </select>

                            </td>
                            <td><input type="text"   name="comentario" id="comentario" value="<%=comentario%>" /></td>
                            <td><input type="submit"  value="Modificar" name="Modificar" id="modificar" /></td> 

                        </tr>
                        <input type="hidden" name="codigoRevision" value="<%=codigo%>" >
                        <input type="hidden" name="estudiante" value="<%=estudiante%>" >
                        <input type="hidden" name="intercambio" value="<%=intercambio%>" >
                    </form>
                    <%
                            }
                        }
                    %> 
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>

