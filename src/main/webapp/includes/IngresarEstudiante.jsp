<%-- 
    Document   : IngresarEstudiante
    Created on : 15-jul-2014, 10:30:16
    Author     : Usuario
--%>

<%@page import="udea.drai.intercambio.dto.Programa"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%     
    List programas=(List) request.getAttribute("programas");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registrar Estudiante</title>
    </head>
  
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <h1 class="text-center login-title">Registrar Estudiante</h1>
                        <div class="account-wall">
                                <br>
                                <form name="formulario" class="form-signin" accept-charset="UTF-8" action="IngresarEstudianteServlet"  method="post">
                                    <input type="text" class="form-control" placeholder="Numero de documento" name="documento" id="documento" required>
                                     <br>
                                     <select class="form-control" name="programa" id="programa" required="TRUE">
                                                <option value=""> Programa Academico</option>
                                                <%
                                                    for(int i=0; i<programas.size();i++){
                                                        Programa nueva=(Programa) programas.get(i);
                                                        String codigo=nueva.getCodigo();
                                                        String nombre=nueva.getNombre();
                                                    
                                                %>
                                                <option value= <%=codigo%> >
                                                               <%=nombre%>
                                                </option>
                                                <%
                                                 }%>
                                        </select>
                                      <br> 
                                      <input type="text" class="form-control" placeholder="Usuario Asignado" name="student" onkeyup="ComprobarAcentos(this);" id="student"  required>
                                     <br> 
                                     <input type="text" class="form-control" placeholder="Password" name="password" onkeyup="ComprobarAcentos(this);" id="password" required>
                                     <br>
                                  <button class="btn btn-lg btn-primary btn-block" type="submit">
                                     Agregar Estudiante</button>
                               </form>
                             </div>
                    </div>
                </div>
            </div>
<!--        <script type="text/javascript" src="../js/jquery.js"></script>
        <script type="text/javascript" src="js/Validaciones.js"></script>-->
    </body>
</html>
