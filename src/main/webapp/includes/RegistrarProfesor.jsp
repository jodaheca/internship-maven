<%-- 
    Document   : RegistrarProfesor
    Created on : 25-ago-2014, 11:18:16
    Author     : Usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registrar Profesor</title>
    </head>
  
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <h1 class="text-center login-title">Registrar Profesor</h1>
                        <div class="account-wall">
                                <br>
                                <form name="formulario" class="form-signin" accept-charset="UTF-8" action="IngresarProfesorServlet" method="post">
                                    <input type="text" class="form-control" placeholder="Nombre" name="nombre" id="nombre" required>
                                     <br>
                                     <input type="text" class="form-control" placeholder="Apellido" name="apellido" id="apellido" required>
                                     <br>
                                     <input type="text" class="form-control" placeholder="identificación" name="id" id="id" required>
                                     <br>  
                                     <input type="email" class="form-control" placeholder="email" name="email" id="email" required>
                                     <br> 
                                     <input type="text" class="form-control" placeholder="usuario Asignado" name="usuario" onkeyup="ComprobarAcentos(this);" id="usuario" required>
                                     <br> 
                                     <input type="text" class="form-control" placeholder="Contraseña" onkeyup="ComprobarAcentos(this);" name="password" id="password" required>
                                     <br> 
                                  <button class="btn btn-lg btn-primary btn-block" type="submit">
                                     Agregar Profesor</button>
                               </form>
                             </div>
                    </div>
                </div>
            </div>
    </body>
</html>

