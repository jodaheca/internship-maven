<%-- 
    Document   : EliminarTipoIntercambio
    Created on : 14-jul-2014, 11:07:46
    Author     : Usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Eliminar Tipo Intercambio</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <br><br>
                    <h1 class="text-center login-title">Eliminar Tipo Intercambio</h1>
                        <div class="account-wall">
                                <br>
                                <form name="eliminar" class="form-signin" accept-charset="UTF-8" action="EliminarTipoIntercambioServlet" method="post">
                                    <input type="text" class="form-control" placeholder="Codigo del Intercambio" name="codigo" id="codigo" required>
                                     <br>  
                                  <button class="btn btn-lg btn-primary btn-block" type="submit">
                                     Eliminar Intercambio</button>
                               </form>
                             </div>
                    </div>
                </div>
            </div>
    </body>
</html>
