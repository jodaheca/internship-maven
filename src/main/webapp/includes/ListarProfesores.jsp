<%-- 
    Document   : ListarPorfesores
    Created on : 04-sep-2014, 14:54:51
    Author     : Usuario
--%>

<%@page import="udea.drai.intercambio.dto.Persona"%>
<%@page import="udea.drai.intercambio.dto.Institucion"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%     
    List profesores=(List) request.getAttribute("profesores");
      HttpSession sesionOk = request.getSession();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista de Profesores</title>
    </head>
    <body>
        <div class="container">
                        <h1 class="text-center login-title"> Profesores Registrados</h1>
                                <div class="account-wall">
                        <br><br>
                     <% 
                            if(profesores==null){
                                %>
                                <div class="col-sm-6 col-md-4 col-md-offset-4">      
                                 <p class="bg-info">Ocurrió un erro al recuperar la lista de los profesores, intente Luego </p>
                                 </div>
                            <%
                            } else  if(profesores.size()==0){
                                %>
                                 <div class="col-sm-6 col-md-4 col-md-offset-4">    
                                     <p class="bg-info">No existen Profesores registrados </p>
                                 </div>
                        <%
                            }else{
                        %>   
                    <table class="table table-bordered">
                        <thead>
                            <tr> 
                                <th>Identificación</th> 
                                <th>Nombre</th> 
                                <th>Apellido</th> 
                                <th>email</th>
                                <th>usario Sistema</th>
                            </tr>
                        </thead> 
                        <tbody>
                            <%
                              for(int i=0;i<profesores.size();i++){
                                Persona nuevo=(Persona) profesores.get(i);
                                String id=nuevo.getCedula();
                                String nombre=nuevo.getNombre();
                                String apellido=nuevo.getApellido();
                                String email=nuevo.getEmail();
                                String usuario=nuevo.getUsuario();

                            %>
                             <tr class="active">
                                <td><% if(id != null){ %><%=id%> <% } %></td> 
                                <td><% if(nombre != null){ %><%=nombre%><% } %></td> 
                                <td><% if(apellido != null){ %><%=apellido%><% } %></td> 
                                <td><% if(email != null){ %><%=email%><% } %></td>
                                <td><% if(usuario != null){ %><%=usuario%><% } %></td>
                                <td><form action="PrincipalAdmin.jsp?modificarProfesor" method="POST">
                                         <input type="hidden" id="identificacion" name="identificacion" value="<%=id%>">
                                         <input type="hidden" id="nombre" name="nombre" value="<%=nombre%>">
                                         <input type="hidden"  id="apellido" name="apellido" value="<%=apellido%>">
                                         <input type="hidden" id="email" name="email" value="<%=email%>">
                                        <button class="btn btn-lg btn-primary btn-xs" type="submit">
                                     Modificar</button></form></td>
                                     <td><form action="PrincipalAdmin.jsp?eliminarProfesor" method="POST">
                                         <input type="hidden" id="id" name="id" value="<%=id%>">
                                        <button class="btn btn-lg btn-primary btn-xs" type="submit">
                                     Eliminar</button></form></td>
                            </tr>

                            <%    
                            }  
                            %> 
                        </tbody>
                    </table>
                        <%
                            }
                        %>
                </div>
            </div>
    </body>
</html>
