<%-- 
    Document   : ListarSemestres
    Created on : 29-jul-2014, 11:34:24
    Author     : Usuario
--%>

<%@page import="udea.drai.intercambio.dto.Semestre"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%     
    List semestres=(List) request.getAttribute("semestres");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista de Semestres</title>
    </head>
    <body>
        <div class="container">
                        <h1 class="text-center login-title"> Semestres Registrados</h1>
                                <div class="account-wall">
                        <br><br>
                        <% 
                            if(semestres==null){
                                %>
                                <div class="col-sm-6 col-md-4 col-md-offset-4">      
                                 <p class="bg-info">Ocurrio un erro al recuperar los Semestres, intente Luego </p>
                                 </div>
                            <%
                            } else  if(semestres.size()==0){
                                %>
                                 <div class="col-sm-6 col-md-4 col-md-offset-4">    
                                     <p class="bg-info">No existen semestres registrados </p>
                                 </div>
                        <%
                            }else{
                        %>
                    <table class="table table-bordered">
                        <thead>
                            <tr> 
                                <th>Codigo</th> 
                                <th>año</th>
                                <th>Periodo</th>
                                <th>Modificar</th>
                                <th>Eliminar</th>
                            </tr>
                        </thead> 
                        <tbody>
                            <%
                              for(int i=0;i<semestres.size();i++){
                                Semestre nueva=(Semestre) semestres.get(i);
                                int codigo=nueva.getCodigo();
                                int anio=nueva.getAno();
                                int periodo=nueva.getPeriodo();

                            %>
                             <tr class="active">
                                <td><%=codigo%></td> 
                                <td><%=anio%></td>
                                <td><%=periodo%></td>
                                <td><form action="PrincipalAdmin.jsp?modificarSemestre" method="POST">
                                         <input type="hidden" id="cod" name="cod" value="<%=codigo%>">
                                         <input type="hidden" id="anio" name="anio" value="<%=anio%>">
                                         <input type="hidden"  id="periodo" name="periodo" value="<%=periodo%>">
                                        <button class="btn btn-lg btn-primary btn-xs" type="submit">
                                     Modificar</button></form></td>
                                     <td><form action="PrincipalAdmin.jsp?eliminarSemestre" method="POST">
                                         <input type="hidden" id="codigo" name="codigo" value="<%=codigo%>">
                                        <button class="btn btn-lg btn-primary btn-xs" type="submit">
                                     Eliminar</button></form></td>
                            </tr>
                            

                            <%    
                            }  
                            %> 
                        </tbody>
                    </table>
                        <%
                        }
                        %>
                </div>
            </div>
    </body>
</html>

