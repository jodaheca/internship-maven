<%-- 
    Document   : EnviarMensajeEstudiantes
    Created on : 02-feb-2015, 14:45:18
    Author     : Usuario
--%>

<%@page import="java.util.List"%>
<%@page import="udea.drai.intercambio.dto.Persona"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Persona> estudiantes = (List) request.getAttribute("estudiantes");
    String profesor = (String) request.getAttribute("usuario");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Enviar Mensaje</title>
    </head>

    <body>

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <h1 class="text-center login-title">Enviar Mensaje</h1>
                    <div class="account-wall">
                        
                        <form action="Principal.jsp?ListarEstudiantes" method="POST">
                        <button class="btn btn-lg btn-primary btn-warning" type="submit">
                            Atrás </button>
                        </form>
                        <br>
                        <!--<input type="checkbox" class="checkbox" name="destinatarios" value="">-->
                        <form name="formulario" class="form-signin" accept-charset="UTF-8" action="MensajeProfesorServlet" method="POST">
                            <select class="form-control" name="codigoVerificacion" id="codigoVerificacion" onfocus="obtenerLista()" onchange="obtenerLista()"  required="true">
                                <option value="1122334455" selected> Enviar a Todos</option>
                                <option value="1111111111"> Enviar a varios</option>
                            </select>
                            <!--<br>-->
                            <!--                            <div class="form-group">
                                                            
                                                            <table class="table table-bordered">
                                                                <thead>
                                                                    <tr> 
                                                                        <th>Estudiante</th> 
                                                                        <th>Enviar correo</th>
                                                                    </tr>
                                                                </thead> 
                                                                <tbody>
                            <%
                                for (int i = 0; i < estudiantes.size(); i++) {
                                    Persona estudiante = estudiantes.get(i);
                                    String correo = estudiante.getEmail();
                                    String est = estudiante.getNombre() + " " + estudiante.getApellido();
                            %>
                            <tr class="active">
                                <td><%=est%></td>

                                <td><input type="checkbox" class="checkbox" name="destinatarios" value="<%=correo%>"></td>
                            </tr>
                            <%
                                }
                            %>
                        </tbody>
                    </table>
                </div>-->  
                            <br>
                            <div  class="form-group" id="estudiantes">

                            </div>
                            <div class="form-group">
                                <label for="asunto">Asunto:</label>
                                <input type="text" class="form-control"  name="asunto" id="asunto" required>
                            </div>
                            <br>
                            <div class="form-group">
                                <label for="mensaje">Mensaje</label>
                                <textarea class="form-control" rows="5" name="mensaje" id="mensaje" required=""></textarea>
                            </div>
                            <br>
                            <input type="hidden" name="profesor" id="profesor" value="<%=profesor%>">
                            <button class="btn btn-lg btn-primary btn-block" type="submit">
                                Enviar Mensaje</button>
                        </form>  
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>

