<%-- 
    Document   : subirNota
    Created on : 17-jul-2014, 15:09:21
    Author     : Usuario
--%>

<%@page import="org.apache.commons.fileupload.FileItem"%>
<%@page import="udea.drai.intercambio.dto.Semestre"%>
<%@page import="java.util.Iterator"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type="text/javascript" src="js/validaciones.js"></script>
    </head>
    <body>   
        <a class="col-lg-2" href="PrincipalEstudiante.jsp?intercambios&close">[X]</a>
        <br />
        <br />
        <div class="container" >
            
                    
        <% if(sesion.getAttribute("archTem") != "Y"){ %>
        <div class="row">
            
            <form method="POST" enctype='multipart/form-data' action="cargarArchivo"  > 
                <div class="col-lg-7">
                    Seleccione el archivo .PDF correspondiente:
                    <input  type="file" name="fichero" accept=".PDF" required  />   
                <br />
                    <input type="submit" value="Cargar">
                    </div>
       
                </form>
         
             </div>
            <% }else{ 
           FileItem f = (FileItem) sesion.getAttribute("arch");
            String name = f.getName();
        %>
        <div class="col-lg-3">
        Archivo subido: 
        <input class="form-control " type="text" value="<%=name %>" readonly/>
        </div>
            <form id="sub" name="sub" method="POST"  action="subirArchivo" >  
                    <input type="hidden" name="intercambio" id="intercambio" value="<%=id.getCodigo() %>" />
                     <div class="row">
               
                <div class="col-lg-2">
                Seleccione el periodo:
                <br />
                <select class="form-control"  id="semestre" name="semestre">
                            <%
                            Iterator itP = periodos.iterator();
                            while(itP.hasNext()){
                                Semestre tipo =(Semestre) itP.next();
                                %>
                                <option value="<%=tipo.getCodigo() %>"><%=tipo.getAno() %> - <%=tipo.getPeriodo() %> </option>
                                <%
                            }
                                %>
                </select>
                </div>
                      </div>
                <br />
                <div class="row">
                    <div class="col-lg-7">
                 <input type="submit" value="Subir archivo" />
                    </div>
                          
                </div>
         </form> 
     <% } %>
       
         </div>
         </div>
    </body>
</html>
