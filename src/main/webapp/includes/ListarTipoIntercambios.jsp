<%-- 
    Document   : ListarTipoIntercambios
    Created on : 14-jul-2014, 11:38:31
    Author     : Usuario
--%>

<%@page import="udea.drai.intercambio.dto.TipoIntercambio"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%     
    List tipoIntercambios=(List) request.getAttribute("intercambios");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista de Tipos Intercambios</title>
    </head>
    <body>
        <div class="container">
                        <h1 class="text-center login-title"> Tipos de Intercambios Registrados</h1>
                                <div class="account-wall">
                        <br><br>
                    <% 
                            if(tipoIntercambios==null){
                                %>
                                <div class="col-sm-6 col-md-4 col-md-offset-4">      
                                    <p class="bg-info ">Ocurrio un erro al recuperar los Tipos de Intercambios, intente Luego </p>
                                </div>
                         <%
                            } else  if(tipoIntercambios.size()==0){
                                %>
                                <div class="col-sm-6 col-md-4 col-md-offset-4">        
                                    <p class="bg-info">No existen Tipos de intercambios registrados </p>
                                    </div>
                            <%
                            }else{
                        %>    
                    <table class="table table-bordered">
                        <thead>
                            <tr> 
                                <th>Codigo</th> 
                                <th>Nombre</th> 
                                <th>Numero Semestres</th> 
                            </tr>
                        </thead> 
                        <tbody>
                            <%
                              for(int i=0;i<tipoIntercambios.size();i++){
                                TipoIntercambio nueva=(TipoIntercambio) tipoIntercambios.get(i);
                                String codigo=nueva.getCodigo();
                                String nombre=nueva.getNombre();
                                String numSemestres=nueva.getNumSemestres();

                            %>
                             <tr class="active">
                                <td><% if(codigo != null){ %><%=codigo%> <% } %></td> 
                                <td><% if(nombre != null){ %><%=nombre%><% } %></td> 
                                <td><% if(numSemestres != null){ %><%=numSemestres%><% } %></td> 
                            </tr>

                            <%    
                            }  
                            %> 
                        </tbody>
                    </table>
                        <%
                            }
                        %>
                </div>
            </div>
    </body>
</html>
