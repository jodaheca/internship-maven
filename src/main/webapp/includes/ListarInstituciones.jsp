<%-- 
    Document   : ListarInstituciones
    Created on : 19-jun-2014, 8:33:03
    Author     : Usuario
--%>

<%@page import="udea.drai.intercambio.dto.Institucion"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%     
    List instituciones=(List) request.getAttribute("instituciones");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista de Instituciones</title>
    </head>
    <body>
        <div class="container">
                        <h1 class="text-center login-title"> Instituciones Registradas</h1>
                                <div class="account-wall">
                        <br><br>
                     <% 
                            if(instituciones==null){
                                %>
                                <div class="col-sm-6 col-md-4 col-md-offset-4">      
                                 <p class="bg-info">Ocurrio un erro al recuperar las Instituciones, intente Luego </p>
                                 </div>
                            <%
                            } else  if(instituciones.size()==0){
                                %>
                                 <div class="col-sm-6 col-md-4 col-md-offset-4">    
                                     <p class="bg-info">No existen Instituciones registradas </p>
                                 </div>
                        <%
                            }else{
                        %>   
                    <table class="table table-bordered">
                        <thead>
                            <tr> 
                                <th>Codigo</th> 
                                <th>Nombre</th> 
                                <th>Pais</th>
                            </tr>
                        </thead> 
                        <tbody>
                            <%
                              for(int i=0;i<instituciones.size();i++){
                                Institucion nueva=(Institucion) instituciones.get(i);
                                String codigo=nueva.getCodigo();
                                String nombre=nueva.getNombre();
                                String pais=nueva.getPais();

                            %>
                             <tr class="active">
                                <td><% if(codigo != null){ %><%=codigo%><% } %></td> 
                                <td><% if(nombre != null){ %><%=nombre%><% } %></td> 
                                <td><% if(pais != null){ %><%=pais%><% } %></td> 
                                <td><form action="PrincipalAdmin.jsp?modificarInstitucion" method="POST">
                                         <input type="hidden" id="cod" name="cod" value="<%=codigo%>">
                                         <input type="hidden" id="nombre" name="nombre" value="<%=nombre%>">
                                         <input type="hidden"  id="pais" name="pais" value="<%=pais%>">
                                        <button class="btn btn-lg btn-primary btn-xs" type="submit">
                                     Modificar</button></form></td>
                                     <td><form action="PrincipalAdmin.jsp?eliminarInstitucion" method="POST">
                                         <input type="hidden" id="codigo" name="codigo" value="<%=codigo%>">
                                        <button class="btn btn-lg btn-primary btn-xs" type="submit">
                                     Eliminar</button></form></td>
                            </tr>

                            <%    
                            }  
                            %> 
                        </tbody>
                    </table>
                        <%
                            }
                        %>
                </div>
            </div>
    </body>
</html>
