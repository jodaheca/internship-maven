<%-- 
    Document   : EstudiantexInstitucion
    Created on : 06-oct-2014, 10:55:00
    Author     : Usuario
--%>

<%@page import="udea.drai.intercambio.dto.Institucion"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Institucion> instituciones = (List) request.getAttribute("instituciones");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Estudiantes x Institución</title>
    </head>
    <body>

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <h2 class="text-center login-title">Estudiantes x Institución</h2>
                    <div class="account-wall">
                        <br>
                        <form name="formulario" class="form-signin" accept-charset="UTF-8" action="RecuperarEstudiantesxInstitucion" method="POST">
                            <select class="form-control" name="institucion" id="institucion"  required="true">
                                <option value=""> Seleccione Institución</option>
                                <%
                                    for (int i = 0; i < instituciones.size(); i++) {
                                        Institucion institucion = (Institucion) instituciones.get(i);
                                        String codigo = institucion.getCodigo();
                                        String nombre = institucion.getNombre();

                                %>
                                <option value= <%=codigo%> >
                                    <%=nombre%>
                                </option>
                                <%    }%>
                            </select>
                            <br>
                            <button class="btn btn-lg btn-primary btn-block" type="submit">
                                Revisar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>
