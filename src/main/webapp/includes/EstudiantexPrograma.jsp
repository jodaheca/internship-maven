<%-- 
    Document   : EstudiantexPrograma
    Created on : 09-oct-2014, 15:05:24
    Author     : Usuario
--%>

<%@page import="udea.drai.intercambio.dto.Programa"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Programa> programas = (List) request.getAttribute("programas");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Estudiantes x Programa</title>
    </head>
    <body>

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <h2 class="text-center login-title">Estudiantes x Programa</h2>
                    <div class="account-wall">
                        <br>
                        <form name="formulario" class="form-signin" accept-charset="UTF-8" action="RecuperarEstudiantesxPrograma" method="POST">
                            <select class="form-control" name="programa" id="programa"  required="true">
                                <option value=""> Seleccione Programa</option>
                                <%
                                    for (int i = 0; i < programas.size(); i++) {
                                        Programa nuevoPrograma = (Programa) programas.get(i);
                                        String codigo = nuevoPrograma.getCodigo();
                                        String nombre = nuevoPrograma.getNombre();

                                %>
                                <option value= <%=codigo%> >
                                    <%=nombre%>
                                </option>
                                <%    }%>
                            </select>
                            <br>
                            <button class="btn btn-lg btn-primary btn-block" type="submit">
                                Revisar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>

