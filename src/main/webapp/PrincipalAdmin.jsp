<%-- 
    Document   : PrincipalAdmin
    Created on : 13-ago-2014, 14:57:40
    Author     : Joaquin David Hernández Cárdenas.
    Comentario : Esta es la vista principal del Administrador, desde aqui podrá realizar todas las operaciones que está autorizado
    a realizar.
--%>


<%@page import="udea.drai.intercambio.dao.TipoIntercambioDAO"%>
<%@page import="udea.drai.intercambio.dao.ProfesorDAO"%>
<%@page import="udea.drai.intercambio.dao.SemestreDAO"%>
<%@page import="udea.drai.intercambio.dao.EstudianteDAO"%>
<%@page import="udea.drai.intercambio.dao.IntercambioDAO"%>
<%@page import="udea.drai.intercambio.dao.ProgramaDAO"%>
<%@page import="udea.drai.intercambio.dto.Persona"%>
<%@page import="java.util.List"%>
<%@page import="udea.drai.intercambio.dao.InstitucionDAO"%>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%    Persona persona = null;
    String identificacion = "";
    response.setContentType("text/html;charset=UTF-8");
    request.setCharacterEncoding("UTF-8");
    try {
        HttpSession sesionOk = request.getSession();
        String login = "LOGIN";
        login = (String) sesionOk.getAttribute("login");

        String tipoUsuario = (String) sesionOk.getAttribute("tipoUsuario");
        persona = (Persona) sesionOk.getAttribute("persona");
        identificacion = persona.getCedula();
        System.out.println("El estado del login es= " + login);
        System.out.println("El tipo de usuario es= " + tipoUsuario);
        if (login == null || tipoUsuario != "1") {
            //redireccionamos a la pagina del login
%>
<jsp:forward page="index.jsp" />

<% }
} catch (Exception e) {
    System.out.println("Error leyendo el archivo Principal del Administrador " + e.toString());
%>
<jsp:forward page="index.jsp" />

<%
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width" />
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Tangerine" />
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="css/logo-nav.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="css/main.css" />
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <script src="js/CargarIntercambioEstudiante.js"></script>
        <script src="js/jquery.js"></script>
        <script src="js/main.js"></script>
        <script src="js/vendor/bootstrap.js"></script>
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/vendor/jquery-1.10.1.min.js"></script>
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
         <script src="js/diseno.js"></script>
        <title>Principal Administrador</title>
</head>

<body style="margin-top: 0px; height: 100%;">
    <div align="right">
        <% //out.println("Usuario: "+persona.getNombre());%>
        <!--            <a href="./logout"><img src="imagenes/out.png" alt="" width="15" height="20" style="border:none" /></a>-->
    </div>
    <!--Barra de Navegacion-->
    <div class="container" align="center" style="margin-bottom: 50px;">
        <div class="row">
            <nav class="navbar navbar-default" style="width: 78%; font-size: 10px;">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Cambiar Navegacion</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="PrincipalAdmin.jsp?listarProfesores" class="navbar-brand" style="font-size: 11px; font-weight: bold;">Principal</a></div>

                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav">
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Profesores<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href='PrincipalAdmin.jsp?agregarProfesor' >Agregar</a></li>
                                <!--<li><a href="PrincipalAdmin.jsp?eliminarProfesor" >Eliminar</a></li>-->
                                <li><a href="PrincipalAdmin.jsp?listarProfesores" >Listar Profesores</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Semestres<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href='PrincipalAdmin.jsp?agregarSemestre' >Agregar</a></li>
                                <!--<li><a href="PrincipalAdmin.jsp?eliminarSemestre" >Eliminar</a></li>-->
                                <li><a href="PrincipalAdmin.jsp?ListarSemestres">Listar Semestres</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Instituciones<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href='PrincipalAdmin.jsp?agregarInstitucion' >Agregar</a></li>
                                <!--<li><a href="PrincipalAdmin.jsp?eliminarInstitucion" >Eliminar</a></li>-->
                                <li><a href="PrincipalAdmin.jsp?ListarInstituciones">Listar Instituciones</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Programas<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="PrincipalAdmin.jsp?agregarPrograma">Agregar</a></li>
                                <!--<li><a href="PrincipalAdmin.jsp?eliminarPrograma">Eliminar</a></li>-->
                                <li><a href="PrincipalAdmin.jsp?ListarProgramas">Listar Programas</a></li>
                            </ul>
                        </li>

                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Estudiantes<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <!--                                <li><a href="PrincipalAdmin.jsp?agregarEstudiante">Ingresar estudiante</a></li>-->
                                <li><a href="PrincipalAdmin.jsp?revisarComprobante">Revisar Comprobante</a></li>
                                <li><a href="PrincipalAdmin.jsp?revisarTareas">Revisar Tareas</a></li>
                                <li><a href="PrincipalAdmin.jsp?revisarMaterias">Revisar Materias</a></li>
                                <li><a href="PrincipalAdmin.jsp?ListarEstudiantes">Listar Estudiantes</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Tipos Intercambios<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="PrincipalAdmin.jsp?agregarIntercambio">Agregar Tipo Intercambio</a></li>
                                <li><a href="PrincipalAdmin.jsp?eliminarIntercambio">Eliminar Tipo Intercambio</a></li>
                                <li><a href="PrincipalAdmin.jsp?ListarTipoIntercambios">Listar Tipo Intercambios</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Consultas<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="PrincipalAdmin.jsp?estudiantexInstitucion">Estudiantes x Institución</a></li>
                                <li><a href="PrincipalAdmin.jsp?estudiantexPrograma">Estudiantes x Programa</a></li>
                                <li><a href="PrincipalAdmin.jsp?estudiantexTipoIntercambio">Estudiantes x T.Intercambio</a></li>
                                <li><a href="PrincipalAdmin.jsp?buscarEstudiante">Información Estudiante</a></li>
                            </ul>
                        </li>

                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="<%= request.getContextPath() + "/LogOutServlet"%>"><%=persona.getUsuario()%> - Cerrar Sesión</a></li>

                    </ul>
                </div>
            </nav>  
            <%
                if (!(request.getParameter("agregarInstitucion") == null)) {
            %>
            <%@include file="includes/RegistrarInstitucion.jsp" %>
            <%
            } else if (!(request.getParameter("eliminarInstitucion") == null)) {
            %>
            <%@include  file="includes/EliminarInstitucion.jsp" %>
            <%
            } else if (request.getParameter("ListarInstituciones") != null) {
                InstitucionDAO nuevo = new InstitucionDAO();

                List insti = nuevo.getInstituciones();
                request.setAttribute("instituciones", insti);
            %>
            <%@include  file="includes/ListarInstituciones.jsp" %>
            <% } else if (!(request.getParameter("EICorrectamente") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-success">La institución se eliminó correctamente</p>
            </div>
            <% } else if (!(request.getParameter("EIError") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">Ocurrio un error al intentar eliminar la institución</p>
            </div>
            <% } else if (!(request.getParameter("IICorrectamente") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-success">La institución se agregó correctamente</p>
            </div>
            <% } else if (!(request.getParameter("IIError") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">La institución no se pudo agregar</p>
            </div>
            <% } else if (!(request.getParameter("agregarPrograma") == null)) {
                ProfesorDAO managerProfesor = new ProfesorDAO();
                List prof = managerProfesor.getProfesores();
                request.setAttribute("profesores", prof);
            %>
            <%@include  file="includes/RegistrarProgramaAdmin.jsp" %>
            <% } else if (!(request.getParameter("IPCorrectamente") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-success">El programa se agregó correctamente</p>
            </div>
            <% } else if (!(request.getParameter("IPError") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">El programa no se pudo Ingresar</p>
            </div>
            <% } else if (!(request.getParameter("eliminarPrograma") == null)) {
                %>
            <%@include  file="includes/EliminarPrograma.jsp" %>
            <% } else if (!(request.getParameter("PECorrectamente") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-success">El programa se eliminó correctamente</p>
            </div>
            <% } else if (!(request.getParameter("PEError") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">El programa no se pudo eliminar</p>
            </div>
            <% } else if (request.getParameter("ListarProgramas") != null) {
                ProgramaDAO nuevo = new ProgramaDAO();
                ProfesorDAO managerProfesor = new ProfesorDAO();
                List prog = nuevo.getProgramas();
                List directores = managerProfesor.getDirectores(prog);
                request.setAttribute("programas", prog);
                request.setAttribute("directores", directores);
            %>
            <%@include  file="includes/ListarProgramasAdmin.jsp" %>
            <% } else if (!(request.getParameter("agregarIntercambio") == null)) {
            %>
            <%@include  file="includes/RegistrarIntercambio.jsp" %>
            <% } else if (!(request.getParameter("INTCorrectamente") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-success">El Intercambio se agregó correctamente</p>
            </div>
            <% } else if (!(request.getParameter("INTError") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">El Intercambio no se pudo Agregar</p>
            </div>
            <% } else if (!(request.getParameter("eliminarIntercambio") == null)) {
            %>
            <%@include  file="includes/EliminarTipoIntercambio.jsp" %>
            <% } else if (!(request.getParameter("INECorrectamente") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-success">El Intercambio se eliminó correctamente</p>
            </div>
            <% } else if (!(request.getParameter("INError") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">El Intercambio no se pudo Eliminar</p>
            </div>
            <% } else if (request.getParameter("ListarTipoIntercambios") != null) {
                IntercambioDAO nuevo = new IntercambioDAO();
                List inter = nuevo.getTipoIntercambios();
                request.setAttribute("intercambios", inter);
            %>
            <%@include  file="includes/ListarTipoIntercambios.jsp" %>
            <% } else if (!(request.getParameter("agregarEstudiante") == null)) {
                ProgramaDAO nuevo = new ProgramaDAO();
                List progr = nuevo.getProgramas(identificacion);
                request.setAttribute("programas", progr);
            %>
            <%@include  file="includes/IngresarEstudiante.jsp" %>
            <% } else if (!(request.getParameter("EstudianteCorrectamente") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-success">Estudiante Ingresado Correctamente</p>
            </div>
            <% } else if (!(request.getParameter("EstudianteError") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">Ocurrio un error al ingresar el estudiante, intente luego</p>
            </div>
            <% } else if (!(request.getParameter("faltanCampos") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">Ingrese Correctamente todos los campos del formulario</p>
            </div>
            <% } else if (!(request.getParameter("revisarMaterias") == null)) {
                EstudianteDAO managerEstudiante = new EstudianteDAO();
                List student = managerEstudiante.getEstudiantes2();
                request.setAttribute("estudiantes", student);
            %>
            <%@include  file="includes/RevisarMateria.jsp" %>
            <% } else if (!(request.getParameter("homologaciones") == null)) {
                //List homologaciones=(List)request.getAttribute("homologaciones");
                HttpSession sesionOk = request.getSession();
                List homologaciones = (List) sesionOk.getAttribute("materias");
                String modifi = (String) sesionOk.getAttribute("modificacion");
                if (modifi == "true") {
                    request.setAttribute("modificacion", "true");
                } else {
                    request.setAttribute("modificacion", "false");
                }
                request.setAttribute("materias", homologaciones);
            %>
            <%@include  file="includes/RevisarMateriasEstudianteAdmin.jsp" %>
            <% } else if (!(request.getParameter("agregarSemestre") == null)) {
            %>
            <%@include  file="includes/RegistrarSemestre.jsp" %>
            <% } else if (!(request.getParameter("SemestreCorrectamente") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-success">Semestre Ingresado Correctamente</p>
            </div>
            <% } else if (!(request.getParameter("SemestreError") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">El semestre no pudo ser ingresado</p>
            </div>
            <% } else if (request.getParameter("ListarSemestres") != null) {
                SemestreDAO managerSemestre = new SemestreDAO();

                List semest = managerSemestre.obtenerSemestres();
                request.setAttribute("semestres", semest);
            %>
            <%@include  file="includes/ListarSemestres.jsp" %>
            <% } else if (!(request.getParameter("eliminarSemestre") == null)) {
            %>
            <%@include  file="includes/EliminarSemestre.jsp" %>
            <% } else if (!(request.getParameter("ISECorrectamente") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-success">Semestre Eliminado Correctamente</p>
            </div>
            <% } else if (!(request.getParameter("PEError") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">El semestre no pudo Eliminar</p>
            </div>
            <% } else if (!(request.getParameter("revisarTareas") == null)) {

                EstudianteDAO managerEstudiante = new EstudianteDAO();
                List students = managerEstudiante.getEstudiantes2();
                request.setAttribute("estudiantes", students);
            %>

            <%@include  file="includes/RevisarTareas.jsp" %>
            <% } else if (!(request.getParameter("mostrarRevisiones") == null)) {
                HttpSession sesionOk = request.getSession();
                List revisiones = (List) sesionOk.getAttribute("revisiones");
                String modificaciones = (String) sesionOk.getAttribute("modificaRevisiones");
                if (modificaciones == "true") {
                    request.setAttribute("modificacion", "true");
                } else {
                    request.setAttribute("modificacion", "false");
                }
                request.setAttribute("revisiones", revisiones);
            %>

            <%@include  file="includes/MostrarTareasEstudianteAdmin.jsp" %>
            <% } else if (!(request.getParameter("ErrorMostrarTareas") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">Lo sentimos ocurrió un error sacando la información.</p>
            </div>
            <% } else if (request.getParameter("ListarEstudiantes") != null) {
                EstudianteDAO managerEstudiante = new EstudianteDAO();

                List students = managerEstudiante.getEstudiantes();
                request.setAttribute("estudiantes", students);
            %>
            <%@include  file="includes/ListarEstudianteAdmin.jsp" %>
            <% } else if (!(request.getParameter("codPrograma") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">El codigo del programa debe ser un valor numérico</p>
            </div>
            <% } else if (!(request.getParameter("revisarComprobante") == null)) {
                EstudianteDAO managerEstudiante = new EstudianteDAO();
                List student = managerEstudiante.getEstudiantes2();
                request.setAttribute("estudiantes", student);
            %>
            <%@include  file="includes/MostrarComprobante.jsp" %>
            <% } else if (!(request.getParameter("cedulaInvalida") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">La identificación del estudiante debe ser un valor numérico</p>
            </div>
            <% } else if (!(request.getParameter("estudianteExiste") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-info">Ya existe un estudiante con la identifcación ingresada.Verifique</p>
            </div>
            <% } else if (!(request.getParameter("usuarioAsignado") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-info">El usuario ya fué asignado a otra persona,debe Ingresar otro usuario </p>
            </div>
            <% } else if (request.getParameter("agregarProfesor") != null) {
            %>
            <%@include  file="includes/RegistrarProfesor.jsp" %>
            <% } else if (!(request.getParameter("errorIngresoLogin") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">Ocurrió un error al ingresar el profesor intente Luego</p>
            </div>
            <% } else if (!(request.getParameter("ingresoProfesorCorrecto") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-success">El profesor ha sido ingresado correctamente</p>
            </div>
            <% } else if (!(request.getParameter("noExisteProfesor") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-info">No existe un profesor con la Identificación ingresada</p>
            </div>
            <% } else if (!(request.getParameter("ProElimCorrectamente") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-success">El profesor se elimino Correctamente</p>
            </div>
            <% } else if (!(request.getParameter("ErrorEliProfesor") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">El profesor no se pudo eliminar intente luego</p>
            </div>
            <% } else if (request.getParameter("eliminarProfesor") != null) {
            %>
            <%@include  file="includes/EliminarProfesor.jsp" %>
            <% } else if (request.getParameter("listarProfesores") != null) {
                ProfesorDAO managerProfesor = new ProfesorDAO();

                List prof = managerProfesor.getProfesores();
                request.setAttribute("profesores", prof);
            %>
            <%@include  file="includes/ListarProfesores.jsp" %>
            <% } else if (!(request.getParameter("ErrorEstudiantesxInstitucion") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">Ocurrió un error retornando la información, notifiquelo al administrador</p>
            </div>
            <% } else if (!(request.getParameter("estudiantexInstitucion") == null)) {
                InstitucionDAO managerInstituciones = new InstitucionDAO();
                List insti = managerInstituciones.getInstituciones();
                request.setAttribute("instituciones", insti);
            %>
            <%@include  file="includes/EstudiantexInstitucion.jsp" %>
            <% } else if (!(request.getParameter("MostrarEstudiantesIntitucion") == null)) {
                HttpSession sesionOk = request.getSession();
                List estudianteInstitucion = (List) sesionOk.getAttribute("ListEstudianteIntercambio");

                request.setAttribute("estudianteInstitucion", estudianteInstitucion);
            %>

            <%@include  file="includes/ListarEstudiantexInstitucion.jsp" %>
            <% } else if (!(request.getParameter("estudiantexPrograma") == null)) {
                ProgramaDAO managerPrograma = new ProgramaDAO();
                List progra = managerPrograma.getProgramas();
                request.setAttribute("programas", progra);
            %>
            <%@include  file="includes/EstudiantexPrograma.jsp" %>
            <% } else if (!(request.getParameter("ErrorEstudiantesxPrograma") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">Ocurrió un error retornando la información, notifiquelo al administrador</p>
            </div>
            <% } else if (!(request.getParameter("MostrarEstudiantesPrograma") == null)) {
                HttpSession sesionOk = request.getSession();
                List estudiantePrograma = (List) sesionOk.getAttribute("ListEstudiantePrograma");

                request.setAttribute("estudiantePrograma", estudiantePrograma);
            %>

            <%@include  file="includes/ListarEstudiantexPrograma.jsp" %>
            <% } else if (!(request.getParameter("estudiantexTipoIntercambio") == null)) {
                TipoIntercambioDAO managerTipo = new TipoIntercambioDAO();
                List tipos = managerTipo.obtenerTipoIntercambios();
                request.setAttribute("tiposIntercambio", tipos);
            %>

            <%@include  file="includes/EstudiantesxTipoIntercambio.jsp" %>
            <% } else if (!(request.getParameter("ErrorEstudiantesxTipo") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">Ocurrió un error retornando la información, notifiquelo al administrador</p>
            </div>
            <% } else if (!(request.getParameter("MostrarEstudiantesTipo") == null)) {
                HttpSession sesionOk = request.getSession();
                List estudianteTipo = (List) sesionOk.getAttribute("ListEstudianteTipo");

                request.setAttribute("estudianteTipo", estudianteTipo);
            %>

            <%@include  file="includes/ListarEstudiantexTipoIntermcambio.jsp" %>
            <% } else if (!(request.getParameter("buscarEstudiante") == null)) {
            %>

            <%@include  file="includes/BuscarEstudiante.jsp" %>
            <% } else if (!(request.getParameter("ErrorEstudiantesxCedula") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">Ocurrió un error retornando la información, notifiquelo al administrador</p>
            </div>
            <% } else if (!(request.getParameter("MostrarEstudiantesCedula") == null)) {
                HttpSession sesionOk = request.getSession();
                List estudianteCedula = (List) sesionOk.getAttribute("estudianteCedula");

                request.setAttribute("estudianteCedula", estudianteCedula);
            %>

            <%@include  file="includes/MostrarEstudiante.jsp" %>
            <% } else if (!(request.getParameter("NoExisteEstudiantesCedula") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-success">No existe estudiante con la identificación ingresada</p>
            </div>
            <% } else if (!(request.getParameter("semestreExiste") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">Ya existe un semestre con los datos ingresados </p>
            </div>
            <% } else if (!(request.getParameter("modificarProfesor") == null)) {

            %>

            <%@include  file="includes/ModificarProfesor.jsp" %>
            <% } else if (!(request.getParameter("errorModificarProfesor") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">Ocurrió un error modificando la información del Profesor, Intente en otro momento </p>
            </div>
            <% } else if (!(request.getParameter("modificarSemestre") == null)) {
            %>

            <%@include  file="includes/ModificarSemestre.jsp" %>
            <% } else if (!(request.getParameter("errorModificarSemestre") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">Ocurrió un error modificando la información del Semestre, Intente en otro momento </p>
            </div>
            <% } else if (!(request.getParameter("modificarInstitucion") == null)) {
            %>

            <%@include  file="includes/ModificarInstitucion.jsp" %>
            <% } else if (!(request.getParameter("errorModificarIntitucion") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">Ocurrió un error modificando la información de la Institución, Intente en otro momento </p>
            </div>
            <% } else if (!(request.getParameter("modificarPrograma") == null)) {
                ProfesorDAO managerProfesor = new ProfesorDAO();
                List prof = managerProfesor.getProfesores();
                request.setAttribute("profesores", prof);
            %>

            <%@include  file="includes/ModificarPrograma.jsp" %>
            
            <% } else if (!(request.getParameter("errorModificarPrograma") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">Ocurrió un error modificando la información del programa, Intente en otro momento </p>
            </div>
            <% } else if (!(request.getParameter("modificarEstudiante") == null)) {
            %>

            <%@include  file="includes/ModificarEstudiante.jsp" %>
            <% } else if (!(request.getParameter("errorModificarEstudiante") == null)) {
            %>
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <br><br><br>
                <p class="bg-danger">Ocurrió un error modificando la información del Estudiante, Intente en otro momento </p>
            </div>
            <% } else {%>
            <div  style="height: 300px">

            </div>
            <% }%>
        </div>
    </div>    

    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

    <script src="js/vendor/bootstrap.js"></script>
    <script src="js/main.js"></script>
</body>
</html>

