<%-- 
    Document   : PrincipalEstudiante
    Created on : 09-jun-2014, 10:02:59
    Author     : Joaquin David Hernandez Cárdenas
    Comentario :  Esta es la vista principal del Profesor, desde aqui podrá realizar todas las operaciones que está autorizado
    a realizar.
--%> 
<%@page language="java" %>
<%@page  import="java.util.*" %>
<%@page import="udea.drai.intercambio.dto.Programa"%>
<%@page import="udea.drai.intercambio.dao.ProgramaDAO"%>
<%@page import="udea.drai.intercambio.dto.Persona"%>
<%@ page language="java" pageEncoding="UTF-8"%>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Información Personal</title>
        <link href="Internacionalizacion/css/bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="css/logo-nav.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="css/base/jquery-ui.css" />
        <script type="text/javascript" src="js/jquery.js" ></script>
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/vendor/jquery-1.10.1.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui.js"></script>
        <script type="text/javascript" src="js/vendor/bootstrap.js"></script>

 <script src="js/diseno.js"></script>
        <script type="text/javascript" src="js/datepicker-espanol.js"></script>
        <script>
            jQuery(document).ready(function() {
                $(".fecha").datepicker({
                    showOn: "button",
                    buttonImage: "img/calendar.jpg",
                    buttonImageOnly: true,
                    buttonText: "Select date",
                    format: "yyyy-mm-dd"
                });
            });


        </script>



        <%    HttpSession sesion = request.getSession();

            // sesion.setAttribute("temporal", null);
            if (sesion == null || request.getParameter("logout") != null) {
                sesion.setAttribute("login", null);
                session.invalidate();
        %>
        <jsp:forward page="index.jsp" /> 
        <%}
            Object log = sesion.getAttribute("login");
            String login = "";
            String tipoUsuario;

            Persona persona = (Persona) sesion.getAttribute("persona");

            if (log == null || sesion == null || persona == null) {
        %>
        <jsp:forward page="index.jsp" /> 
        <%
            }

            login = log.toString();
            tipoUsuario = (String) sesion.getAttribute("tipoUsuario");


        %> 
    </head>
    <body style="margin-top: 0px; height: 100%">
<script>
window.onload=function(){
var pos=window.name || 0;
window.scrollTo(0,pos);
}
window.onunload=function(){
window.name=self.pageYOffset || (document.documentElement.scrollTop+document.body.scrollTop);
}
</script>
        <div class="container" align="center">

            <nav class="navbar navbar-default" style="width: 79%;">
              
                <div class="navbar-header" >
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
             <span class="sr-only">Cambiar Navegacion</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                         <a href="PrincipalEstudiante.jsp?home" class="navbar-brand">Principal</a>
                </div>
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                        <ul class="nav navbar-nav">
                            <li ><a href="PrincipalEstudiante.jsp?EnviarMensaje" class="" >Contacto</a>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <%                          Programa pr = null;
                                if (login != null && login.equals("true")) {

                                    if (persona.getPrograma() != null) {
                                        String prog = persona.getPrograma();
                                        ProgramaDAO verificarP = new ProgramaDAO();
                                        pr = verificarP.obtenerNombre(prog);

                                    }

                            %>
 
                            <li><a href="PrincipalEstudiante.jsp?logout" style="font-size: 12px;"><%=persona.getUsuario()%> - Cerrar Sesión</a></li>
                                <% } else {%>
                            <li><a href="index.jsp">Acceder</a></li>
                                <% } %>
                        </ul>

                    </div>
            
            </nav>



            <%
                if (request.getParameter("modificado") != null) {
            %>
            <br/>
            <span style="color: #009900;" ><p>¡Estudiante modificado correctamente!</p></span>
            <%
            } else if (request.getParameter("error") != null) {
            %>
            <br/>
            <span style="color: crimson;" ><p>¡Contraseña ingresada incorrecta!</p></span>
            <%
            } else if (request.getParameter("home") != null || request.getParameter("close") != null) {
                sesion.setAttribute("archTem", "N");
                sesion.setAttribute("arch", null);
            } else if (request.getParameter("cambioExitoso") != null) {
            %>
            <span style="color: #009900;" ><p>¡Contraseña modificada correctamente!</p></span>
            <%
            } else if (request.getParameter("cambioFallido") != null) {
            %>
            <span style="color: crimson;" ><p>¡Problema realizando el cambio de contraseña!</p></span>
            <%
            } else if (request.getParameter("claveIncorrecta") != null) {
            %>
            <span style="color: crimson;" ><p>¡Contraseña incorrecta!</p></span>
            <%
            } else if (request.getParameter("Send") != null) {

                if (request.getParameter("Send").equalsIgnoreCase("yes")) {
            %>
            <span style="color: #009900;" ><p>¡Mensaje enviado correctamente!</p></span>
            <% } else { %>
            <span style="color: crimson;" ><p>¡Problemas enviando mensaje!</p></span>
            <%
                    }
                }

                if (request.getParameter("intercambios") != null) {
            %>
            <%@include file ="includes/Intercambio.jsp" %>
            <%
            } else if (request.getParameter("Bitacora") != null) {
            %>
            <%@include file ="includes/Bitacora.jsp" %>

            <%
            } else if (request.getParameter("newPassword") != null) {
            %>
            <%@include file ="includes/CambiarContrasena.jsp" %>
            <%
            } else if (request.getParameter("EnviarMensaje") != null) {
            %>
            <%@include file ="includes/EnviarMensaje.jsp" %>
            <% } else {       %>
            <%@include file="includes/InformacionPersonal.jsp" %>
            <%
                }
            %>

        </div>




    </body>
</html>
