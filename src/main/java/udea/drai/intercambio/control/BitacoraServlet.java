/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udea.drai.intercambio.control;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import udea.drai.intercambio.dao.HomologacionDAO;
import udea.drai.intercambio.dto.Homologacion;

/**
 *
 * @author Usuario
 * Crea el historial de modificaciones de las materias que tiene matriculadas un estudiante.
 */
public class BitacoraServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        String codAnt = request.getParameter("codAnt");
        String codDes = request.getParameter("codDesp");
        HomologacionDAO hDAO = new HomologacionDAO();
        Homologacion hmAnt = new Homologacion();
        if(codAnt != null && !codAnt.equals("")){
        hmAnt.setCodigo(Integer.parseInt(codAnt));
        }else{
            hmAnt.setCodigo(0);
        }
        hDAO.cargarHomologacion(hmAnt);
        Homologacion hmDes = new Homologacion();
        if(codDes != null && !codDes.equals("")){
            hmDes.setCodigo(Integer.parseInt(codDes));
        }else{
            hmDes.setCodigo(0);
        }
        hDAO.cargarHomologacion(hmDes);
        StringBuffer combo = new StringBuffer();

        combo.append("<!DOCTYPE html>");
        combo.append("<html>");
        combo.append("<head>");
        combo.append("<title>Tabla homologacion</title>");
        combo.append("</head>");
        combo.append("<body>");
        combo.append("<table class='table'>");
        combo.append("<tr>");
        combo.append("<th>Codigo Materia Local Anterior</th>");
        combo.append("<th>Nombre Materia Local Anterior</th>");
        combo.append("<th>Codigo Materia Extranjero Anterior</th>");
        combo.append("<th>Nombre Materia Extranjero Anterior</th>");
        combo.append("</tr>");
        combo.append("<tr>");
        combo.append("<td>" + hmAnt.getCodLocal() + "</td>");
        combo.append("<td>" + hmAnt.getMateriaLocal() + "</td>");
        combo.append("<td>" + hmAnt.getCodExtranjero() + "</td>");
        combo.append("<td>" + hmAnt.getMateriaExtranjero() + "</td>");
        combo.append("</tr>");
        combo.append("<tr>");
        combo.append("<th>Codigo Materia Local Nueva</th>");
        combo.append("<th>Nombre Materia Local Nueva</th>");
        combo.append("<th>Codigo Materia Extranjero Nueva</th>");
        combo.append("<th>Nombre Materia Extranjero Nueva</th>");
        combo.append("</tr>");
        combo.append("<tr>");
        combo.append("<td>" + hmDes.getCodLocal() + "</td>");
        combo.append("<td>" + hmDes.getMateriaLocal() + "</td>");
        combo.append("<td>" + hmDes.getCodExtranjero() + "</td>");
        combo.append("<td>" + hmDes.getMateriaExtranjero() + "</td>");
        combo.append("</tr>");
        combo.append("</table>");
        combo.append("</body>");
        combo.append("</html>");
        out.print(combo.toString());
        System.out.println(combo.toString());
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter out = response.getWriter();

		// Obtengo los datos de la peticion
		/*int codigo = request.getParameter("codigo");

         // Compruebo que los campos del formulario tienen datos para añadir a la tabla
         if (!nombre.equals("") && !apellido.equals("") && !edad.equals("")) {
         // Creo el objeto persona y lo añado al arrayList
         Persona persona = new Persona(nombre, apellido, edad);
         personas.add(persona);
         }

         combo.append("<table style= cellspacing=\"1\" bgcolor=\"#0099cc\">");
         combo.append("<tr>");
         combo.append("<td style= rowspan=\"7\" align=\"center\" bgcolor=\"#f8f8f8\"> NOMBRE </td>");			
         combo.append("<td style= rowspan=\"7\" align=\"center\" bgcolor=\"#f8f8f8\">APELLIDO</td>");
         combo.append("<td style= rowspan=\"7\" align=\"center\" bgcolor=\"#f8f8f8\">EDAD</td>");
         combo.append("</tr>");
         for(int i=0; i<personas.size(); i++){
         Persona persona = personas.get(i);
         combo.append("<tr>");
         combo.append("<td style= rowspan=\"7\" align=\"center\" bgcolor=\"#f8f8f8\">"+persona.getNombre()+"</td>");			
         combo.append("<td style= rowspan=\"7\" align=\"center\" bgcolor=\"#f8f8f8\">"+persona.getApellido()+"</td>");
         combo.append("<td style= rowspan=\"7\" align=\"center\" bgcolor=\"#f8f8f8\">"+persona.getEdad()+"</td>");
         combo.append("</tr>");
         }
         combo.append("</table>");
         processRequest(request, response);*/
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
