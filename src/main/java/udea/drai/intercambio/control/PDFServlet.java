    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udea.drai.intercambio.control;

import udea.drai.intercambio.dto.FormatoDocumento;
import udea.drai.intercambio.dto.InfoCompromiso;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.OutputStream;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Daniel Resptrepo Arcila
 * Servlet que permite al profesor y a los estudiantes descargar en formato .PDF el compromiso que ambos 
 * deben firmar.
 */
public class PDFServlet extends HttpServlet {

    // <editor-fold defaultstate='collapsed' desc='HttpServlet methods. Click on the + sign on the left to edit the code.'>
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         try {        
            HttpSession sesion = request.getSession();
            if (sesion == null || request.getParameter("logout") != null) {
                sesion.setAttribute("login", null);
                response.sendRedirect("/index.jsp");
            }
       
            InfoCompromiso info = new InfoCompromiso();
            if (sesion.getAttribute("compromiso") != null) {
                info = (InfoCompromiso) sesion.getAttribute("compromiso");
            }else{
                response.sendRedirect("/PrincipalEstudiante.jsp");
            }
            response.setContentType("application/pdf");
           //  response.setHeader("Content-Disposition","attachment;filename=PDFServlet.pdf" );
            //Get the output stream for writing PDF object        
            OutputStream out = response.getOutputStream();
            
            Document document = new Document();
            /* Basic PDF Creation inside servlet */
            PdfWriter writer = PdfWriter.getInstance(document, out);
          //  PdfWriter.getInstance(document, out);
          
           
                 String relativeWebPath = "/images/fondo4.png";
            String absoluteDiskPath = getServletContext().getRealPath(relativeWebPath);
              FormatoDocumento encabezado = new FormatoDocumento(absoluteDiskPath);
              writer.setPageEvent(encabezado);
               document.open();
      
            
            Font fuente = new Font();
            fuente.isBold();
            fuente.setSize(25);
            Paragraph p = new Paragraph("Universidad de Antioquia", fuente);
            p.setAlignment(1);
            document.add(p);
            fuente.setSize(18);
            p = new Paragraph("Facultad de Ingeniería", fuente);
            p.setAlignment(1);
            document.add(p);
            p = new Paragraph("Programa de Internacionalización ", fuente);
            p.setAlignment(1);
            document.add(p);
            fuente.setSize(14);
            document.add(new Paragraph(" "));
            PdfPTable tabla = new PdfPTable(2);
             PdfPCell celda =null ;
             celda = new PdfPCell(new Paragraph(" "));
                        celda.setColspan(2);
                        celda.setRowspan(2);
                        celda.setBorder(Rectangle.BOTTOM);
                        celda.setBorderWidthBottom(5);
                        celda.setBorderColorBottom(new BaseColor(10, 104, 19));
                        tabla.addCell(celda);
            //tabla.getDefaultCell().setBorder(0);
         
            fuente.setSize(15);
            p =new Paragraph("Programa:",fuente);
            p.setAlignment(1);
            celda = new PdfPCell(p);
            celda.setBorder(0);
            tabla.addCell(celda);
            celda = new PdfPCell(new Paragraph(info.getPrograma(), fuente));
             celda.setBorder(0);
            tabla.addCell(celda);
            p =new Paragraph("Nombre:",fuente);
            celda = new PdfPCell(p);
             celda.setBorder(0);
            tabla.addCell(celda);
            celda = new PdfPCell(new Paragraph(info.getEstudiante(), fuente));
             celda.setBorder(0);
            tabla.addCell(celda);
            p =new Paragraph("Apellido:",fuente);
            celda = new PdfPCell(p);
             celda.setBorder(0);
            tabla.addCell(celda);
            celda = new PdfPCell(new Paragraph(info.getApellido(), fuente));
             celda.setBorder(0);
            tabla.addCell(celda);
            p =new Paragraph("Identificación:",fuente);
            celda = new PdfPCell(p);
             celda.setBorder(0);
            tabla.addCell(celda);
            celda = new PdfPCell(new Paragraph(info.getIdEstudiante(), fuente));
             celda.setBorder(0);
            tabla.addCell(celda);
            p =new Paragraph("Universidad de Destino:",fuente);
            celda = new PdfPCell(p);
             celda.setBorder(0);
            tabla.addCell(celda);
            celda = new PdfPCell(new Paragraph(info.getInstDestino(), fuente));
             celda.setBorder(0);
            tabla.addCell(celda);
            p =new Paragraph("Email:",fuente);
            celda = new PdfPCell(p);
             celda.setBorder(0);
            tabla.addCell(celda);
            celda = new PdfPCell(new Paragraph(info.getEmail(), fuente));
             celda.setBorder(0);
            tabla.addCell(celda);
            p =new Paragraph("Semestre de Inicio:",fuente);
            celda = new PdfPCell(p);
             celda.setBorder(0);
            tabla.addCell(celda);
            celda = new PdfPCell(new Paragraph(info.getSemInicio(), fuente));
             celda.setBorder(0);
            tabla.addCell(celda);
            p =new Paragraph("Semestre de Finalización:",fuente);
            celda = new PdfPCell(p);
             celda.setBorder(0);
            tabla.addCell(celda);
            celda = new PdfPCell(new Paragraph(info.getSemFin(), fuente));
             celda.setBorder(0);
            tabla.addCell(celda);
            p =new Paragraph("Fecha de viaje:",fuente);
            celda = new PdfPCell(p);
             celda.setBorder(0);
            tabla.addCell(celda);
            celda = new PdfPCell(new Paragraph(info.getIda(), fuente));
             celda.setBorder(0);
            tabla.addCell(celda);
            p =new Paragraph("Fecha esperada de Retorno:",fuente);
            celda = new PdfPCell(p);
             celda.setBorder(0);
            tabla.addCell(celda);
            celda = new PdfPCell(new Paragraph(info.getVuelta(), fuente));
             celda.setBorder(0);
            tabla.addCell(celda);
            
            //document.add(new Paragraph("  "));
            celda = new PdfPCell(new Paragraph(" "));
            celda.setRowspan(2);
            celda.setColspan(2);
            celda.setBorder(0);
            tabla.addCell(celda);
            fuente.isBold();
           /* celda = new PdfPCell(new Paragraph("Materias Matriculadas",fuente));
             celda.setBorder(0);
             celda.setColspan(2);
             tabla.addCell(celda);*/
               celda = new PdfPCell(new Paragraph(" "));
            celda.setRowspan(2);
            celda.setColspan(2);
            celda.setBorder(0);
            tabla.addCell(celda);
         /*
            HomologacionDAO hDAO = new HomologacionDAO();
            SemestreDAO sDAO =new  SemestreDAO();
             ArrayList hm = hDAO.homologacionCompromiso(info.getIntercambio().getCodigo());
            Iterator it = hm.iterator();
            String fechTemp="";
            int temp=0,sem=0;
             Font fuente2 = new Font();
            fuente2.setSize(10);
            while(it.hasNext()){
            Object ob = it.next();
            if(ob != null){
            Homologacion hh = (Homologacion) ob ;
       
                    if(hh.getActa() != 0 && hh.getFechaActa() != null && hh.getActa() != temp && temp != 0){
                        p = new Paragraph("Acta – Resolución Comité de Carrera (Fecha:"+hh.getFechaActa().toString()+" No:"+hh.getActa()+")", fuente2);
                        celda = new PdfPCell(p);
                        celda.setBorder(0);
                        celda.setColspan(2);
                        tabla.addCell(celda);
                        celda = new PdfPCell(new Paragraph(" "));
                        celda.setRowspan(2);
                        celda.setColspan(2);
                        celda.setBorder(0);
                        tabla.addCell(celda);
                        }
                        if(hh.getSemestre() != sem){
                        Semestre se = new Semestre();

                        se.setCodigo(hh.getSemestre());
                        sDAO.cargarSemestre(se);
                        fuente.isBold();
                        celda = new PdfPCell(new Paragraph("Materias Matriculadas "+se.getAno()+ " - "+ se.getPeriodo(), fuente));
                        celda.setBorder(0);
                        celda.setColspan(2);
                        celda.setRowspan(2);
                        tabla.addCell(celda);
                        celda = new PdfPCell(new Paragraph(" "));
                        celda.setColspan(2);
                        celda.setRowspan(2);
                        celda.setBorder(Rectangle.BOTTOM);
                        celda.setBorderWidthBottom(5);
                        celda.setBorderColorBottom(new BaseColor(10, 104, 19));
                        tabla.addCell(celda);
                        celda = new PdfPCell(new Paragraph("Universidad Origen",fuente));
                        celda.setBorder(0);
                        tabla.addCell(celda);
                        celda = new PdfPCell(new Paragraph("Universidad Destino", fuente));
                        celda.setBorder(0);
                        tabla.addCell(celda);
                        celda = new PdfPCell(new Paragraph("Codigo - Nombre", fuente));
                        celda.setBorder(0);
                        tabla.addCell(celda);
                       tabla.addCell(celda);
                    }
                 
                       celda = new PdfPCell(new Paragraph(hh.getCodLocal()+" - "+hh.getMateriaLocal()));
                       celda.setBorder(0);
                       tabla.addCell(celda);
                       celda = new PdfPCell(new Paragraph(hh.getCodExtranjero()+" - "+ hh.getMateriaExtranjero()));
                       celda.setBorder(0);
                       tabla.addCell(celda);
               
                    temp = hh.getActa();
                    if(hh.getFechaActa() != null){
                        fechTemp= hh.getFechaActa().toString();
                    }else{
                        fechTemp = null;
                    }
                    sem = hh.getSemestre();
            }
            }
           if(fechTemp != null && temp != 0){
            p = new Paragraph("Acta – Resolución Comité de Carrera (Fecha:"+fechTemp+" No:"+temp+")", fuente2);
                        celda = new PdfPCell(p);
                        celda.setBorder(0);
                        celda.setColspan(2);
                        tabla.addCell(celda);
           }
           
           */
                          celda = new PdfPCell(new Paragraph(" "));
            celda.setRowspan(2);
            celda.setColspan(2);
            celda.setBorder(0);
            tabla.addCell(celda);
             celda = new PdfPCell(new Paragraph("Compromiso", fuente));
            celda.setBorder(0);
            celda.setColspan(2);
            celda.setRowspan(2);
            tabla.addCell(celda);
            celda = new PdfPCell(new Paragraph(" "));
            celda.setColspan(2);
            celda.setRowspan(2);
            celda.setBorder(Rectangle.BOTTOM);
            celda.setBorderWidthBottom(5);
            celda.setBorderColorBottom(new BaseColor(10, 104, 19));
            tabla.addCell(celda);
            p = new Paragraph("- Enviar en las fechas estipuladas por el comité de Internacionalización las notas de los "
                    + "cursos asistidos en la Universidad de Destino. \n \n");
            celda = new PdfPCell(p);
            celda.setBorder(0);
            celda.setColspan(2);
            celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            //  celda.setRowspan(45);
            tabla.addCell(celda);
            p = new Paragraph("- Informar oportunamente (5 días calendario de sucedido el evento) cualquier novedad "
                    + "ocurrida, referente a los cursos matriculados en la Universidad e destino, tales como "
                    + "cancelaciones, perdidas de materias, entre otras. \n \n");
            celda = new PdfPCell(p);
            celda.setBorder(0);
            celda.setColspan(2);
            celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            //  celda.setRowspan(45);
            tabla.addCell(celda);
            p = new Paragraph(" - Definir cada semestre claramente las materias que se van a matricular en la "
                    + " Universidad Origen, basado en posibles cambios en la oferta de materias en la  "
                    + "Universidad de Destino (según fechas establecidas en el calendario académico de "
                    + "la Facultad de Ingeniería de la Universidad de Antioquia \n \n");
            celda = new PdfPCell(p);
            celda.setBorder(0);
            celda.setColspan(2);
            celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            //  celda.setRowspan(45);
            tabla.addCell(celda);
            p = new Paragraph("– Será responsabilidad el estudiante estar pendiente de dichas fechas y comunicación). El no envío de dicha "
                    + "información puede ocasionar que el estudiante quede como desertor de la Universidad "
                    + "de Antioquia y en ningún motivo será responsabilidad del departamento o del "
                    + "coordinador de Internacionalización tal novedad. \n \n");
            celda = new PdfPCell(p);
            celda.setBorder(0);
            celda.setColspan(2);
            celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            //  celda.setRowspan(45);
            tabla.addCell(celda);

            p = new Paragraph("- Es responsabilidad del coordinar de Internacionalización dar respuesta en 5 días "
                    + "hábiles a los comentarios, dudas y sugerencias de los estudiantes en pasantía o doble "
                    + "titulación. En algunos casos dichas respuestas pueden tomar más tiempo en caso que "
                    + "las consultas deban esperar reuniones del comité de carrera o consejo de Facultad. \n \n");
            celda = new PdfPCell(p);
            celda.setBorder(0);
            celda.setColspan(2);
            celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            //  celda.setRowspan(45);
            tabla.addCell(celda);
            String st = "- Es responsabilidad del coordinador de Internacionalización matricular oportunamente "
                    + "los estudiantes que envíen a tiempo (Mínimo 2 días antes del cierre de matrículas en "
                    + "el sistema MARES) la oferta de matrículas e igualmente ingresar las notas al sistema "
                    + "mares en los plazos establecidos, siempre y cuando el estudiante envíe dichas notas "
                    + "en los plazos estipulados para ello. \n \n ";
            Chunk c = new Chunk(st);
            Phrase ph = new Phrase();
            ph.add(c);
            Paragraph p1 = new Paragraph();
            p1.add(ph);
            celda = new PdfPCell(p1);
            celda.setBorder(0);
            celda.setColspan(2);
            celda.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
            //  celda.setRowspan(45);
            tabla.addCell(celda);

            p = new Paragraph("Por el Departamento \n \n \n \n");
            celda = new PdfPCell(p);
            celda.setBorder(0);
            celda.setColspan(1);
            celda.setRowspan(4);
            tabla.addCell(celda);
            p = new Paragraph(" El Estudiante \n \n \n \n");
            celda = new PdfPCell(p);
            celda.setBorder(0);
            celda.setColspan(1);
            celda.setRowspan(4);
            tabla.addCell(celda);
            p = new Paragraph(" ________________________ ");
            celda = new PdfPCell(p);
            celda.setBorder(0);
            celda.setColspan(1);
            tabla.addCell(celda);

            tabla.addCell(celda);

            p = new Paragraph(info.getNombreProfesor()+" \n");
            celda = new PdfPCell(p);
            celda.setBorder(0);
            celda.setColspan(1);
            tabla.addCell(celda);

            p = new Paragraph(info.getEstudiante() + " "+info.getApellido());
            celda = new PdfPCell(p);
            celda.setBorder(0);
            celda.setColspan(1);
            tabla.addCell(celda);

            p = new Paragraph("C.C. ");
            celda = new PdfPCell(p);
            celda.setBorder(0);
            celda.setColspan(1);
            tabla.addCell(celda);
            p = new Paragraph("C.C. ");
            celda = new PdfPCell(p);
            celda.setBorder(0);
            celda.setColspan(1);
            tabla.addCell(celda);
            p = new Paragraph("Coordinador(a)");
            celda = new PdfPCell(p);
            celda.setBorder(0);
            celda.setColspan(1);
            tabla.addCell(celda);
            p = new Paragraph("Estudiante ");
            celda = new PdfPCell(p);
            celda.setBorder(0);
            celda.setColspan(1);
            tabla.addCell(celda);
            document.add(tabla);
            document.close();
   
            
        } catch (DocumentException ex) {
            Logger.getLogger(PDFServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
     
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
    }
   

}


