

package udea.drai.intercambio.control;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import udea.drai.intercambio.dao.RevisionDAO;

/**
 * Servlet encargado de modificar el estado de las tareas de un estudiante.
 * @author Joaquin David Hernandez Cardenas. <jdavidhc94@gmail.com>
 * 
 */
public class ModificarRevisionV2 extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        processRequest(request, response);
        String url = "";
        String codRevision = request.getParameter("codigoRevision");
        String estudiante = request.getParameter("estudiante");
        String intercambio = request.getParameter("intercambio");
        String comentario = request.getParameter("comentario");
        int inter = Integer.parseInt(intercambio);
        int cod = Integer.parseInt(codRevision);
        String estado = request.getParameter("estado");
        RevisionDAO managerRevision = new RevisionDAO();
        int resultado = managerRevision.modificarEstadoRevision(cod, estado, comentario);
        if (resultado == 1) {
            List revisiones = managerRevision.obtenerRevisiones2(estudiante, inter);
            if (revisiones != null) {
                HttpSession sesionOk = request.getSession();
                sesionOk.setAttribute("revisiones", revisiones);
                sesionOk.setAttribute("modificaRevisiones", "true");
                url = "./Principal.jsp?mostrarRevisiones";
                response.setHeader("Location", url);
                response.sendRedirect(url);
            }
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
