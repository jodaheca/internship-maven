/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udea.drai.intercambio.control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import udea.drai.intercambio.dao.IntercambioDAO;

/**
 *
 * @author Joaquin David Hernádez Cárdeas
 * Servlet mediante el cual se puede ingresar un nuevo Tipo de Intercambio al sistema
 */
public class IngresarIntercambioServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
             response.setContentType("text/html;charset=UTF-8");
         request.setCharacterEncoding("UTF-8");
        String url = "./PrincipalAdmin.jsp";
        String nombre = request.getParameter("nombre");
        String numSemestres = request.getParameter("numSemestres");

        IntercambioDAO agregar = new IntercambioDAO();
        int resultado = agregar.agregarIntercambio(nombre, numSemestres);
        if (resultado == 1) {
            url = "./PrincipalAdmin.jsp?INTCorrectamente";
            response.setHeader("Location", url);
            response.sendRedirect(url);

        } else {
            url = "./PrincipalAdmin.jsp?INTError";
            response.setHeader("Location", url);
            response.sendRedirect(url);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
