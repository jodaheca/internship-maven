/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udea.drai.intercambio.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import udea.drai.intercambio.dao.EstudianteDAO;
import udea.drai.intercambio.dto.Persona;

/**
 *
 * @author Joaquin David Hernández Cárdenas
 * Servlet mediante el cual se retorna una lista de los estudiantes que estan de intercabio en otras instituciones
 * y que tienen en común pertenecer al mismo programa académico.
 */
public class RecuperarEstudiantesxPrograma extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        processRequest(request, response);

        String url = "";
        String codPrograma = request.getParameter("programa");
        HttpSession sesionOK = request.getSession();
        String tipoUsuario = (String) sesionOK.getAttribute("tipoUsuario");
        EstudianteDAO managerEstudiante = new EstudianteDAO();
        List resultadoFinal = new ArrayList();
        //Entro como Administrador
        if (tipoUsuario == "1") {
            resultadoFinal = managerEstudiante.getEstudiantexPrograma(codPrograma);
            if (resultadoFinal == null) {
                url = "./PrincipalAdmin.jsp?ErrorEstudiantesxPrograma";
                response.setHeader("Location", url);
                response.sendRedirect(url);
                return;
            }
            sesionOK.setAttribute("ListEstudiantePrograma", resultadoFinal);
            url = "./PrincipalAdmin.jsp?MostrarEstudiantesPrograma";
            response.setHeader("Location", url);
            response.sendRedirect(url);
            // Entro como Profesor
        } else if (tipoUsuario == "2") {
            Persona profesor = (Persona) sesionOK.getAttribute("persona");
            String cedulaProfesor = profesor.getCedula();
            resultadoFinal = managerEstudiante.getEstudiantexPrograma2(codPrograma, cedulaProfesor);
            if (resultadoFinal == null) {
                url = "./Principal.jsp?ErrorEstudiantesxPrograma";
                response.setHeader("Location", url);
                response.sendRedirect(url);
                return;
            }
            sesionOK.setAttribute("ListEstudiantePrograma", resultadoFinal);
            url = "./Principal.jsp?MostrarEstudiantesPrograma";
            response.setHeader("Location", url);
            response.sendRedirect(url);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
