/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udea.drai.intercambio.control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import udea.drai.intercambio.dao.IntercambioDAO;
import udea.drai.intercambio.dao.SemestreDAO;
import udea.drai.intercambio.dto.Intercambio;
import udea.drai.intercambio.dto.Persona;
import udea.drai.intercambio.dto.Semestre;

/**
 *
 * @author Daniel   Restrepo Arcila
 * Este servlet le ermite al estudiante registrar un nuevo intercambio en el sistema.
 */
public class intercambioServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
             response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        SemestreDAO managerSemestre= new SemestreDAO();
        HttpSession sesion = request.getSession();
        String codigo = "0";
        if (sesion.getAttribute("temporal") != null) {
            codigo = request.getParameter("intercambios");
        }
        String tipo = request.getParameter("tipo");
        String origen = request.getParameter("origen");
        String destino = request.getParameter("destino");
        String inicio = request.getParameter("semestreInicio");
        String fin = request.getParameter("semestreFin");
        String ida = request.getParameter("fechaViaje");
        String regreso = request.getParameter("fechaRegreso");
        Persona e = (Persona) sesion.getAttribute("persona");
        System.out.println(ida);

        Intercambio i = new Intercambio();
        if(codigo != null){
            i.setCodigo(Integer.parseInt(codigo));
        }else{
            i.setCodigo(0);
        }
        i.setTipo(Integer.parseInt(tipo));
        i.setOrigen(Integer.parseInt(origen));
        i.setDestino(Integer.parseInt(destino));
        Semestre inicial = managerSemestre.obtenerSemestreCodigo(Integer.parseInt(inicio));
        Semestre fina = managerSemestre.obtenerSemestreCodigo(Integer.parseInt(fin));
        if ((inicial.getAno() > fina.getAno()) || ((inicial.getAno() == fina.getAno()) &&(inicial.getPeriodo() > fina.getPeriodo())) || (inicial.getCodigo() == fina.getCodigo())){
            String  url = "./PrincipalEstudiante.jsp?intercambios&mensaje=3";
            response.setHeader("Location", url);
            response.sendRedirect(url);
            return;
            
        }
        
        i.setInicio(inicial.getCodigo());
        i.setFin(fina.getCodigo());
        
        
        i.setIda(ida);
        i.setRegreso(regreso);
        i.setEstudiante(e.getCedula());

        IntercambioDAO iDAO = new IntercambioDAO();
         String url= "" ;
         int result = iDAO.ingresarIntercambio(i);
        if (result!=0) {
           url = "./PrincipalEstudiante.jsp?intercambios="+result+"&mensaje=1";
        } else {
          url = "./PrincipalEstudiante.jsp?intercambios&mensaje=2";

        }
       
        response.setHeader("Location", url);
        response.sendRedirect(url);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
