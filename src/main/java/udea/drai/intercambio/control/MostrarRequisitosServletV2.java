package udea.drai.intercambio.control;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import udea.drai.intercambio.dao.EstudianteDAO;
import udea.drai.intercambio.dao.IntercambioDAO;
import udea.drai.intercambio.dao.RequisitoDAO;
import udea.drai.intercambio.dao.RevisionDAO;
import udea.drai.intercambio.dto.Intercambio;
import udea.drai.intercambio.dto.Persona;
import udea.drai.intercambio.dto.Semestre;
import udea.drai.intercambio.util.IntercambioUtil;
import udea.drai.intercambio.util.SemestreUtil;

/**
 *
 * Servlet en el cual se recibe la peticion para visualizar las tareas que debe
 * cumplir un estudiante en un intercambio en una Universidad Extrangera.
 *
 * @author Joaquin Hernandez Cardenas <jdavidhc94@gmail.com>
 */
public class MostrarRequisitosServletV2 extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Metodo Post en el que vamos a recibir la petición, recibe como parametro
     * la identificación del estudiante.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");

        processRequest(request, response);
        HttpSession sesionOk = request.getSession();
        String tipoUsuario = (String) sesionOk.getAttribute("tipoUsuario");
        /**
         * Calculamos el semestre actual, para verificar que el intercambio
         * obtenido esta vigente en el semestre que estamos
         */
        SemestreUtil managerSemestre = new SemestreUtil();
        String semestreActual = managerSemestre.calcularSemestre();
        // System.out.println("Semestre calculado: " + semestre );

        String identificacion = request.getParameter("identificacion");
        EstudianteDAO managerEastudiante = new EstudianteDAO();
        Persona estudiante = managerEastudiante.getEstudianteXcedula(identificacion);
        String nombreEstudiante = estudiante.getNombre() + " " + estudiante.getApellido();
        String infoIntercambio = estudiante.getNombreIntercambio();
        IntercambioDAO managerIntercambio = new IntercambioDAO();
        List intercambios = managerIntercambio.ObtenerintercambiosXEstudiante(identificacion);
        String url;

        /**
         * Si la lista esta vacia es porque el estudiante no tiene intercambios
         * registrados
         */
        if (intercambios.isEmpty()) {
            url = "./Principal.jsp?NoTieneIntercambio";
            response.setHeader("Location", url);
            response.sendRedirect(url);
            return;
        }
        /**
         * obtenemos el primer intercambio, el cual debe ser el actual
         */
        Intercambio intercambio = (Intercambio) intercambios.get(0);

        IntercambioUtil interUtil = new IntercambioUtil();
        List semestres = interUtil.obtenerSemestre(intercambio.getCodigo());
        
        Semestre sem =(Semestre) semestres.get(0);
        String infoSemestres = sem.getAno() + "-" + sem.getPeriodo();
        int longitudList = semestres.size();
        sem =(Semestre) semestres.get(longitudList-1);
        
        infoSemestres = infoSemestres + " A " + sem.getAno() + "-" + sem.getPeriodo();
        
        infoIntercambio = infoIntercambio + " DE " + infoSemestres;
        
        System.out.println("Informacion Intercambio: " + infoIntercambio);
        /**
         * Se crea la variable interVigente para verificar si el intercambio
         * obtenido contiene el semestre en el que estamos, es decir que el
         * estudiante si está de intermcabio este semestre.
         */
        String interVigente = "false";
        for (Object semes : semestres) {
            Semestre sm = (Semestre) semes;
            String nombre = sm.getAno() + "-" + sm.getPeriodo();
            if (semestreActual.equalsIgnoreCase(nombre)) {
                interVigente = "true";
                break;
            }
            System.out.println("Semestre: " + nombre);
        }
        /**
         * Ahora obtengo las revisiones del intercambio para poder mostrarlas al
         * profesor
         */
        RevisionDAO managerRevision = new RevisionDAO();
        List revisiones = managerRevision.obtenerRevisiones(identificacion, intercambio.getCodigo());
        if (revisiones == null) {
            System.out.println("Error recuperando las revisones");
            url = "./Principal.jsp?ErrorMostrarTareas";
            response.setHeader("Location", url);
            response.sendRedirect(url);
            return;
        }
        /**
         * Si la consulta es vacia entonces es necesario ingresar las revisiones
         * a la DB
         */
        if (revisiones.isEmpty()) {
            RequisitoDAO managerRequisito = new RequisitoDAO();
            List requisitos = managerRequisito.getRequisitos();
            if (requisitos == null) {
                url = "./Principal.jsp?ErrorMostrarTareas";
                response.setHeader("Location", url);
                response.sendRedirect(url);
                System.out.println("Error al sacar los requisitos de la DB");
                return;
            }
            // Ingreso todas las revisiones a la DB
            int result = managerRevision.ingresarRevisones(requisitos, intercambio.getCodigo(), identificacion);
            if (result == 1) {
                // Sacar las revisiones de la tabla para poder mostrarlas al profesor
                revisiones = managerRevision.obtenerRevisiones2(identificacion, intercambio.getCodigo());
                if (revisiones != null) {
                    sesionOk.setAttribute("revisiones", revisiones);
                    sesionOk.setAttribute("modificaRevisiones", "false");
                    sesionOk.setAttribute("interVigente", interVigente);
                    sesionOk.setAttribute("infoIntercambio", infoIntercambio);                    
                    url = "./Principal.jsp?mostrarRevisionesV2";
                    response.setHeader("Location", url);
                    response.sendRedirect(url);
                }

            }
        } else {
            revisiones = managerRevision.obtenerRevisiones2(identificacion, intercambio.getCodigo());
            if (revisiones != null) {
                sesionOk = request.getSession();
                sesionOk.setAttribute("revisiones", revisiones);
                sesionOk.setAttribute("modificaRevisiones", "false");
                sesionOk.setAttribute("interVigente", interVigente);
                sesionOk.setAttribute("infoIntercambio", infoIntercambio);   
                url = "./Principal.jsp?mostrarRevisionesv2";
                response.setHeader("Location", url);
                response.sendRedirect(url);
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
