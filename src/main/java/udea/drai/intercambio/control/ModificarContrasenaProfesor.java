/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udea.drai.intercambio.control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import udea.drai.intercambio.dao.LoginDAO;
import udea.drai.intercambio.dto.Persona;

/**
 *
 * @author Joaquin David Hernádez Cárdeas
 * Servlet que le permite al profesor modificar su contraseña
 */
public class ModificarContrasenaProfesor extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        processRequest(request, response);
        String url = "";
        String contraseActual = request.getParameter("contraActual");
        String nuevaContra = request.getParameter("nuevaContrasena");
        String confirmacion = request.getParameter("confirmaContrasena");
        HttpSession sesionOk = request.getSession();
        Persona persona = (Persona) sesionOk.getAttribute("persona");
        String contra = persona.getContrasena();
        if (contraseActual == null ? contra != null : !contraseActual.equals(contra)) {
            System.out.println("Las contraseña ingresada no coincide con la contraseña de sesion");
            url = "./Principal.jsp?contrasenaErronea";
            response.setHeader("Location", url);
            response.sendRedirect(url);
            return;
        }
        if (nuevaContra == null ? confirmacion != null : !nuevaContra.equals(confirmacion)) {
            System.out.println("Las contraseñas ingresas no coinciden");
            url = "./Principal.jsp?nuevaContraNoCoincide";
            response.setHeader("Location", url);
            response.sendRedirect(url);
            return;
        }
        String usuario = persona.getUsuario();
        LoginDAO managerLogin = new LoginDAO();
        int resultado = managerLogin.modificarContrasena(usuario, nuevaContra);
        if (resultado == 0) {
            url = "./Principal.jsp?ErrorModificandoContrasena";
            response.setHeader("Location", url);
            response.sendRedirect(url);
            return;
        }
        persona.setContrasena(nuevaContra);
        sesionOk.setAttribute("persona", persona);
        url = "./Principal.jsp?ExitoModificandoContrasena";
        response.setHeader("Location", url);
        response.sendRedirect(url);

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
