/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package udea.drai.intercambio.control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import udea.drai.intercambio.dao.SemestreDAO;
import udea.drai.intercambio.dto.Semestre;

/**
 *
 * @author Joaquin David Hernández Cárdenas
 * Servlet que permite modificar la informacion de un Semestre
 */
public class ModificarSemestreServlet extends HttpServlet {

   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
       
    }

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String url = "";
        String codigo = request.getParameter("codigo");
        String anio = request.getParameter("anio");
        String periodo = request.getParameter("periodo");
        Semestre semestre = new Semestre();
        try{
        semestre.setAno(Integer.parseInt(anio));
        semestre.setPeriodo(Integer.parseInt(periodo));
        semestre.setCodigo(Integer.parseInt(codigo));
        }catch(NumberFormatException e){
            System.out.println("Error convirtiendo Enteros"+ e.toString());
            url = "./PrincipalAdmin.jsp?faltanCampos";
            response.setHeader("Location", url);
            response.sendRedirect(url);
            return;
        }
        SemestreDAO managerSemestre = new SemestreDAO();
        int resultado = managerSemestre.editarSemestre(Integer.parseInt(anio),Integer.parseInt(periodo),Integer.parseInt(codigo));
        if (resultado == 1) {
            url = "./PrincipalAdmin.jsp?ListarSemestres";
            response.setHeader("Location", url);
            response.sendRedirect(url);
        } else {
            url = "./PrincipalAdmin.jsp?errorModificarSemestre";
            response.setHeader("Location", url);
            response.sendRedirect(url);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
