/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package udea.drai.intercambio.control;

import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import udea.drai.intercambio.dao.SemestreDAO;
import udea.drai.intercambio.dao.subirDAO;
import udea.drai.intercambio.dto.Persona;
import udea.drai.intercambio.dto.Semestre;
import udea.drai.intercambio.dto.registro;

/**
 *
 * @author Daniel Restrepo
 * Servlet que permite descargar al profesor o al administrador la constacia de las notas que sacó un estudiante
 */
public class descargaServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession sesion = request.getSession();
        int id = Integer.parseInt(request.getParameter("archivos"));
        subirDAO sDAO = new subirDAO();
        registro r = sDAO.descargar(id);
        SemestreDAO s = new SemestreDAO();
        Semestre ss = s.obtenerSemestreCodigo(r.getSemestre());
        Blob blob = r.getBlob();
        int iLength = 0;  
        try {
            iLength = (int)(blob.length());
        } catch (SQLException ex) {
            Logger.getLogger(descargaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
 Persona p= (Persona)sesion.getAttribute("persona");
response.setHeader("Content-Disposition", "attachment; filename=\""+p.getUsuario()+" del semestre "+ss.getAno()+"-"+ss.getPeriodo()+".pdf\"");  
response.setContentType("application/pdf");  
response.setContentLength(iLength);  
  
ServletOutputStream op = response.getOutputStream();  
        try {  
            op.write(blob.getBytes(1, iLength));
        } catch (SQLException ex) {
            Logger.getLogger(descargaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
op.flush();  
op.close();  

        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
