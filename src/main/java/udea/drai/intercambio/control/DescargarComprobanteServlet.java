/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udea.drai.intercambio.control;

import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import udea.drai.intercambio.dao.EstudianteDAO;
import udea.drai.intercambio.dao.IntercambioDAO;
import udea.drai.intercambio.dao.subirDAO;
import udea.drai.intercambio.dto.Intercambio;
import udea.drai.intercambio.dto.Persona;
import udea.drai.intercambio.dto.registro;

/**
 *
 * @author Danie Restrepo Arcila.
 * Servlet mediante el cual. el estudiante o el profesor pueden descarcar el compromiso que deben firmar 
 * antes de realizar el intercabio.
 */
public class DescargarComprobanteServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        int id = Integer.parseInt(request.getParameter("archivos"));
        String idEstudiante = request.getParameter("estudiante");
        String intercambio = request.getParameter("intercambios");
        subirDAO sDAO = new subirDAO();
        registro r = sDAO.descargar(id);
        Blob blob = r.getBlob();
        int iLength = 0;
        try {
            iLength = (int) (blob.length());
        } catch (SQLException ex) {
            Logger.getLogger(descargaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        EstudianteDAO managerEstudiante = new EstudianteDAO();
        IntercambioDAO managerIntercambio = new IntercambioDAO();
        Persona p = managerEstudiante.getEstudianteXcedula(idEstudiante);
        Intercambio este = new Intercambio();
        este.setCodigo(id);
        este = managerIntercambio.cargarIntercambio(este);
        String muestra = "Desde" + este.getIda() + "Hasta" + este.getRegreso();

        response.setHeader("Content-Disposition", "attachment; filename=\"" + p.getNombre() + " - " + muestra + ".pdf\"");
        response.setContentType("application/pdf");
        response.setContentLength(iLength);

        ServletOutputStream op = response.getOutputStream();
        try {
            op.write(blob.getBytes(1, iLength));
        } catch (SQLException ex) {
            Logger.getLogger(descargaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        op.flush();
        op.close();
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
