/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udea.drai.intercambio.control;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import udea.drai.intercambio.dao.EstudianteDAO;
import udea.drai.intercambio.dto.Persona;

/**
 *
 * @author Joaquin David Hernádez Cárdeas
 * Servlet mediante el cual los estudiantes pueden enviar un correo electronico a sus profesores
 */
public class MensajeProfesorServlet extends HttpServlet {

    final String VariosEstudiantes = "1111111111";
    final String TodosLosEstudiantes = "1122334455";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MensajeProfesorServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MensajeProfesorServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    static String username, contrasena;
    static Properties p;
    static Session sesion;

    private static Session crearSesion() {
        Session session = Session.getInstance(p,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, contrasena);
                    }
                });
        return session;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String asunto = request.getParameter("asunto");
        String mensaje1 = request.getParameter("mensaje");
        String[] correos = request.getParameterValues("destinatarios");
        String cedProfesor = request.getParameter("profesor");
        String codigoVerificacion = request.getParameter("codigoVerificacion");

        //datos de conexion
        //datos internacionalización --->  ingenieriainternacional@udea.edu.co falta contraseña y permisos !
        username = "jdavidhc94@gmail.com";
        contrasena = "94030909725jdhc";
        //propiedades de la conexion
        p = new Properties();
        p.put("mail.smtp.auth", "true");
        p.put("mail.smtp.starttls.enable", "true");
        p.put("mail.smtp.host", "smtp.gmail.com");
        p.put("mail.smtp.port", "587");
        Session session = Session.getDefaultInstance(p, null);
        //creamos la sesion
        sesion = crearSesion();
        Message mensaje = new MimeMessage(sesion);
        String url = "";
        String mensaje2 = "";
        mensaje2 = mensaje1 + "\n \n \n \n";
        HttpSession sesion = request.getSession();
        Persona p = (Persona) sesion.getAttribute("persona");
        mensaje2 = mensaje2 + "Docente que envia el mensaje: \n" + p.getNombre() + " " + p.getApellido();
        mensaje2 = mensaje2 + "\n \n Señor estudiante este correo es solo informativo,"
                + "abstenerse de hacer preguntas por este medio, comuníquese directamente con su coordinador";
        mensaje2 = mensaje2 + "\n \n [Sistema de internacionalización Universidad de Antioquia]";
        //Mensaje para varios estudiantes pero no selecciono ninguno.
        if (codigoVerificacion.equals(VariosEstudiantes) && correos == null) {
            url = "./Principal.jsp?NoSeleccionoEstudiantes";
            response.setHeader("Location", url);
            response.sendRedirect(url);
            return;
        }
        if (codigoVerificacion.equals(VariosEstudiantes) && correos != null) {
            Address[] destinos = new Address[correos.length + 1];//Aqui usamos el arreglo de destinatarios
            int i = 0;
            try {
                for (String s : correos) {
                    destinos[i] = new InternetAddress(correos[i]);
                    i = i+1;
                }
                destinos[i] = new InternetAddress(p.getEmail());
                mensaje.setRecipients(Message.RecipientType.TO, destinos);
                mensaje.setSubject("[Sistema internacionalización] " + asunto);
                mensaje.setText(mensaje2);
                //Enviamos el Mensaje
                Transport.send(mensaje);
                url = "./Principal.jsp?Send=yes";
            } catch (MessagingException ex) {
                System.err.println("Problemas enviando mensaje a varios estudiantes " + ex);
                url = "./Principal.jsp?Send=no";
            }
        }   else { //Mensaje para todos los estudiantes del profesor
            EstudianteDAO managerEstudiante = new EstudianteDAO();
            List estudiantes = managerEstudiante.getEstudianteXProfesor2(p.getCedula());
            String [] destinatarios = new String[estudiantes.size()+1];
            //Sacamos los correos de los estudiantes
            int i;
            System.out.println("Estudiantes: " + estudiantes.toString());
            for(i=0; i<estudiantes.size(); i++){
                Persona estudiante = (Persona) estudiantes.get(i);
                System.out.println("Correo Estudiante: " + estudiante.getEmail());
                destinatarios[i] = estudiante.getEmail();
            }
            destinatarios[i] = p.getEmail(); // Copia del mensaje para el profesor
            Address []destinos = new Address[destinatarios.length];//Aqui usamos el arreglo de destinatarios
            for(int j=0;j<destinatarios.length; j++){
                try {
                    destinos[j] = new InternetAddress(destinatarios[j]);
                } catch (AddressException ex) {
                    Logger.getLogger(MensajeProfesorServlet.class.getName()).log(Level.SEVERE, null, ex);
                    System.out.println("Error ingresando el correo: "+ destinatarios[j]);
                }
            }
            System.out.println("Destinatarios: "+ destinos.toString());
                try {
                    //mensaje.setRecipient(Message.RecipientType.TO, new InternetAddress(destinos));
                    mensaje.addRecipients(Message.RecipientType.TO, destinos);//agregamos los destinatarios
                    mensaje.setSubject("[Sistema internacionalización] " + asunto);
                    mensaje.setText(mensaje2);
                    //Enviamos el Mensaje
                    Transport.send(mensaje);
                    url = "./Principal.jsp?Send=yes";
                } catch (Exception ex) {
                   //System.err.println("Problemas enviando mensaje al estudiante " + estudiante.getNombre() + " " + estudiante.getApellido() + ex);
                    url = "./Principal.jsp?Send=no";
                }
//            try {
//                mensaje.setRecipient(Message.RecipientType.TO, new InternetAddress(p.getEmail()));
//                mensaje.setSubject("[Sistema internacionalización] " + asunto);
//                mensaje.setText(mensaje2);
//                //Enviamos el Mensaje
//                System.out.println("Mensaje enviado al Profesor");
//                Transport.send(mensaje);
//                url = "./Principal.jsp?Send=yes";
//            } catch (Exception ex) {
//                System.err.println("Problemas enviando mensaje  al profesor" + ex);
//                url = "./Principal.jsp?Send=no";
//            }
        }
        response.setHeader("Location", url);
        response.sendRedirect(url);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
