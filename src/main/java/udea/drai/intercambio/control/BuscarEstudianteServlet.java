/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udea.drai.intercambio.control;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import udea.drai.intercambio.dao.EstudianteDAO;
import udea.drai.intercambio.dto.Persona;

/**
 *
 * @author Usuario
 * Servlet mediante el cual se obtiene la inforación de un estudiante.
 */
public class BuscarEstudianteServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        processRequest(request, response);
        String url = "";
        String identificacion = request.getParameter("identificacion");
        HttpSession sesionOK = request.getSession();
        String tipoUsuario = (String) sesionOK.getAttribute("tipoUsuario");
        EstudianteDAO managerEstudiante = new EstudianteDAO();
        Persona estudiante = new Persona();
        List resultado = new ArrayList();
        //Entro como Administrador
        if (tipoUsuario == "1") {
            estudiante = managerEstudiante.getEstudianteXcedula(identificacion);
            if (estudiante == null) {
                url = "./PrincipalAdmin.jsp?ErrorEstudiantesxCedula";
                response.setHeader("Location", url);
                response.sendRedirect(url);
                return;
            }
            String existe = estudiante.getNombre();
            if (existe == "NOEXISTE") {
                url = "./PrincipalAdmin.jsp?NoExisteEstudiantesCedula";
                response.setHeader("Location", url);
                response.sendRedirect(url);
                return;
            }
            resultado.add(estudiante);
            sesionOK.setAttribute("estudianteCedula", resultado);
            url = "./PrincipalAdmin.jsp?MostrarEstudiantesCedula";
            response.setHeader("Location", url);
            response.sendRedirect(url);
            // Entro como Profesor
        } else if (tipoUsuario == "2") {
            Persona profesor = (Persona) sesionOK.getAttribute("persona");
            estudiante = managerEstudiante.getEstudianteXcedula2(identificacion, profesor.getCedula());
            if (estudiante == null) {
                url = "./Principal.jsp?ErrorEstudiantesxCedula";
                response.setHeader("Location", url);
                response.sendRedirect(url);
                return;
            }
            String existe = estudiante.getNombre();
            if (existe == "NOEXISTE") {
                url = "./Principal.jsp?NoExisteEstudiantesCedula";
                response.setHeader("Location", url);
                response.sendRedirect(url);
                return;
            }
            resultado.add(estudiante);
            sesionOK.setAttribute("estudianteCedula", resultado);
            url = "./Principal.jsp?MostrarEstudiantesCedula";
            response.setHeader("Location", url);
            response.sendRedirect(url);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
