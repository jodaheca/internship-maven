/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udea.drai.intercambio.control;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import udea.drai.intercambio.dao.RequisitoDAO;
import udea.drai.intercambio.dao.RevisionDAO;

/**
 *
 * @author Joaquin David Hernández Cárdenasvar a cabo correctamente
 * su intercambio en una universidad de otro Pais.
 * Servlet utilizado para mostrar los requisitos que debe cumplir un estudiante para llevar a cabo correctamente
 * su intercambio en una universidad de otro Pais.
 */
public class MostrarRequisitosServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        processRequest(request, response);
        String url = "";
        String estudiante = request.getParameter("estudiante");
        String intercambio = request.getParameter("intercambio");
        int inter = 0;
        HttpSession sesionOk = request.getSession();
        String tipoUsuario = (String) sesionOk.getAttribute("tipoUsuario");
        try {
            inter = Integer.parseInt(intercambio);
        } catch (NumberFormatException e) {
            System.out.println("Error conviertiendo la cedula de un estudiante y el codigo de un intercambio a String,"
                    + "Necesarios para poder Mostrar los requisitos de un intercambio" + e.toString());
            if (tipoUsuario == "2") {
                url = "./Principal.jsp?ErrorMostrarTareas";
                response.setHeader("Location", url);
                response.sendRedirect(url);
                return;
            } else {
                url = "./PrincipalAdmin.jsp?ErrorMostrarTareas";
                response.setHeader("Location", url);
                response.sendRedirect(url);
            }
        }
        RevisionDAO managerRevision = new RevisionDAO();
        List revisiones = managerRevision.obtenerRevisiones(estudiante, inter);
        if (revisiones == null) {
            System.out.println("Error recuperando las revisones");
            if (tipoUsuario == "2") {
                url = "./Principal.jsp?ErrorMostrarTareas";
                response.setHeader("Location", url);
                response.sendRedirect(url);
                return;
            } else {
                url = "./PrincipalAdmin.jsp?ErrorMostrarTareas";
                response.setHeader("Location", url);
                response.sendRedirect(url);
                return;
            }
        }
        if (revisiones.isEmpty()) {
            RequisitoDAO managerRequisito = new RequisitoDAO();
            List requisitos = managerRequisito.getRequisitos();
            if (requisitos == null) {
                if (tipoUsuario == "2") {
                    url = "./Principal.jsp?ErrorMostrarTareas";
                    response.setHeader("Location", url);
                    response.sendRedirect(url);
                    System.out.println("Error al sacar los requisitos de la DB");
                    return;
                } else {
                    url = "./PrincipalAdmin.jsp?ErrorMostrarTareas";
                    response.setHeader("Location", url);
                    response.sendRedirect(url);
                    System.out.println("Error al sacar los requisitos de la DB");
                    return;
                }
            }
            int result = managerRevision.ingresarRevisones(requisitos, inter, estudiante);
            if (result == 1) {
                // Sacar las revisiones de la tabla para poder mostrarlas al profesor
                revisiones = managerRevision.obtenerRevisiones2(estudiante, inter);
                if (revisiones != null) {
                    sesionOk.setAttribute("revisiones", revisiones);
                    sesionOk.setAttribute("modificaRevisiones", "false");
                    if (tipoUsuario == "2") {
                        url = "./Principal.jsp?mostrarRevisiones";
                        response.setHeader("Location", url);
                        response.sendRedirect(url);
                    } else {
                        url = "./PrincipalAdmin.jsp?mostrarRevisiones";
                        response.setHeader("Location", url);
                        response.sendRedirect(url);
                    }
                }

            }
        } else {
            revisiones = managerRevision.obtenerRevisiones2(estudiante, inter);
            if (revisiones != null) {
                sesionOk = request.getSession();
                sesionOk.setAttribute("revisiones", revisiones);
                sesionOk.setAttribute("modificaRevisiones", "false");
                if (tipoUsuario == "2") {
                    url = "./Principal.jsp?mostrarRevisiones";
                    response.setHeader("Location", url);
                    response.sendRedirect(url);
                } else {
                    url = "./PrincipalAdmin.jsp?mostrarRevisiones";
                    response.setHeader("Location", url);
                    response.sendRedirect(url);
                }
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
