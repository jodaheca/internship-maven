/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udea.drai.intercambio.control;

import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import udea.drai.intercambio.dao.EstudianteDAO;
import udea.drai.intercambio.dao.LoginDAO;
import udea.drai.intercambio.dto.Persona;
import udea.drai.intercambio.dto.login;

/**
 *
 * @author Joaquin David Hernádez Cárdeas
 * Servlet mediante el cual se registra un nuevo estudiante en el sistema
 */
public class IngresarEstudianteServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        processRequest(request, response);
        String url = "./Principal.jsp";
        String cedula = request.getParameter("documento");
        String usser = request.getParameter("student");
        String password = request.getParameter("password");
        String programa = request.getParameter("programa");
        System.out.println("Se ingresará: " + usser);
        if (programa == "") {
            url = "./Principal.jsp?faltanCampos";
            response.setHeader("Location", url);
            response.sendRedirect(url);
            return;
        }
        try {
            int numero = Integer.parseInt(cedula);
        } catch (NumberFormatException e) {
            url = "./Principal.jsp?cedulaInvalida";
            response.setHeader("Location", url);
            response.sendRedirect(url);
            return;
        }
        Persona estudiante = new Persona();
        estudiante.setCedula(cedula);
        estudiante.setUsuario(usser);
        estudiante.setPrograma(programa);
        EstudianteDAO managerStudent = new EstudianteDAO();
        Persona existe = managerStudent.getEstudianteXcedula(cedula);
        if (existe != null) {
            if (existe.getNombre() != "NOEXISTE") {
                url = "./Principal.jsp?estudianteExiste";
                response.setHeader("Location", url);
                response.sendRedirect(url);
                return;
            }
        }
        try {
            LoginDAO managerLogin = new LoginDAO();

            login ex = managerLogin.obtenerLoginPorUsuario(usser);
            if (ex != null) {
                url = "./Principal.jsp?usuarioAsignado";
                response.setHeader("Location", url);
                response.sendRedirect(url);
                return;
            }
            int result = managerLogin.IngresarUsuario(usser, password, 3);
            if (result == 1) {

                int resultado = managerStudent.IngresarEstudiante(estudiante);
                if (resultado == 1) {
                    url = "./Principal.jsp?EstudianteCorrectamente";
                    response.setHeader("Location", url);
                    response.sendRedirect(url);
                } else {
                    url = "./Principal.jsp?EstudianteError";
                    response.setHeader("Location", url);
                    response.sendRedirect(url);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(IngresarEstudianteServlet.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error al ingresar el estudiante");
            url = "./Principal.jsp?EstudianteError";
            response.setHeader("Location", url);
            response.sendRedirect(url);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
