/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udea.drai.intercambio.control;

import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import udea.drai.intercambio.dao.HomologacionDAO;
import udea.drai.intercambio.dto.Bitacora;
import udea.drai.intercambio.dto.Homologacion;
import udea.drai.intercambio.dto.Persona;

/**
 *
 * @author Daniel Restrepo
 * Este servlet le permite al estudiante registrar  las materias que va a cursar en la universidad a la cuál 
 * se va de intercambio.
 */
public class materiaServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/xml;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        HttpSession sesion = request.getSession();
        String estado = request.getParameter("homologacion");
        HomologacionDAO homo = new HomologacionDAO();
        Homologacion h = new Homologacion();
        String codLocal = request.getParameter("codOrigen");
        String codDestino = request.getParameter("codDestino");
        String materiaDestino = request.getParameter("nombreDestino");
        String materiaOrigen = request.getParameter("nombreOrigen");
         int semestre = Integer.parseInt(request.getParameter("semestre"));
        if (estado == null || estado.equals("")) {
            Date fechaActa=null;
            int acta = 0;
            if(request.getParameter("fecha") != null && !request.getParameter("fecha").equals("")){
                fechaActa = Date.valueOf(request.getParameter("fecha"));
            }
            String intercambio = (String) sesion.getAttribute("temporal");
            if(request.getParameter("acta") != null && !request.getParameter("acta").equals("")){
                acta = Integer.parseInt(request.getParameter("acta"));
            }
           
            h.setFechaActa(fechaActa);
            h.setActa(acta);
            h.setRevision('N');
            h.setIntercambio(Integer.parseInt(intercambio));
     
        } else {
            homo.inhabilitar(Integer.parseInt(estado));
            h.setCodigo(Integer.parseInt(estado));
            homo.cargarHomologacion(h);
        }
               h.setSemestre(semestre);
        h.setCodLocal(codLocal);
        h.setCodExtranjero(codDestino);
        h.setMateriaLocal(materiaOrigen);
        h.setMateriaExtranjero(materiaDestino);
        h.setEstado(1);

        if (estado == null || estado.equals("")) {
            if (homo.ingresarHomologacion(h, null) == 1) {
                request.setAttribute("mensaje", "Homologación ingresada!");
                response.sendRedirect("PrincipalEstudiante.jsp?intercambios&ingresada");
                // dispatcher = getServletContext().getRequestDispatcher("/PrincipalEstudiante.jsp?Intercambios");              
            } else {
                request.setAttribute("mensaje", "Homologación no se pudo ingresar!");
                //  dispatcher = getServletContext().getRequestDispatcher("/PrincipalEstudiante.jsp?Intercambios");
                response.sendRedirect("PrincipalEstudiante.jsp?intercambios&homologacionFail");
            }
        } else {
            Bitacora bi = new Bitacora();
            bi.setCodAnt(h.getCodigo());
            Persona p = (Persona) sesion.getAttribute("persona");
            bi.setUser(p.getUsuario());
            bi.setSsoffi(request.getParameter("ssoffi"));
            bi.setObservacion(request.getParameter("obervaciones"));
            java.util.Date hoy = new java.util.Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            bi.setFecha(sdf.format(hoy));

            if (homo.ingresarHomologacion(h, bi) == 1) {
                request.setAttribute("mensaje", "Homologación ingresada!");
                // dispatcher = getServletContext().getRequestDispatcher("/PrincipalEstudiante.jsp?Intercambios");              
                response.sendRedirect("PrincipalEstudiante.jsp?intercambios&ingresada");
            } else {
                request.setAttribute("mensaje", "Homologación no se pudo ingresar!");
                //  dispatcher = getServletContext().getRequestDispatcher("/PrincipalEstudiante.jsp?Intercambios");
                response.sendRedirect("PrincipalEstudiante.jsp?intercambios&homologacionFail");
            }
        }

        // dispatcher.forward(request, response);        
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
