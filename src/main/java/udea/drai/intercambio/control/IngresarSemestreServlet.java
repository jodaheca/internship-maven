package udea.drai.intercambio.control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import udea.drai.intercambio.dao.SemestreDAO;

/**
 *
 * @author Joaquin David Hernádez Cárdeas
 * Servlet mediante el cual el administrador puede ingresar un nuevo semestre al sistema
 */
public class IngresarSemestreServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        processRequest(request, response);
        String url = "";
        String anio = request.getParameter("anio");
        String periodo = request.getParameter("periodo");
        int ano = 0;
        int per = 0;
        try {
            ano = Integer.parseInt(anio);
            per = Integer.parseInt(periodo);
        } catch (NumberFormatException e) {
            System.out.println("Error convirtinendo el ano y el perido a entero" + e.toString());
            url = "./PrincipalAdmin.jsp?faltanCampos";
            response.setHeader("Location", url);
            response.sendRedirect(url);
            return;
        }
        SemestreDAO managerSemestre = new SemestreDAO();
        boolean existe = managerSemestre.existeSemestre(ano, per);
        // verificamos si ya se ingreso un semestre con ese año y ese semestre
        if (existe) {
            System.out.println("El semestre que se desea registrar ya existe en la DB");
            url = "./PrincipalAdmin.jsp?semestreExiste";
            response.setHeader("Location", url);
            response.sendRedirect(url);
            return;
        }
        int result = managerSemestre.ingresarSemestre(ano, per);
        HttpSession sesionOk = request.getSession();
        String tipoUsuario = (String) sesionOk.getAttribute("tipoUsuario");
        if (result == 1) {
            if ("2".equals(tipoUsuario)) {
                url = "./Principal.jsp?SemestreCorrectamente";
                response.setHeader("Location", url);
                response.sendRedirect(url);
            } else {
                url = "./PrincipalAdmin.jsp?SemestreCorrectamente";
                response.setHeader("Location", url);
                response.sendRedirect(url);
            }
        } else {
            if (tipoUsuario == "2") {
                url = "./Principal.jsp?SemestreError";
                response.setHeader("Location", url);
                response.sendRedirect(url);
            } else {
                url = "./PrincipalAdmin.jsp?SemestreError";
                response.setHeader("Location", url);
                response.sendRedirect(url);
            }
        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
