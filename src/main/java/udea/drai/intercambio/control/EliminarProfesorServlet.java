/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udea.drai.intercambio.control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import udea.drai.intercambio.dao.LoginDAO;
import udea.drai.intercambio.dao.ProfesorDAO;
import udea.drai.intercambio.dto.Persona;

/**
 *
 * @author Joaquin David Hernández Cárdenas
 * Servlet mediante el cual puede el administrador eliminar un Profesor del sistema
 */
public class EliminarProfesorServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String url = "./Principal.jsp?respuesta";
        String id = request.getParameter("id");

        ProfesorDAO managerProfesor = new ProfesorDAO();
        boolean existe = managerProfesor.existeProfesor(id);
        if (existe == false) {
            url = "./PrincipalAdmin.jsp?noExisteProfesor";
            response.setHeader("Location", url);
            response.sendRedirect(url);
            return;
        }

        Persona profe = managerProfesor.getProfesor(id);
        int respuesta = managerProfesor.eliminarProfesor(id);
        if (respuesta == 1) {
            if (profe == null) {
                url = "./PrincipalAdmin.jsp?ErrorEliProfesor";
                response.setHeader("Location", url);
                response.sendRedirect(url);
                return;
            }
            LoginDAO managerLogin = new LoginDAO();
            int res = managerLogin.EliminarLoginPorUsuario(profe.getUsuario());
            if (res == 2) {
                url = "./PrincipalAdmin.jsp?ErrorEliProfesor";
                response.setHeader("Location", url);
                response.sendRedirect(url);
                return;
            }
            url = "./PrincipalAdmin.jsp?ProElimCorrectamente";
            response.setHeader("Location", url);
            response.sendRedirect(url);

        } else {
            url = "./PrincipalAdmin.jsp?ErrorEliProfesor";
            response.setHeader("Location", url);
            response.sendRedirect(url);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
