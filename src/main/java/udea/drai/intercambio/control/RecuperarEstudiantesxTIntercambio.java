/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udea.drai.intercambio.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import udea.drai.intercambio.dao.EstudianteDAO;
import udea.drai.intercambio.dao.SemestreDAO;
import udea.drai.intercambio.dto.EstudianteIntercambio;
import udea.drai.intercambio.dto.EstudianteIntercambio2;
import udea.drai.intercambio.dto.Persona;

/**
 *
 * @author Joaquin David Hernández Cárdenas
 * Servlet mediante el cual se retorna una lista de los estudiantes que estan de intercabio en otras intituciones
 * y tiene en común estar todos en el mismo tipo de intermcambio, por ejemplo, todos esta en Pasantía o todos
 * estan el Doble titulación
 */
public class RecuperarEstudiantesxTIntercambio extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        processRequest(request, response);
        String url = "";
        String codTipo = request.getParameter("tipoIntercambio");
        HttpSession sesionOK = request.getSession();
        String tipoUsuario = (String) sesionOK.getAttribute("tipoUsuario");
        EstudianteDAO managerEstudiante = new EstudianteDAO();
        SemestreDAO managerSemestre = new SemestreDAO();
        List resultadoFinal = new ArrayList();
        List informacion;
        //Entro como Administrador
        if (tipoUsuario == "1") {
            informacion = managerEstudiante.getEstudiantexTipoIntercambio(codTipo);
            if (informacion == null) {
                url = "./PrincipalAdmin.jsp?ErrorEstudiantesxTipo";
                response.setHeader("Location", url);
                response.sendRedirect(url);
                return;
            }
            for (int i = 0; i < informacion.size(); i++) {
                EstudianteIntercambio datos = (EstudianteIntercambio) informacion.get(i);
                int codSemestreInicio = datos.getSemestreInicio();
                int codSemesFin = datos.getSemestreFin();
                String semestreInicio = managerSemestre.obtenerSemestreCodigo2(codSemestreInicio);
                String semestreFin = managerSemestre.obtenerSemestreCodigo2(codSemesFin);
                EstudianteIntercambio2 nuevoDato = new EstudianteIntercambio2();
                nuevoDato.setCedula(datos.getCedula());
                nuevoDato.setNombre(datos.getNombre());
                nuevoDato.setApellido(datos.getApellido());
                nuevoDato.setFechaIda(datos.getFechaIda());
                nuevoDato.setFechaRegreso(datos.getFechaRegreso());
                nuevoDato.setInstitucion(datos.getInstitucion());
                nuevoDato.setSemestreInicio(semestreInicio);
                nuevoDato.setSemestreFin(semestreFin);
                nuevoDato.setPrograma(datos.getPrograma());
                resultadoFinal.add(nuevoDato);
            }
            sesionOK.setAttribute("ListEstudianteTipo", resultadoFinal);
            url = "./PrincipalAdmin.jsp?MostrarEstudiantesTipo";
            response.setHeader("Location", url);
            response.sendRedirect(url);
            // Entro como Profesor
        } else if (tipoUsuario == "2") {
            Persona profesor = (Persona) sesionOK.getAttribute("persona");
            String cedulaProfesor = profesor.getCedula();
            informacion = managerEstudiante.getEstudiantexTipoIntercambio2(codTipo, cedulaProfesor);
            if (informacion == null) {
                url = "./Principal.jsp?ErrorEstudiantesxTipo";
                response.setHeader("Location", url);
                response.sendRedirect(url);
                return;
            }
            for (int i = 0; i < informacion.size(); i++) {
                EstudianteIntercambio datos = (EstudianteIntercambio) informacion.get(i);
                int codSemestreInicio = datos.getSemestreInicio();
                int codSemesFin = datos.getSemestreFin();
                String semestreInicio = managerSemestre.obtenerSemestreCodigo2(codSemestreInicio);
                String semestreFin = managerSemestre.obtenerSemestreCodigo2(codSemesFin);
                EstudianteIntercambio2 nuevoDato = new EstudianteIntercambio2();
                nuevoDato.setCedula(datos.getCedula());
                nuevoDato.setNombre(datos.getNombre());
                nuevoDato.setApellido(datos.getApellido());
                nuevoDato.setFechaIda(datos.getFechaIda());
                nuevoDato.setFechaRegreso(datos.getFechaRegreso());
                nuevoDato.setInstitucion(datos.getInstitucion());
                nuevoDato.setSemestreInicio(semestreInicio);
                nuevoDato.setSemestreFin(semestreFin);
                nuevoDato.setPrograma(datos.getPrograma());
                nuevoDato.setCodigoIntercambio(datos.getCodigoIntercambio());
                resultadoFinal.add(nuevoDato);
            }
            sesionOK.setAttribute("ListEstudianteTipo", resultadoFinal);
            url = "./Principal.jsp?MostrarEstudiantesTipo";
            response.setHeader("Location", url);
            response.sendRedirect(url);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
