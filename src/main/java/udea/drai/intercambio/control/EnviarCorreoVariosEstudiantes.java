/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udea.drai.intercambio.control;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import udea.drai.intercambio.dao.EstudianteDAO;
import udea.drai.intercambio.dto.Persona;

/**
 *
 * @author Joaquin David Hernádez Cárdeas
 * Servlet que contiene la tabla con los estudiantes de un profesor especifico. lista los estudiantes para 
 * quue el profesor seleccione aquellos a los cuales les quiere enviar el correo.
 */
public class EnviarCorreoVariosEstudiantes extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String profesor = request.getParameter("cedulaProfesor");
        System.out.println("Cedula Profesor: " + profesor);
        StringBuffer combo = new StringBuffer();
        if (profesor.equals("8888888888")) { // Si esto se cumple esto borramos la tabla
            System.out.println("Entra en el si");
            combo.append("<div  class=\"form-group\" id=\"estudiantes\">");
            combo.append("</div>");
            out.print(combo.toString());
            System.out.println(combo.toString());
            out.close();
        } else {
            EstudianteDAO managerEstudiante = new EstudianteDAO();
            List estudiantes = managerEstudiante.getEstudianteXProfesor2(profesor);
            combo.append("<div  class=\"form-group\" id=\"estudiantes\">");
            combo.append(" <table class=\"table table-bordered\">");
            combo.append(" <thead>\n"
                    + "                                        <tr> \n"
                    + "                                            <th>Estudiante</th> \n"
                    + "                                            <th>Enviar correo</th>\n"
                    + "                                        </tr>\n"
                    + "                                    </thead>");
            combo.append(" <tbody>");
            for (int i = 0; i < estudiantes.size(); i++) {
                Persona Estudiante = (Persona) estudiantes.get(i);
                String nombreCompleto = Estudiante.getNombre() + " " + Estudiante.getApellido();
                String correo = Estudiante.getEmail();
                combo.append("<tr class=\"active\">\n"
                        + "                                            <td>" + nombreCompleto + "</td>\n"
                        + "\n"
                        + "                                            <td><input type=\"checkbox\" class=\"checkbox\" name=\"destinatarios\" value=\"" + correo + "\"></td>\n"
                        + "                                        </tr>");
            }
            combo.append("</tbody>\n" + "</table>");
            combo.append("</div>");
            out.print(combo.toString());
            System.out.println(combo.toString());
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
//        String profesor = request.getParameter("cedulaProfesor");
//        System.out.println("Cedula Profesor: "+ profesor);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
