/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udea.drai.intercambio.control;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import udea.drai.intercambio.dao.ProgramaDAO;
import udea.drai.intercambio.dto.Persona;

/**
 *
 * @author Usuario
 */
public class MensajeServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MensajeServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MensajeServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    static String username, contrasena;
    static Properties p;
    static Session sesion;

    private static Session crearSesion() {
        Session session = Session.getInstance(p,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, contrasena);
                    }
                });
        return session;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
          response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
       
        String asunto = request.getParameter("asunto");
        String mensaje1 = request.getParameter("mensaje");
        String programa = request.getParameter("programa");

        //datos de conexion
        //datos internacionalización --->  ingenieriainternacional@udea.edu.co falta contraseña y permisos !
        ProgramaDAO pd = new ProgramaDAO();
        Persona profesor = pd.profesorDePrograma(programa);
        String para = profesor.getEmail();
        username = "jdavidhc94@gmail.com";
        contrasena = "94030909725jdhc";
        //propiedades de la conexion
        p = new Properties();
        p.put("mail.smtp.auth", "true");
        p.put("mail.smtp.starttls.enable", "true");
        p.put("mail.smtp.host", "smtp.gmail.com");
        p.put("mail.smtp.port", "587");
        Session session = Session.getDefaultInstance(p, null);
        //creamos la sesion
        sesion = crearSesion();
        Message mensaje = new MimeMessage(sesion);
        String url = "";
        String mensaje2 = "";
        mensaje2 = mensaje1+ "\n \n \n \n";
         HttpSession sesion = request.getSession();
        Persona p =  (Persona) sesion.getAttribute("persona");
        mensaje2 = mensaje2+"Mensaje enviado por el estudiante: \n"+ p.getNombre() + " "+p.getApellido();
        mensaje2 = mensaje2+"\n C.C. "+p.getCedula();
              mensaje2 = mensaje2+ "\n \n [Sistema de internacionalización]";
        try {
            mensaje.setRecipient(Message.RecipientType.TO, new InternetAddress(para));
            mensaje.setSubject("[Sistema internacionalización] "+asunto);
            mensaje.setText(mensaje2);
            //Enviamos el Mensaje
            Transport.send(mensaje);
            System.out.println("Mensaje enviado");
            url = "./PrincipalEstudiante.jsp?EnviarMensaje&Send=yes";
        } catch (Exception ex) {
            System.err.println("Problemas enviando mensaje " + ex);
            url = "./PrincipalEstudiante.jsp?EnviarMensaje&Send=no";
        }

        response.setHeader("Location", url);
        response.sendRedirect(url);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
