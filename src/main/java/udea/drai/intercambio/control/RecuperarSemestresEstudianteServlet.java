/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package udea.drai.intercambio.control;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import udea.drai.intercambio.dao.IntercambioDAO;
import udea.drai.intercambio.dao.SemestreDAO;
import udea.drai.intercambio.dto.Semestre;

/**
 *
 * @author Joaquin David Hernández Cárdenas
 * Servlet que permite recuperar los semestre que tiene un estudiante registrados en un intercambio específico
 */
public class RecuperarSemestresEstudianteServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String codIntercambio = request.getParameter("intercambio");
        int codI;
        try{
        codI = Integer.parseInt(codIntercambio);
        }catch(NumberFormatException e){
            System.out.println("No envio un parametro valido, no selecciono intercambio");
            return;
        }
        IntercambioDAO managerIntercambio = new  IntercambioDAO();
        int sInicio=managerIntercambio.getSemestreInicio(codI);
        int sFin=managerIntercambio.getSemestreFin(codI);
        if(sInicio!=0 && sFin!=0){
            Semestre inicial = new Semestre();
            Semestre ultimo = new Semestre();
            SemestreDAO managerSemestre= new SemestreDAO();
            inicial = managerSemestre.obtenerSemestreCodigo(sInicio);
            ultimo = managerSemestre.obtenerSemestreCodigo(sFin);
            List<Semestre> semestres= new ArrayList();
            int anioInicial = inicial.getAno();
            int anioUltimo = ultimo.getAno();
            int periodoInicial = inicial.getPeriodo();
             while(anioInicial<=anioUltimo){
                Semestre nuevo= new Semestre();
                nuevo=managerSemestre.obtenerSemestreAnioPeriodo(anioInicial,periodoInicial);
                if(nuevo!=null){
                    semestres.add(nuevo);
                }
                if(periodoInicial ==1){
                  nuevo=managerSemestre.obtenerSemestreAnioPeriodo(anioInicial,2);
                  if(nuevo!=null){
                    semestres.add(nuevo);
                }
                }
                periodoInicial=1;
                anioInicial = anioInicial+1;
            }
             Semestre  ultimoLista= semestres.get(semestres.size()-1);
             if(ultimo.getCodigo()!=ultimoLista.getCodigo()){
                 semestres.remove(semestres.size()-1);
             }
             PrintWriter out = response.getWriter();
             StringBuffer combo = new StringBuffer();
             combo.append("<select name='semestres' id = 'semestres' style = 'width:200px'>");
             combo.append("<option value=''>Seleccionar Semestre</option>");
             for(int i=0;i<semestres.size();i++){
                 Semestre actual=semestres.get(i);
                 int codigo=actual.getCodigo();
                 String anio=Integer.toString(actual.getAno());
                 String periodo=Integer.toString(actual.getPeriodo());
                 combo.append("<option value='" + Integer.toString(codigo) + "'>" + anio+"-"+periodo + "</option>");
             }
                combo.append("</select>");
                 out.print(combo.toString());
                  System.out.println(combo.toString());
                  out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
