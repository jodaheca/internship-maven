/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udea.drai.intercambio.control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import udea.drai.intercambio.dao.EstudianteDAO;
import udea.drai.intercambio.dao.UsuarioDAO;
import udea.drai.intercambio.dto.Persona;
import udea.drai.intercambio.dto.login;

/**
 *
 * @author Daniel Restrepo Arcila
 * Este servlet le permite al estudiante modificar su información personal.
 */
public class estudianteServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/xml;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        EstudianteDAO ed = new EstudianteDAO();
        Persona est;
        UsuarioDAO usDAO = new UsuarioDAO();
        String password = request.getParameter("contrasena");
        String user = request.getParameter("user");
        login lg = usDAO.verificacion(user);
        System.out.println(lg.getContrasena()+" "+password);
        if (!lg.getContrasena().equals(password)) {
            request.setAttribute("mensaje", "Contraseña incorrecta!");
            response.sendRedirect("PrincipalEstudiante.jsp?error");
            return;
        }

        String nombre = request.getParameter("nombre");
        String cedula = request.getParameter("cedula");
        String correo = request.getParameter("email");
        String prog = request.getParameter("programa");
        String apellido = request.getParameter("apellido");
        est = new Persona(nombre, apellido, cedula, correo, prog, user);
        if (est != null) {

            HttpSession sesion = request.getSession();
            if (ed.modificarInfo(est) == 1) {
                request.setAttribute("mensaje", "Modificación exitosa!");
            } else {
                request.setAttribute("mensaje", "No se pudo realizar la modificación!");
            }
        } else {
            request.setAttribute("mensaje", "No se pudo realizar la modificación!");
        }
        response.sendRedirect("PrincipalEstudiante.jsp?modificado");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
