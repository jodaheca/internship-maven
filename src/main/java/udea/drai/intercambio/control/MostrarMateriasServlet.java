/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udea.drai.intercambio.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import udea.drai.intercambio.dao.HomologacionDAO;

/**
 *
 * @author Joaquin David Hernandez Cardenas
 * Servlet que permite mostrar las materias de un estudiante
 */
public class MostrarMateriasServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String url = "";
        String estudiante = request.getParameter("estudiante");
        String intercambio = request.getParameter("intercambio");
        int inter = Integer.parseInt(intercambio);
        String semestre = request.getParameter("semestres");
        int semes = Integer.parseInt(semestre);
        HomologacionDAO managerHomologacion = new HomologacionDAO();
        List materias = new ArrayList();
        materias = managerHomologacion.obtenerHomologaciones(inter, semes);
        if (materias != null) {
            HttpSession sesionOk = request.getSession();
            String tipoUsuario = (String) sesionOk.getAttribute("tipoUsuario");
            sesionOk.setAttribute("materias", materias);
            sesionOk.setAttribute("modificacion", "false");
            request.setAttribute("homologaciones", materias);
            if (tipoUsuario == "2") {
                url = "./Principal.jsp?homologaciones";
                response.setHeader("Location", url);
                response.sendRedirect(url);
            } else {
                url = "./PrincipalAdmin.jsp?homologaciones";
                response.setHeader("Location", url);
                response.sendRedirect(url);
            }
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
