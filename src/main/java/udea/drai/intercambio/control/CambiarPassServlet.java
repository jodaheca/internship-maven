/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udea.drai.intercambio.control;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import udea.drai.intercambio.dao.LoginDAO;
import udea.drai.intercambio.dto.login;

/**
 *
 * @author Usuario}
 * Servlet mediante el cual un estudiante puede cambiar su clave en el sistema.
 */
public class CambiarPassServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CambiarPassServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CambiarPassServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String contra = request.getParameter("passNew");
        String user = request.getParameter("user");
        String ant = request.getParameter("passAnt");
        LoginDAO lDAO = new LoginDAO();
        login log = lDAO.obtenerLoginPorUsuario(user);
      //  System.out.println(log.getContrasena() +" - "+contra);
        if (log != null && log.getContrasena().equals(ant)) {
            boolean ver = lDAO.cambiarPass(user, contra);
            if (ver) {
                response.setHeader("Location", "./PrincipalEstudiante.jsp?newPassword&cambioExitoso");
                response.sendRedirect("./PrincipalEstudiante.jsp?newPassword&cambioExitoso");
            } else {
                response.setHeader("Location", "./PrincipalEstudiante.jsp?newPassword&cambioFallido");
                response.sendRedirect("./PrincipalEstudiante.jsp?newPassword&cambioFallido");
            }
        } else {
            response.setHeader("Location", "./PrincipalEstudiante.jsp?newPassword&claveIncorrecta");
            response.sendRedirect("./PrincipalEstudiante.jsp?newPassword&claveIncorrecta");
        }
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
