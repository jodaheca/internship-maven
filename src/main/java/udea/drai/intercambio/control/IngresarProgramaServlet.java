/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udea.drai.intercambio.control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import udea.drai.intercambio.dao.ProgramaDAO;
import udea.drai.intercambio.dto.Persona;
import udea.drai.intercambio.dto.Programa;

/**
 *
 * @author Joaquin David Hernádez Cárdeas
 * Servlet mediante el cual el administrador puede ingresar un nuevo programa al sistema
 */
public class IngresarProgramaServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        processRequest(request, response);
        String url = "";
        try {
            String codigo = request.getParameter("codigo");
            String nombre = request.getParameter("nombre");
            HttpSession sesionOk = request.getSession();
            String tipoUsuario = (String) sesionOk.getAttribute("tipoUsuario");
            try {
                int cod = Integer.parseInt(codigo);
            } catch (NumberFormatException e) {
                if (tipoUsuario == "2") {
                    url = "./Principal.jsp?codPrograma";
                    response.setHeader("Location", url);
                    response.sendRedirect(url);
                    return;
                } else {
                    url = "./PrincipalAdmin.jsp?codPrograma";
                    response.setHeader("Location", url);
                    response.sendRedirect(url);
                    return;
                }
            }
            HttpSession session = request.getSession();
            Persona persona = (Persona) session.getAttribute("persona");
            String identificacion = persona.getCedula();
            Programa nuevo = new Programa();
            nuevo.setCodigo(codigo);
            nuevo.setNombre(nombre);
            nuevo.setDirector(identificacion);
            ProgramaDAO pr = new ProgramaDAO();
            int resultado = pr.agregarPrograma(nuevo);
            if (resultado == 1) {
                if (tipoUsuario == "2") {
                    url = "./Principal.jsp?IPCorrectamente";
                    response.setHeader("Location", url);
                    response.sendRedirect(url);
                } else {
                    url = "./PrincipalAdmin.jsp?IPCorrectamente";
                    response.setHeader("Location", url);
                    response.sendRedirect(url);
                }
            } else {
                if (tipoUsuario == "2") {
                    url = "./Principal.jsp?IPError";
                    response.setHeader("Location", url);
                    response.sendRedirect(url);
                } else {
                    url = "./PrincipalAdmin.jsp?IPError";
                    response.setHeader("Location", url);
                    response.sendRedirect(url);
                }
            }

        } catch (Exception e) {
            System.out.println("Error de procesamiento en el servlet IngregarProgramaServlet" + e.toString());
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
