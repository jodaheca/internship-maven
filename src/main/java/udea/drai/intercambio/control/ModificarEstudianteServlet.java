/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udea.drai.intercambio.control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import udea.drai.intercambio.dao.EstudianteDAO;
import udea.drai.intercambio.dto.Persona;

/**
 *
 * @author Joaquin David Hernádez Cárdeas
 * Servlet mediante el cual el profesor puede modi
public class ModificarEstudianteServlet extficar la informacion de un estudiante
 */
public class ModificarEstudianteServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String url = "";
        String nombre = request.getParameter("nombre");
        String apellido = request.getParameter("apellido");
        String cedula = request.getParameter("id");
        String email = request.getParameter("email");
        Persona estudiante = new Persona();
        estudiante.setCedula(cedula);
        estudiante.setNombre(nombre);
        estudiante.setApellido(apellido);
        estudiante.setEmail(email);
        EstudianteDAO managerEstudiante = new EstudianteDAO();
        int resultado = managerEstudiante.editarEstudiante(estudiante);
        HttpSession sesionOk = request.getSession();
        String tipoUsuario = (String) sesionOk.getAttribute("tipoUsuario");
        if (resultado == 1) {
              if (tipoUsuario == "2") {
                url = "./Principal.jsp?ListarEstudiantes";
                response.setHeader("Location", url);
                response.sendRedirect(url);
            } else {
                url = "./PrincipalAdmin.jsp?ListarEstudiantes";
                response.setHeader("Location", url);
                response.sendRedirect(url);
            }
        } else {
            if (tipoUsuario == "2") {
                url = "./Principal.jsp?errorModificarEstudiante";
                response.setHeader("Location", url);
                response.sendRedirect(url);
            } else {
                url = "./PrincipalAdmin.jsp?errorModificarEstudiante";
                response.setHeader("Location", url);
                response.sendRedirect(url);
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
