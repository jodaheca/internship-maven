/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package udea.drai.intercambio.control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import udea.drai.intercambio.dao.InstitucionDAO;
import udea.drai.intercambio.dto.Institucion;

/**
 *
 * @author Joaquin David Hernández Cárdenas
 * Servlet que le permite al administrsador modificar la inforación de una institución
 */
public class ModificarInstitucionServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String url = "";
        String codigo = request.getParameter("codigo");
        String nombre = request.getParameter("nombre");
        String pais = request.getParameter("pais");
        Institucion institucion = new Institucion();
        institucion.setCodigo(codigo);
        institucion.setNombre(nombre);
        institucion.setPais(pais);
        InstitucionDAO managerInstitucion = new InstitucionDAO();
        int resultado = managerInstitucion.editarInstitucion(codigo,nombre,pais);
        if (resultado == 1) {
            url = "./PrincipalAdmin.jsp?ListarInstituciones";
            response.setHeader("Location", url);
            response.sendRedirect(url);
        } else {
            url = "./PrincipalAdmin.jsp?errorModificarIntitucion";
            response.setHeader("Location", url);
            response.sendRedirect(url);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
