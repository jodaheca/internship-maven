/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udea.drai.intercambio.control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import udea.drai.intercambio.dao.ProgramaDAO;

/**
 *
 * @author Joaquin  David Hernánde Cárdenas
 * Servlet mediante el cual el administrador puede eliminar un programa del sistema
 */
public class EliminarProgramaServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String url = "./Principal.jsp?respuesta";
        String codigo = request.getParameter("codigo");
        int cod = 0;
        HttpSession sesionOk = request.getSession();
        String tipoUsuario = (String) sesionOk.getAttribute("tipoUsuario");
        try {
            cod = Integer.parseInt(codigo);
        } catch (Exception e) {
            System.out.println("Error convirtiendo el codigo de un programa a entero");
            if (tipoUsuario == "2") {
                url = "./Principal.jsp?codPrograma";
                response.setHeader("Location", url);
                response.sendRedirect(url);
                return;
            } else {
                url = "./PrincipalAdmin.jsp?codPrograma";
                response.setHeader("Location", url);
                response.sendRedirect(url);
            }
        }
        ProgramaDAO prog = new ProgramaDAO();
        int respuesta = prog.eliminarPrograma(cod);
        // RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/Principal.jsp?respuesta");
        //  dispatcher.forward(request, response);
        if (respuesta == 1) {
            if (tipoUsuario == "2") {
                url = "./Principal.jsp?PECorrectamente";
                response.setHeader("Location", url);
                response.sendRedirect(url);
            } else {
                url = "./PrincipalAdmin.jsp?PECorrectamente";
                response.setHeader("Location", url);
                response.sendRedirect(url);
            }
        } else {
            if (tipoUsuario == "2") {
                url = "./PrincipalAdmin.jsp?PEError";
                response.setHeader("Location", url);
                response.sendRedirect(url);
            }
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
