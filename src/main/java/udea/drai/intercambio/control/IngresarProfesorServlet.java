/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udea.drai.intercambio.control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import udea.drai.intercambio.dao.LoginDAO;
import udea.drai.intercambio.dao.ProfesorDAO;
import udea.drai.intercambio.dto.Persona;
import udea.drai.intercambio.dto.login;

/**
 *
 * @author Joaquin David Hernádez Cárdeas
 * Servlet mediante el cual se puede ingresar un nuevo profesor al sistema
 */
public class IngresarProfesorServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        processRequest(request, response);

        String url = "./PrincipalAdmin.jsp";
        String nombre = request.getParameter("nombre");
        String apellido = request.getParameter("apellido");
        String id = request.getParameter("id");
        String email = request.getParameter("email");
        String usuario = request.getParameter("usuario");
        String password = request.getParameter("password");
        LoginDAO managerLogin = new LoginDAO();
        login existe = managerLogin.obtenerLoginPorUsuario(usuario);
        if (existe != null) {
            url = "./PrincipalAdmin.jsp?usuarioAsignado";
            response.setHeader("Location", url);
            response.sendRedirect(url);
            return;
        }
        // guardamos el usuario que utilizara el profesor
        int result = managerLogin.IngresarUsuario(usuario, password, 2);
        if (result == 2) {
            url = "./PrincipalAdmin.jsp?errorIngresoLogin";
            response.setHeader("Location", url);
            response.sendRedirect(url);
            return;
        }
        ProfesorDAO managerProfesor = new ProfesorDAO();
        Persona profesor = new Persona();
        profesor.setCedula(id);
        profesor.setNombre(nombre);
        profesor.setApellido(apellido);
        profesor.setUsuario(usuario);
        profesor.setEmail(email);
        profesor.setContrasena(password);
        int resultIngreso = managerProfesor.IngresarProfesor(profesor);
        if (resultIngreso == 2) {
            url = "./PrincipalAdmin.jsp?errorIngresoLogin";
            response.setHeader("Location", url);
            response.sendRedirect(url);
        } else {
            url = "./PrincipalAdmin.jsp?ingresoProfesorCorrecto";
            response.setHeader("Location", url);
            response.sendRedirect(url);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
