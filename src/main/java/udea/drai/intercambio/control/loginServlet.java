/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udea.drai.intercambio.control;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import udea.drai.intercambio.dao.EstudianteDAO;
import udea.drai.intercambio.dao.UsuarioDAO;
import udea.drai.intercambio.dto.Persona;
import udea.drai.intercambio.dto.login;

/**
 *
 * @author Usuario
 */
public class loginServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = "";
        String mensajeError = null;
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String usuario = request.getParameter("usuario");
        String contrasena = (String) request.getParameter("contrasena");
        UsuarioDAO us = new UsuarioDAO();
        EstudianteDAO es = new EstudianteDAO();
        if (usuario == null || contrasena == null) {
            mensajeError = "Usuario y/o contraseña incorrecta";
            request.setAttribute("mensaje", mensajeError);
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/index.jsp");
            dispatcher.forward(request, response);
            return;
        }
        login lg = us.verificacion(usuario);
        if (lg != null && lg.getContrasena().equals(contrasena)) {
            Persona e = es.getEstudianteXusuario(usuario);
            if (e != null) {
                e.setContrasena(contrasena);
            }
            HttpSession session = request.getSession();
            session.setAttribute("persona", e);
            session.setAttribute("login", "true");
            // verificamos si es estudiante

            if (lg.getPermiso() == 3) {
                session.setAttribute("tipoUsuario", "3");
                url = "./PrincipalEstudiante.jsp";
               // RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/PrincipalEstudiante.jsp");
                //  dispatcher.forward(request, response);
                // Si es profesor
            } else if (lg.getPermiso() == 2) {
                session.setAttribute("tipoUsuario", "2");
                url = "./Principal.jsp?ListarEstudiantes";
                //RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/Principal.jsp");
                //dispatcher.forward(request, response);
                // si es Administrador
            } else if (lg.getPermiso() == 1) {
                session.setAttribute("tipoUsuario", "1");
                url = "./PrincipalAdmin.jsp?listarProfesores";
               // RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/PrincipalAdmin.jsp?listarProfesores");
                //  dispatcher.forward(request, response);
            } else {
                request.setAttribute("mensaje", "Usuario y/o contraseña incorrecta");
                url = "./index.jsp";
                //  RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/index.jsp");
                //  dispatcher.forward(request, response);
            }

        } else {
            url = "./index.jsp";
            mensajeError = "Usuario y/o contraseña incorrecta";
            request.setAttribute("mensaje",mensajeError);
            //sesionOk.setAttribute("mensaje", mensajeError);
            // RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/index.jsp");
            //dispatcher.forward(request, response);
        }
        System.out.println("Mensaje de Error en el Servlet: " + mensajeError);
        response.setHeader("Location", url);
        response.sendRedirect(url);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
