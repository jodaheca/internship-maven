/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udea.drai.intercambio.control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import udea.drai.intercambio.dao.IntercambioDAO;

/**
 *
 * @author Joaquin David Hernandez Cárdenas
 * Servlet que le permite al administrador elimar un tipo de intercambio en el sistema.
 */
public class EliminarTipoIntercambioServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String url;
        String codigo = request.getParameter("codigo");
        int cod = Integer.parseInt(codigo);
        IntercambioDAO inter = new IntercambioDAO();
        int respuesta = inter.eliminarIntercambio(cod);
      // RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/Principal.jsp?respuesta");
        //  dispatcher.forward(request, response);
        if (respuesta == 1) {
            url = "./PrincipalAdmin.jsp?INECorrectamente";
            response.setHeader("Location", url);
            response.sendRedirect(url);
        } else {
            url = "./PrincipalAdmin.jsp?INError";
            response.setHeader("Location", url);
            response.sendRedirect(url);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
