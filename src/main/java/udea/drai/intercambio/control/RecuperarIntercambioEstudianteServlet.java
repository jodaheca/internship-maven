/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package udea.drai.intercambio.control;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import udea.drai.intercambio.dao.IntercambioDAO;

/**
 *
 * @author Joaquin  David Hernández Cárdenas
 * Este servlet retorna todos los intemcabios que ha tenido un estudiante, recibe como parametro la cedula del
 * estudiante y retorna una lista con los intercambios en los que está.
 */
public class RecuperarIntercambioEstudianteServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String estudiante = request.getParameter("cedula");
        IntercambioDAO managerIntercambio = new  IntercambioDAO();
       HashMap intercambios= managerIntercambio.cargarIntercambios(estudiante);
        StringBuffer combo = new StringBuffer();
        combo.append("<select name='intercambios' id = 'intercambios' style = 'width:200px'>");
        combo.append("<option value=''>Seleccionar Intercambio</option>");
        Iterator i= intercambios.entrySet().iterator() ;
        while(i.hasNext()){
            Map.Entry e=(Map.Entry)i.next();
            Object cla= e.getKey();
            int clave=Integer.parseInt(cla.toString());
            String valor=(String) e.getValue();
            combo.append("<option value='" + Integer.toString(clave) + "'>" + valor + "</option>");
        }
        combo.append("</select>");
        out.print(combo.toString());
        System.out.println(combo.toString());
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
