package udea.drai.intercambio.control;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import udea.drai.intercambio.dao.EstudianteDAO;
import udea.drai.intercambio.dao.HomologacionDAO;
import udea.drai.intercambio.dao.IntercambioDAO;
import udea.drai.intercambio.dto.Intercambio;
import udea.drai.intercambio.dto.MateriasPorSemestre;
import udea.drai.intercambio.dto.Persona;
import udea.drai.intercambio.dto.Semestre;
import udea.drai.intercambio.util.IntercambioUtil;
import udea.drai.intercambio.util.SemestreUtil;

/**
 * Servlet mediante el cual se obtienen todas las materias de todos los semestre
 * del intercambio de un estudiante y se envian a la vista para ser mostradas al
 * profesor
 *
 * @author Joaquin David Hernández <jdavidhc94@gmail.com>
 */
public class MostrarMateriasServletV2 extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");

        processRequest(request, response);
        HttpSession sesionOk = request.getSession();
        String tipoUsuario = (String) sesionOk.getAttribute("tipoUsuario");

        /**
         * Calculamos el semestre actual, para verificar que el intercambio
         * obtenido esta vigente en el semestre que estamos
         */
        SemestreUtil managerSemestre = new SemestreUtil();
        String semestreActual = managerSemestre.calcularSemestre();
        String idEstuduiante = request.getParameter("identificacion");
        EstudianteDAO managerEastudiante = new EstudianteDAO();
        Persona estudiante = managerEastudiante.getEstudianteXcedula(idEstuduiante);
        String nombreEstudiante = estudiante.getNombre() + " " + estudiante.getApellido();
        String infoIntercambio = estudiante.getNombreIntercambio();
        IntercambioDAO managerIntercambio = new IntercambioDAO();
        List intercambios = managerIntercambio.ObtenerintercambiosXEstudiante(idEstuduiante);
        String url;

        /**
         * Si la lista esta vacia es porque el estudiante no tiene intercambios
         * registrados
         */
        if (intercambios.isEmpty()) {
            url = "./Principal.jsp?NoTieneIntercambio";
            response.setHeader("Location", url);
            response.sendRedirect(url);
            return;
        }
        /**
         * obtenemos el primer intercambio, el cual debe ser el actual
         */
        Intercambio intercambio = (Intercambio) intercambios.get(0);

        IntercambioUtil interUtil = new IntercambioUtil();
        List semestres = interUtil.obtenerSemestre(intercambio.getCodigo());

        Semestre sem = (Semestre) semestres.get(0);
        String infoSemestres = sem.getAno() + "-" + sem.getPeriodo();
        int longitudList = semestres.size();
        sem = (Semestre) semestres.get(longitudList - 1);

        infoSemestres = infoSemestres + " A " + sem.getAno() + "-" + sem.getPeriodo();

        infoIntercambio = infoIntercambio + " DE " + infoSemestres;

        /**
         * Se crea la variable interVigente para verificar si el intercambio
         * obtenido contiene el semestre en el que estamos, es decir que el
         * estudiante si está de intermcabio este semestre.
         */
        String interVigente = "false";
        for (Object semes : semestres) {
            Semestre sm = (Semestre) semes;
            String nombre = sm.getAno() + "-" + sm.getPeriodo();
            if (semestreActual.equalsIgnoreCase(nombre)) {
                interVigente = "true";
                break;
            }
        }

        /**
         * Con el siguiente for obtenemos todas las materias del estudiante en
         * cada uno de los semestres del intercambio
         */
        List MateriasPorSemestre = new ArrayList(); //Lista que se va a enviar a la vistas
        HomologacionDAO managerHomologacion = new HomologacionDAO();
        for (int i = 0; i<semestres.size(); i++) {
            Semestre s = (Semestre) semestres.get(i);
            MateriasPorSemestre mPs = new MateriasPorSemestre();
            List MPorS = new ArrayList();
            MPorS = managerHomologacion.obtenerHomologaciones(intercambio.getCodigo(), s.getCodigo());
            if (MPorS == null) {
                url = "./Principal.jsp?NoHayConexion";
                response.setHeader("Location", url);
                response.sendRedirect(url);
                return;
            }
            mPs.setMaterias(MPorS);
            mPs.setSemestre(s);

            MateriasPorSemestre.add(mPs);
        }

        sesionOk.setAttribute("materias", MateriasPorSemestre);
        sesionOk.setAttribute("modificacion", "false");
        sesionOk.setAttribute("idEstudiante", idEstuduiante);
        request.setAttribute("homologaciones", MateriasPorSemestre);
        sesionOk.setAttribute("infoIntercambio", infoIntercambio);
        sesionOk.setAttribute("interVigente", interVigente);
        sesionOk.setAttribute("nombreEstudiante", nombreEstudiante);
        url = "./Principal.jsp?homologacionesV2";
        response.setHeader("Location", url);
        response.sendRedirect(url);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
