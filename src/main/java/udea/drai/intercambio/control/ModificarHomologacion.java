
package udea.drai.intercambio.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import udea.drai.intercambio.dao.HomologacionDAO;
import udea.drai.intercambio.dto.Homologacion;

/**
 *Servlet que permite modificar  las matriculas de un estudiante
 * @author Daniel Restrepo Arcila
 * 
 */
public class ModificarHomologacion extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String url = "";
        String codHomologacion = request.getParameter("codigo");
        String nota = request.getParameter("nota");
        String estado = request.getParameter("estado");
        HomologacionDAO managerMateria = new HomologacionDAO();
        int resultado = managerMateria.modificarHomologacion(nota, estado, codHomologacion);
        if (resultado == 1) {
            Homologacion muestra = new Homologacion();
            muestra.setCodigo(Integer.parseInt(codHomologacion));
            Homologacion result = managerMateria.cargarHomologacion(muestra);
            List materias = new ArrayList();
            materias = managerMateria.obtenerHomologaciones(result.getIntercambio(), result.getSemestre());
            if (materias != null) {
                HttpSession sesionOk = request.getSession();
                sesionOk.setAttribute("materias", materias);
                sesionOk.setAttribute("modificacion", "true");
                request.setAttribute("homologaciones", materias);
                url = "./Principal.jsp?homologaciones";
                response.setHeader("Location", url);
                response.sendRedirect(url);
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
