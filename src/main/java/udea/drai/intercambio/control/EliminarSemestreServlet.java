/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udea.drai.intercambio.control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import udea.drai.intercambio.dao.SemestreDAO;

/**
 *
 * @author Joaquin David Hernádez Cárdeas
 * Servlet mediante el cual el administrador puede eliminar un semestre
 */
public class EliminarSemestreServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        processRequest(request, response);
        String url = "./Principal.jsp?respuesta";
        String codigo = request.getParameter("codigo");
        int cod = 0;
        HttpSession sesionOk = request.getSession();
        String tipoUsuario = (String) sesionOk.getAttribute("tipoUsuario");
        try {
            cod = Integer.parseInt(codigo);
        } catch (NumberFormatException e) {
            System.out.println("Error al convertir el codigo a entero" + e.toString());
            if (tipoUsuario == "2") {
                url = "./Principal.jsp?faltanCampos";
                response.setHeader("Location", url);
                response.sendRedirect(url);
                return;
            } else {
                url = "./PrincipalAdmin.jsp?faltanCampos";
                response.setHeader("Location", url);
                response.sendRedirect(url);
                return;
            }
        }
        SemestreDAO managerSemestre = new SemestreDAO();
        int result = managerSemestre.desactivarSemestre(cod);
        if (result == 1) {
            if (tipoUsuario == "2") {
                url = "./Principal.jsp?ISECorrectamente";
                response.setHeader("Location", url);
                response.sendRedirect(url);
            } else {
                url = "./PrincipalAdmin.jsp?ISECorrectamente";
                response.setHeader("Location", url);
                response.sendRedirect(url);
            }
        } else {
            if (tipoUsuario == "2") {
                url = "./Principal.jsp?PEError";
                response.setHeader("Location", url);
                response.sendRedirect(url);
            } else {
                url = "./PrincipalAdmin.jsp?PEError";
                response.setHeader("Location", url);
                response.sendRedirect(url);
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
