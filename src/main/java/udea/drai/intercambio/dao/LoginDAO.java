
package udea.drai.intercambio.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import udea.drai.intercambio.dto.login;

/**
 *Clase que contiene todos los metodos DAO necesarios para almacenar, consultar y verificar el ingreso
de los usuarios al sistema.
 * @author Daniel Restrepo & Joaquin Hernandez
 */


public class LoginDAO {

    private BDConexion conexion;

    public LoginDAO() {
        this.conexion = new BDConexion();
    }
    
    /**
     * Método que recibe el nombreUsuario, contraseña y Tipo de usuario y procede a registrarlos en el sistema
    Tipo: Puede contener 3 parametro diferentes
     * @param user Nombre de usuario que se va a registrar
     * @param password Contraseña que se va a registrar
     * @param tipo Entero que puede contener 3 valores, 1= Administrador, 2 = Profesor, 3 = Estudiante
     * @return Int 1 si se realiza el registro correctamente, int 2 si ocurre algún error 
     */
    public int IngresarUsuario(String user, String password, int tipo) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("ingresarUsuarioLogin"));
            ps.setString(1, user);
            ps.setString(2, password);
            ps.setInt(3, tipo);
            ps.executeUpdate();
            System.out.println("Se registro Correctamente el UserPassword en el sistema");
            return 1;

        } catch (Exception ex) {
            System.out.println("Error al ingresar el UserPassword al sistema Error: " + ex);
            return 2;
        } finally {
            conexion.closeConnection();
        }
    }

    /**
     * Método que recibe como parámetro un nombre de Usuario y una contraseña y cambia la contraseña asociada
        en la base de datos al usuario ingresado.
     * @param usuario Nombre del usuario al cual se le va a cambiar la contraseña
     * @param nuevaContrasena Nueva contraseña del usuario.
     * @return Int 1 si la actualización se realiza exitosamente,  0 si ocurre algún error.
     */
    public int modificarContrasena(String usuario, String nuevaContrasena) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("ModificarContrasena"));
            ps.setString(1, nuevaContrasena);
            ps.setString(2, usuario);
            System.out.println(ps.toString());
            ps.executeUpdate();
            return 1;
        } catch (SQLException e) {
            System.out.println("Error modificando la contraseña en la DB");
            return 0;
        } finally {
            conexion.closeConnection();
        }
    }
    /**
     * Método que recibe como parámetro un Nombre de Usuario, va a la DB y recupera toda la información de dicho
    usuario y la retorna en un objeto de la clase Login
     * @param usuario Nombre de usuario en la db
     * @return  Objeto de la clase  Login con la información de la sesion, null si no se recupera nada o ocurre
     * algún error..
     */
    public login obtenerLoginPorUsuario(String usuario) {
        login log = null;
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("obtenerLoginxUsuario"));
            ps.setString(1, usuario);
            System.out.println(ps.toString());
            ResultSet logi = ps.executeQuery();
            if (logi.next()) {
                log = new login();
                log.setContrasena(logi.getString("contrasena"));
                log.setPermiso(logi.getInt("permisos"));
                log.setUsuario(usuario);
            }
            return log;
        } catch (SQLException e) {

            return null;
        } finally {
            conexion.closeConnection();
        }
    }

    /**
     * Método que recibe como parámetro un nombre de Usuario y una contraseña y cambia la contraseña asociada
        en la base de datos al usuario ingresado.
     * @param user nombre de usuario en la db
     * @param pass Nueva contraseña
     * @return True si se realiza la modificación, False si no se realiza la modificación.
     */
    public boolean cambiarPass(String user, String pass) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("cambiarPass"));
            ps.setString(1, pass);
            ps.setString(2, user);
            int logi = ps.executeUpdate();
            if (logi == 1) {
                System.out.println("Modificación de contraseña realizada satisfactoriamente!");
                return true;
            }

        } catch (SQLException e) {
            System.out.println("Error modificando contraseña: " + e);
            return false;
        } finally {
            conexion.closeConnection();
        }
        return false;
    }
    
    /**
     * Método que permite Eliminar los datos de acceso de un usuario por medio de su Nombre de Usuario
    Nota: El nombre de usuario es unico para cada persona.
     * @param usuario Nombre de usuario del usuario que se va a eliminar en la base de datos
     * @return  Int 1 si la eliminación se hace c orrectamente, Int 2 si ocurre algún error.
     */
    public int EliminarLoginPorUsuario(String usuario) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("eliminarLogin"));
            ps.setString(1, usuario);
            System.out.println(ps.toString());
             ps.executeUpdate();
            return 1;
        } catch (SQLException e) {
            System.out.println("ERROR eliminando login: " + e.toString());
            return 2;
        } finally {
            conexion.closeConnection();
        }
    }
}
