
package udea.drai.intercambio.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import udea.drai.intercambio.dto.Semestre;

/**
 * Clase que contiene todos los métodos necesarios para realizar operaciones de los semestres en la 
 * base de datos.
 * @author Daniel Restrepo & Joaquin Hernández
 */
public class SemestreDAO {

    private BDConexion conexion;

    public SemestreDAO() {
        this.conexion = new BDConexion();
    }
    /**
     * Método que permite ingresar un nuevo semestre al sistema
     * @param anio Año del semestre Ejemplo: 2015
     * @param periodo Periodo del semestre, ejemplo: 2
     * @return Int 1 si se ingresa correctamente, 0 si ocurre algún error.
     */
    public int ingresarSemestre(int anio, int periodo) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("ingresarSemestre"));
            ps.setInt(1, anio);
            ps.setInt(2, periodo);
            // al ingresar 1 siginifica que el semestre queda activo
            ps.setInt(3, 1);
            System.out.println(ps.toString());
            ps.executeUpdate();
            return 1;
        } catch (SQLException e) {
            System.out.println("Error al ingresar un nuevo Semestre " + e.toString());
            return 0;
        } finally {
            conexion.closeConnection();
        }
    }
    /**
     * Método que permite editar la información de un semestre ya almacenado en la DB
     * @param anio Año a modificar
     * @param periodo Periodo a modificar
     * @param codigo Codigo del semestre a modificar
     * @return int 1 si se modifica exitosamente, 2 si ocurre algún error.
     */
    public int editarSemestre(int anio, int periodo, int codigo) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("actualizarSemestre"));
            ps.setInt(1, anio);
            ps.setInt(2, periodo);
            ps.setInt(3, codigo);
            System.out.println(ps.toString());
            ps.executeUpdate();
            return 1;
        } catch (SQLException e) {
            System.out.println("Error al actualizar el Semestre " + e.toString());
            return 2;
        } finally {
            conexion.closeConnection();
        }
    }
    /**
     * Método que permite desactivar un semestre para que este no aparezca en los semestre disponibles
     * cuando un estudiante va a ingresasr un nuevo intercambio
     * @param codigo Código del semestre.
     * @return int 1 si se modifica exitosamente, 0 si ocurre algún error.
     */
    public int desactivarSemestre(int codigo) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("desactivarSemestre"));
            ps.setInt(1, codigo);
            System.out.println(ps.toString());
            ps.executeUpdate();
            return 1;
        } catch (Exception e) {
            System.out.println("Erro desactivando el semestre " + e.toString());
            return 0;
        } finally {
            conexion.closeConnection();
        }
    }
    /**
     * Método que mediante el año y el periodo verifica si existe el semestre en la DB.
     * @param anio Año del Semestre
     * @param periodo Perdiodo del semestre
     * @return  True si el semestre existe, False si no existe.
     */
    public boolean existeSemestre(int anio, int periodo) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("existeSemestre"));
            ps.setInt(1, anio);
            ps.setInt(2, periodo);
            System.out.println(ps.toString());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                // Existe el semestre
                return true;
            } else {
                // NO existe el semestre
                return false;
            }
        } catch (SQLException e) {
            System.out.println("Error consultando si un semestre Existe " + e.toString());
            return true;
        } finally {
            conexion.closeConnection();
        }
    }
    /**
     * Método que no recibe parámetros y retorna un List con todos los Semestre almacenados en el Sistema
     * @return Objeto de la clase List con la información de todos los semestres almacenados en la DB 
     */
    public List<Semestre> obtenerSemestres() {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            List<Semestre> semes = new ArrayList();
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("obtenerSemestres"));
            System.out.println(ps.toString());
            ResultSet semestres = ps.executeQuery();
            while (semestres.next()) {
                Semestre nuevo = new Semestre();
                nuevo.setAno(semestres.getInt("ano"));
                nuevo.setPeriodo(semestres.getInt("periodo"));
                nuevo.setCodigo(semestres.getInt("codigo"));
                semes.add(nuevo);
            }
            return semes;

        } catch (Exception e) {
            System.out.println("Error obteniendo los semestres activos desde la DB" + e.toString());
            return null;
        } finally {
            conexion.closeConnection();
        }
    }
    /**
     * ObtienE el semestre correspondiente a un Año y un perido y que ademÁs este activo, revisar el Query
     *para entender mejor el método
     * @param anio Año del semestre
     * @param periodo Perdiodo del semestre
     * @return Objeto de la clase semestre con la información del semestre
     */
    public Semestre obtenerSemestreAnioPeriodo(int anio,int periodo) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("obtenerSemestreAnioPeriodo"));
            ps.setInt(1,anio);
            ps.setInt(2,periodo);
            System.out.println(ps.toString());
            ResultSet semestre = ps.executeQuery();
            Semestre nuevo = new Semestre();
            while (semestre.next()) {
                nuevo.setAno(semestre.getInt("ano"));
                nuevo.setPeriodo(semestre.getInt("periodo"));
                nuevo.setCodigo(semestre.getInt("codigo"));
            }
            return nuevo;
        } catch (Exception e) {
            System.out.println("Error al recuperar la informacion de un semestre mediante su año y periodo" + e.toString());
            return null;
        } finally {
            conexion.closeConnection();
        }
    }
    /**
     * Método que mediante el código de un Semestre permite obtener toda su información y retorna
     * un objeto de la clase Semestre
     * @param cod Código del semestre
     * @return  Objeto de la clase Semestre con la información del semestre.
     */
    public Semestre obtenerSemestreCodigo(int cod) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("obtenerSemestreCodigo"));
            ps.setInt(1, cod);
            System.out.println(ps.toString());
            ResultSet semestre = ps.executeQuery();
            Semestre nuevo = new Semestre();
            while (semestre.next()) {
                nuevo.setAno(semestre.getInt("ano"));
                nuevo.setPeriodo(semestre.getInt("periodo"));
                nuevo.setCodigo(cod);
            }
            return nuevo;
        } catch (Exception e) {
            System.out.println("Error al recuperar la informacion de un semestre mediante su codigo" + e.toString());
            return null;
        } finally {
            conexion.closeConnection();
        }

    }
    /**
     * Método que mediante el código de un semestre obtiene toda la información relacionada a 
     * este semestre pero en un String
     * @param cod Codigo del semestre
     * @return String con ls información del semestre
     */
    public String obtenerSemestreCodigo2(int cod) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            String resultado = "";
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("obtenerSemestreCodigo"));
            ps.setInt(1, cod);
            System.out.println(ps.toString());
            ResultSet semestre = ps.executeQuery();
            Semestre nuevo = new Semestre();
            while (semestre.next()) {
                nuevo.setAno(semestre.getInt("ano"));
                nuevo.setPeriodo(semestre.getInt("periodo"));
                nuevo.setCodigo(cod);
                resultado = nuevo.getAno() + "-" + nuevo.getPeriodo();
            }
            return resultado;
        } catch (Exception e) {
            System.out.println("Error al recuperar la informacion de un semestre mediante su codigo" + e.toString());
            return null;
        } finally {
            conexion.closeConnection();
        }

    }
    /*Metodo Depricate*/
    public void cargarSemestre(Semestre sem) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("cargarSemestre"));
            ps.setInt(1, sem.getCodigo());

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                sem.setAno(rs.getInt("ano"));
                sem.setPeriodo(rs.getInt("periodo"));
            }
            return;
        } catch (Exception ex) {
            System.out.println("Error cargando el semestre!: " + ex);
        } finally {
            conexion.closeConnection();
        }
        return;
    }
}
