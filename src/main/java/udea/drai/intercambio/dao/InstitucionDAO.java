package udea.drai.intercambio.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import udea.drai.intercambio.dto.Institucion;

/**
 *Clase encargada de realizar todas las operacion
 * referentes a la base de datos para las Instituciones
 * @author Joaquin David Hernádez Cárdenas <jdavidhc94@gmail.com>.
 * 
 */
public class InstitucionDAO {

    private BDConexion conexion;

    public InstitucionDAO() {
        this.conexion = new BDConexion();
    }

    /**
     *
     * Método que permite agregar una nueva institución a la base de datos, recibe como parámetro un objeto 
     * de la clase Institución
     * @param inst Objeto de la clase Intitución.
     * @return Integer 1 si se realiza correctamente la inserción, 0 si ocurre algun error.
     */
    public int agregarInstitucion(Institucion inst) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("agregarInstitucion"));
            ps.setString(1, inst.getNombre());
            ps.setString(2, inst.getPais());
            System.out.println(ps.toString());
            ps.executeUpdate();
            ps.close();
            return 1;
        } catch (Exception ex) {
            System.out.println("Error al ingresar una nueva Institucion! Error: " + ex);
            return 2;
        } finally {
            conexion.closeConnection();
        }
    }
    
    /**
     * Método que recibe como parámetro el código, nombre y País de una institución y modifica la institución en 
    la DB  cambiando el nombre y el Pais por los que se le pasan como parámetro.
     * @param codigo Codigo de la institución.
     * @param nombre Nuvo nombre de la institución.
     * @param pais Nombre del país donde esta la institución.
     * @return  Integer 1 si todo se realiza corractamente, 2 si ocurre algún error
     */
    public int editarInstitucion(String codigo, String nombre, String pais) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("actualizarInstitucion"));
            ps.setString(1, nombre);
            ps.setString(2, pais);
            ps.setString(3, codigo);
            System.out.println(ps.toString());
            ps.executeUpdate();
            return 1;
        } catch (SQLException e) {
            System.out.println("Error al actualizar la institucion " + e.toString());
            return 2;
        } finally {
            conexion.closeConnection();
        }
    }
    /**
     * Método Mediante el cual se obtiene las instituciones registradas en el sistema
     * @return Objeto de la clase List que contiene la información de las instituciones.
     */
    public List<Institucion> getInstituciones() {
        List inst = new ArrayList();
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("ObtenerInstituciones"));
            System.out.println(ps.toString());
            ResultSet instituciones = ps.executeQuery();
            while (instituciones.next()) {
                Institucion nueva = new Institucion();
                nueva.setCodigo(Integer.toString(instituciones.getInt("codigo")));
                nueva.setNombre(instituciones.getString("nombre"));
                nueva.setPais(instituciones.getString("pais"));
                inst.add(nueva);
            }
            System.out.println("Las instituciones se recuperaron exitosamente");
            return inst;
        } catch (Exception e) {
            System.out.println("error al sacar las instituciones de la BD, Error= " + e.toString());
        } finally {
            conexion.closeConnection();
        }
        return null;

    }
    
    /**
     * Método que recibe como parámetro el código de una institución y elimina la institución que esta en la 
    Base de datos y que corresponde a la institución Ingresada.
     * @param codigo Codigo de la institucion que se va a eliminar
     * @return Integer 1 si se elimina correctamente, 2 si ocurre algun error.
     */
    public int eliminarInstitucion(int codigo) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("EliminarInstitucion"));
            System.out.println(ps.toString());
            ps.setInt(1, codigo);
            ps.executeUpdate();
            System.out.println("Institucion Eliminada exitosamente");
            return 1;
        } catch (SQLException e) {
            System.out.println("Error al eliminar la Institucion");
            return 2;
        } finally {
            conexion.closeConnection();
        }
    }
}
