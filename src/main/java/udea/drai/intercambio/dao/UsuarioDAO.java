

package udea.drai.intercambio.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import udea.drai.intercambio.dto.login;

/**
 *Clase que contiene los métodos necesarios para realizar operaciones de los usuarios en la DB
 * @author Joaquin Hernández Cárdenas <jdavidhc94@gmail.com>
 */
public class UsuarioDAO {
    private BDConexion conection;

    public UsuarioDAO() {
          this.conection = new BDConexion();
    }
    
    /**
     * Método que permite obtener la información de un Uusario en la DB pór medio de su nombre de usuario 
     * @param user Nombre de usuario
     * @return  Objeto ede la clase login con la información de autenticación.
     */
    public login verificacion(String user){
         login lg = null;
        try{
             if (conection.getConnection().isClosed()) {
                conection = new BDConexion();
            }
           // conection = new BDConexion();
            Connection con = this.conection.getConnection();
            PreparedStatement st = con.prepareStatement(BDConexion.getStatement("verficarUsuario"));
            
            st.setString(1, user);
            System.out.println(st.toString());
            ResultSet rs= st.executeQuery();
            
           if(rs.next()){
               lg = new login();
                lg.setUsuario(user);
                lg.setContrasena(rs.getString("contrasena"));
                lg.setPermiso(rs.getInt("permisos"));
           }
           
        }catch(Exception e){
            System.out.println("Error extrayendo información de usuario!");
            return null;
        } finally {
            conection.closeConnection();
        }

        return lg;
    }
    
}
