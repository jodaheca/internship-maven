
package udea.drai.intercambio.dao;

import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import udea.drai.intercambio.dto.Requisito;
import udea.drai.intercambio.dto.Revision;
import udea.drai.intercambio.dto.RevisionCompleta;

/**
 *Clase Mediante la cual se puede acceder a las revisiones de un Estudiante.
    Revision = Tareas que el profesor va tachando a medida que un estudiante avanza en su Intercambio
 * @author Joaquin David Hernández Cárdenas <jdavidhc94@gmail.com>
 */
public class RevisionDAO {

    private BDConexion conexion;

    public RevisionDAO() {
        this.conexion = new BDConexion();
    }
    /**
     * Método que recibe como parametro la identificación de un estudiante y el código de un intermcabio,
     * mmediante estos parámetro realiza una consulta a la DB  y retorna todas las Revisiones que estan 
     * asociadas a ese estudiante en el intercambio seleccionado.
     * @param estudiante Identificación del estudiante
     * @param intercambio Código del intercambio
     * @return. Objeto de la clase List con la información de las revisiones
     */
    public List obtenerRevisiones(String estudiante, int intercambio) {
        List rev = new ArrayList();
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("obtenerRevisiones"));
            ps.setString(1, estudiante);
            ps.setInt(2, intercambio);
            System.out.println(ps.toString());
            ResultSet revisiones = ps.executeQuery();
            while (revisiones.next()) {
                Revision nuevo = new Revision();
                nuevo.setCodigo(revisiones.getInt("codigo"));
                nuevo.setCodRequisito(revisiones.getInt("codRequisito"));
                nuevo.setEstado(revisiones.getString("estado"));
                nuevo.setIntercambio(revisiones.getInt("intercambio"));
                nuevo.setEstudiante(revisiones.getString("estudiante"));
                rev.add(nuevo);
            }
            return rev;
        } catch (Exception e) {
            System.out.println("Error recuperando las Revisiones de un estudiante " + e.toString());
            return null;
        } finally {
            conexion.closeConnection();
        }
    }
    /**
     * Método que recibe como parámetro la identificación de un Estudiante y el código de un Intercabio y 
     * obtiene una lista de obejetos RevisionCompleta.
     *Nota:Para entender mejor la diferencia de este metodo con el anterior, revisar los query de cada metodo.
     * @param estudiante Identificación del estudiante.
     * @param intercambio Código del intercambio 
     * @return Objeto de la clase List con la información de las Revisiones
     */
    public List obtenerRevisiones2(String estudiante, int intercambio) {
        List rev = new ArrayList();
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("obtenerRevisionEstudiante"));
            ps.setString(1, estudiante);
            ps.setInt(2, intercambio);
            System.out.println(ps.toString());
            ResultSet revisiones = ps.executeQuery();
            while (revisiones.next()) {
                RevisionCompleta nuevo = new RevisionCompleta();
                nuevo.setCodigo(revisiones.getInt("codigo"));
                nuevo.setEstado(revisiones.getString("estado"));
                nuevo.setIntercambio(revisiones.getInt("intercambio"));
                nuevo.setEstudiante(revisiones.getString("estudiante"));
                nuevo.setNombre(revisiones.getString("nombre"));
                nuevo.setComentario(revisiones.getString("comentario"));
                nuevo.setNombreEstudiante(revisiones.getString("nombreEstudiante") + " " +revisiones.getString("apellidoEstudiante"));

                rev.add(nuevo);
            }
            return rev;
        } catch (SQLException e) {
            System.out.println("Error recuperando las Revisiones de un estudiante " + e.toString());
            return null;
        } finally {
            conexion.closeConnection();
        }
    }
    /**
     * Método que modifica la información de una Revsion en la DB
     * @param codigo Código de la revisión.
     * @param estado Estado de la revisión.
     * @param comentario Cómentario que el profesor hace sobre la revisión.
     * @return Int 1 si se modifica exitosamente. 2 si ocurre algún error.
     */ 
    public int modificarEstadoRevision(int codigo, String estado, String comentario) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("modificarEstadoRevision"));
            ps.setString(1, estado);
            ps.setString(2, comentario);
            ps.setInt(3, codigo);
            ps.executeUpdate();
            return 1;

        } catch (SQLException e) {
            System.out.println("Error modificando el estado de una revison de un estudiante en un semestre especifico" + e.toString());
            return 0;
        } finally {
            conexion.closeConnection();
        }
    }
    /**
     * Método que permite ingresar las revisiones de un estudiante en un Intercabio en la DB
     *Este método recibe la Lista de los requisitos y los convierte en revisiones para un estudiante 
     * en un Intercabio asociado.
     * @param requisitos Objeto de la clase List con los requisitos establecidos por los profesores.
     * @param intercambio codigo del intercambio asociado.
     * @param estudiante Identificación del estudiante al cual se le van a crear las revisiones
     * @return int 1 si se ingresan correctamente, o si ocurre algún error.
     */ 
    public int ingresarRevisones(List requisitos, int intercambio, String estudiante) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            for (int i = 0; i < requisitos.size(); i++) {
                try {

                    Requisito actual = (Requisito) requisitos.get(i);
                    PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("ingresarRevision"));
                    ps.setInt(1, actual.getCodigo());
                    ps.setString(2, "P");
                    ps.setString(3, estudiante);
                    ps.setInt(4, intercambio);
                    System.out.println(ps.toString());
                    ps.executeUpdate();
                } catch (SQLException e) {
                    System.out.println("Ocurrio un error ingresando una o mas revisiones de un intermcabio");
                    return 0;
                }
            }
            return 1;
        } catch (Exception e) {
            System.out.println("Error ingresando las revisiones de un intercambio");
            return 0;
        } finally {
            conexion.closeConnection();
        }
    }
}
