
package udea.drai.intercambio.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import udea.drai.intercambio.dto.EstudianteIntercambio;
import udea.drai.intercambio.dto.Persona;

/**
 *  Clase que contiene todos los metodos DAO que permiten recuperar la información de todos los estudiantes
 * regitrados en el sistema.
 * @author Joaquin David Hernádez Cárdenas <jdavidhc94@gmail.com>.
 */
public class EstudianteDAO {

    private BDConexion conexion;

    /**
     * Constructor de la vacio clase
     */
    public EstudianteDAO() {
        this.conexion = new BDConexion();
    }
    /**
     * Metodo que recibe como parametro un objeto de la clase Persona  y se encarga de realizar las modificaciones
       enviadas en este objeto, busca en la DB una persona que responda a este numero de identificacion y cambia 
       la informacion almacenada por la informacion que contiene el objeto.
     * @param e Objeto de la clase Persona
     * @return  Retorna un entero con valor 0 si la operacion se realiza exitosamente
     */
    public int modificarInfo(Persona e) {

        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("modificarEstudiante"));
            ps.setString(1, e.getNombre());
            ps.setString(2, e.getApellido());
            ps.setString(4, e.getPrograma());
            ps.setString(3, e.getEmail());
            ps.setString(5, e.getCedula());
            ps.setString(6, e.getUsuario());
            if (ps.executeUpdate() == 1) {
                System.out.println("Modificacion exitosa!");

            }
        } catch (Exception ex) {
            System.out.println("Error en la modificación! Error: " + ex);

        } finally {
            conexion.closeConnection();
        }
        return 0;

    }
    
    /**
     * Metodo que recibe como parameto el codigo de una institucion y la identificacion de un profesor
        Retorna una lista con los estudiantes que estan de intercabio en la institucion recibida y a cargo del 
        profesor pasado como parametro.
     * @param codInstitucion Codigo de la institucion. 
     * @param profesor Identificacion del profesor.
     * @return  Objeto de la clase List que contiene los estudiantes que estan de intercambio en la 
     * institución pasada como parametro y que estan a cargo del profesor pasado como parametro 
     */
    public List obtenerEstudiatesxInstitucionProfesor(String codInstitucion, String profesor) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            List resultado = new ArrayList();
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("estudiantexInstitucionProfesor"));
            ps.setString(1, codInstitucion);
            ps.setString(2, profesor);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                EstudianteIntercambio es = new EstudianteIntercambio();
                es.setCedula(rs.getString("cedula"));
                es.setNombre(rs.getString("nombre"));
                es.setApellido(rs.getString("apellido"));
                es.setTipoIntercambio(rs.getString("TipoIntercambio"));
                es.setFechaIda(rs.getDate("fechaIda"));
                es.setFechaRegreso(rs.getDate("fechaRetorno"));
                es.setSemestreInicio(rs.getInt("SemestreInicio"));
                es.setSemestreFin(rs.getInt("SemestreFin"));
                es.setPrograma(rs.getString("Programa"));
                resultado.add(es);
            }
            return resultado;
        } catch (SQLException e) {
            System.out.println("Error recuperando la informacion de los estudiantes en una institucion" + e.toString());
            return null;
        } finally {
            conexion.closeConnection();
        }
    }
   
    /**
     *Este metodo deberia estar en una clase DAO llamada Profesor, sin embargo esta en esta clase por cumplir
    con las mismas propiedas que para almacenar un Estudiante, con la excepción que no tiene un codigo de 
    programa asignado 
     * @param estudiante Objeto de la clase Estudiante
     * @return  Entero que es 1 si la ctualización se realiza correctamente y 2 si ocurre algun error.
     */
     public int editarEstudiante(Persona estudiante) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("actualizarProfesor"));
            ps.setString(1, estudiante.getNombre());
            ps.setString(2, estudiante.getApellido());
            ps.setString(3, estudiante.getEmail());
            ps.setString(4, estudiante.getCedula());
            System.out.println(ps.toString());
            ps.executeUpdate();
            System.out.println("El Profesor de cedula:" +estudiante.getCedula()+"Se actualizo correctamente");
            return 1;
        } catch (SQLException e) {
            System.out.println("Error al actualizar Profesor " + e.toString());
            return 2;
        } finally {
            conexion.closeConnection();
        }
    }
    
     /**
      * Este metodo recibe como parametro el codigo de una institución y retorna una lista con la información de
        todos los estudiantes que han estado o estan de intercambio en dicha institución. 
      * @param codInstitucion Codigo de la intitición a Evaluar
      * @return  Objeto de la clase List que contiene los estudiantes.
      */
    public List obtenerEstudiatesxInstitucionAdmin(String codInstitucion) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            List resultado = new ArrayList();
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("estudiantexInstitucionAdmin"));
            ps.setString(1, codInstitucion);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                EstudianteIntercambio es = new EstudianteIntercambio();
                es.setCedula(rs.getString("cedula"));
                es.setNombre(rs.getString("nombre"));
                es.setApellido(rs.getString("apellido"));
                es.setTipoIntercambio(rs.getString("TipoIntercambio"));
                es.setFechaIda(rs.getDate("fechaIda"));
                es.setFechaRegreso(rs.getDate("fechaRetorno"));
                es.setSemestreInicio(rs.getInt("SemestreInicio"));
                es.setSemestreFin(rs.getInt("SemestreFin"));
                es.setPrograma(rs.getString("Programa"));
                resultado.add(es);
            }
            return resultado;
        } catch (SQLException e) {
            System.out.println("Error recuperando la informacion de los estudiantes en una institucion" + e.toString());
            return null;
        } finally {
            conexion.closeConnection();
        }
    }
    
    /**
     *Metodo que recibe como parametro el codigo de un programa, retorna una lista con la informacion de los
    estudiantes que pertenecen al progama seleccionado. 
     * @param programa Codigo del programa
     * @return  Objeto de la clase List con la información de los Estudiantes
     */
    public List getEstudiantexPrograma(String programa) {
        List estudiantes = new ArrayList();
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("estudiantexPrograma"));
            ps.setString(1, programa);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String nombre = rs.getString("nombre");
                String apellido = rs.getString("apellido");
                String cedula = rs.getString("cedula");
                String prog = rs.getString("programa");
                String email = rs.getString("email");
                String ud = rs.getString("usuario");
                Persona e = new Persona(nombre, apellido, cedula, prog, email, ud);
                estudiantes.add(e);
            }
            return estudiantes;
        } catch (Exception e) {
            System.out.println("Error recuperando la informacion de los estudiantes el el programa" + programa);
            return null;

        } finally {
            conexion.closeConnection();
        }
    }
    
    /**
     * Metodo que recibe como parametro el codigo de un tipo de intercambio, retorna una lista con todos los 
    estudiantes que tenga un intercambion del tipo recibido como parametro. 
     * @param tipo Codigo del tipo de intercambio consultado
     * @return  Objeto de la Clase List que contiene la información de los estudiantes.
     */
    public List getEstudiantexTipoIntercambio(String tipo) {
        List resultado = new ArrayList();
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("estudiantexTipoIntercambioAdmin"));
            ps.setString(1, tipo);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                EstudianteIntercambio es = new EstudianteIntercambio();
                es.setCedula(rs.getString("cedula"));
                es.setNombre(rs.getString("nombre"));
                es.setApellido(rs.getString("apellido"));
                es.setInstitucion(rs.getString("Institucion"));
                es.setFechaIda(rs.getDate("fechaIda"));
                es.setFechaRegreso(rs.getDate("fechaRetorno"));
                es.setSemestreInicio(rs.getInt("SemestreInicio"));
                es.setSemestreFin(rs.getInt("SemestreFin"));
                es.setPrograma(rs.getString("Programa"));
                resultado.add(es);
            }
            return resultado;
        } catch (Exception e) {
            System.out.println("Error recuperando la informacion de los estudiantes en un intercambio especifico" + e.toString());
            return null;

        } finally {
            conexion.closeConnection();
        }
    }
    
    /**
     * Metodo que recibe como parámetro un tipo de intercambio y la identificación de un profesor,
    retorna una lista con los estudiantes que tienen el tipo de intercambio pasado como parámetro y que 
    además tiene el profesor ingresado como encargado del programa al que pertenecen. 
     * @param tipo codigo del tipo de intercambio
     * @param profesor Identificación del profesor
     * @return Objeto de la clase List que contiene los estudiantes encontrados. 
     */
    public List getEstudiantexTipoIntercambio2(String tipo, String profesor) {
        List resultado = new ArrayList();
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("estudiantexTipoIntercambioProfesor"));
            ps.setString(1, tipo);
            ps.setString(2, profesor);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                EstudianteIntercambio es = new EstudianteIntercambio();
                es.setCedula(rs.getString("cedula"));
                es.setNombre(rs.getString("nombre"));
                es.setApellido(rs.getString("apellido"));
                es.setInstitucion(rs.getString("Institucion"));
                es.setFechaIda(rs.getDate("fechaIda"));
                es.setFechaRegreso(rs.getDate("fechaRetorno"));
                es.setSemestreInicio(rs.getInt("SemestreInicio"));
                es.setSemestreFin(rs.getInt("SemestreFin"));
                es.setPrograma(rs.getString("Programa"));
                es.setCodigoIntercambio(Integer.parseInt(rs.getString("codIntercambio")));
                resultado.add(es);
            }
            return resultado;
        } catch (Exception e) {
            System.out.println("Error recuperando la informacion de los estudiantes en un intercambio especifico" + e.toString());
            return null;

        } finally {
            conexion.closeConnection();
        }
    }
    /**
     * Método que recibe como parámetro el código de un programa y la identificación de un profesor, retorna una
    lista con la información de los estudiantes que pertenecen al programa ingresado y que están a cargo del 
    profesor pasado como parámetro. 
     * @param programa Código del programa.
     * @param encargado Identificación del profesor encargado del programa.
     * @return  Objeto de la clase List con la información del los estudiantes.
     */
    public List getEstudiantexPrograma2(String programa, String encargado) {
        List estudiantes = new ArrayList();
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("estudiantexPrograma2"));
            ps.setString(1, programa);
            ps.setString(2, encargado);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String nombre = rs.getString("nombre");
                String cedula = rs.getString("cedula");
                String prog = rs.getString("programa");
                String email = rs.getString("email");
                String ud = rs.getString("usuario");
                String apellido = rs.getString("apellido");
                Persona e = new Persona(nombre, apellido, cedula, prog, email, ud);
                estudiantes.add(e);
            }
            return estudiantes;
        } catch (Exception e) {
            System.out.println("Error recuperando la informacion de los estudiantes el el programa" + programa);
            return null;

        } finally {
            conexion.closeConnection();
        }
    }
 
    /**
     *Método que recibe como parámetro el nombre de usuario de un estudiante y retorna toda la información 
    del estudiante que está identificado con el usuario pasado como parámetro 
     * @param user Username del estudiante
     * @return  Objeto de la clase Persona con la información del estudiante.
     */
    public Persona getEstudianteXusuario(String user) {
        Persona e = null;

        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("estudianteXusuario"));
            ps.setString(1, user);
            System.out.println(ps.toString());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String nombre = rs.getString("nombre");
                String cedula = rs.getString("cedula");
                String prog = rs.getString("programa");
                String email = rs.getString("email");
                String ud = rs.getString("usuario");
                String apellido = rs.getString("apellido");
                e = new Persona(nombre, apellido, cedula, prog, email, ud);
                return e;
            }
        } catch (Exception ex) {
            System.out.println("Error obteniendo estudiante " + ex);
        } finally {
            conexion.closeConnection();
        }

        return e;
    }
    
    /**
     * Método que recibe como parámetro la identificación de un estudiante y retorna toda la información del 
    estudiante que corresponde a la identificaión ingresada. 
     * @param ced Identificación del estudiante
     * @return Objeto de la clase Persona con la información del estudiante.
     */
    public Persona getEstudianteXcedula(String ced) {
        Persona e = null;
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("InfoEstudiantexCedulaAdmin"));
            ps.setString(1, ced);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String nombre = rs.getString("nombre");
                String cedula = rs.getString("cedula");
                String prog = rs.getString("programa");
                String email = rs.getString("email");
                String ud = rs.getString("usuario");
                String apellido = rs.getString("apellido");
                e = new Persona(nombre, apellido, cedula, prog, email, ud);

            }
            /**
             * De una vez se verifica si el estudiante tiene intercambio
             */
             if(e!=null){ 
                ps = con.prepareStatement(BDConexion.getStatement("obtenerNombreTipoIntercambio"));
                ps.setString(1, e.getCedula());
                rs = ps.executeQuery();
                if(rs.next()){
                  e.setNombreIntercambio(rs.getString("nombreTipo")); 
                }else{
                    e.setNombreIntercambio("No tiene Intercambio");
                }
             } 

        } catch (Exception ex) {
            System.out.println("Error obteniendo estudiante " + ex);
            return null;
        } finally {
            conexion.closeConnection();
        }
        return e;
    }
    
    /**
     * Método que recibe como parámetro la identificación de un estudiante y la de un profesor, si el estudiante
    seleccionado esta bajo la responsabilidad del profesor recibido como parámetro entonces, este es retornado
    de lo contrario se retorna un null. 
     * @param ced Identificación del estudiante
     * @param profesor Identificación del profesor
     * @return Objeto de la clase Persona si se cumple la condicion, Null en caso que no se cumpla.
     */
    public Persona getEstudianteXcedula2(String ced, String profesor) {
        Persona e = new Persona();
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("InfoEstudiantexCedulaProfesor"));
            ps.setString(1, profesor);
            ps.setString(2, ced);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String nombre = rs.getString("nombre");
                String cedula = rs.getString("cedula");
                String prog = rs.getString("programa");
                String email = rs.getString("email");
                String ud = rs.getString("usuario");
                String apellido = rs.getString("apellido");
                e = new Persona(nombre, apellido, cedula, prog, email, ud);
                return e;
            }
        } catch (Exception ex) {
            System.out.println("Error obteniendo estudiante " + ex);
            return null;
        } finally {
            conexion.closeConnection();
        }
        e.setNombre("NOEXISTE");
        return e;
    }
    
    /**
     * Método que recibe como parámetro un objeto de la clase Persona, con la información basica de un estudiante
    es decir, cedula, programa, usuario, tipoDeUsuario y contraseña, estos son los parámetro que ingresa
    el profesor cuando esta registrando un nuevo estudiante, posteriormente ingresa el estudiante con 
    el usuario y la contraseña asignada por el profesor e ingresa todos los datos restantes, Nombre, Apellidos
    Correo, entre otros, este método retorna un 1 si la operación se llevo a cabo correctamente o 2 si ocurrio
    algún error almacenando la información. 
     * @param student Objeto de la clase Persona.
     * @return Int 1 si es exitoso, Int 2 si ocurrio algún problema.
     * @throws SQLException  Si ocurre algún error sobre la base de datos.
     */
    public int IngresarEstudiante(Persona student) throws SQLException {
        String tipoUsuario = "3";
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("ingresarEstudiante"));
            ps.setString(1, student.getCedula());
            ps.setString(2, student.getUsuario());
            ps.setString(3, tipoUsuario);
            ps.setString(4, student.getPrograma());
            ps.executeUpdate();
            System.out.println("El estudiante se registro exitosamente en la BD");
            return 1;
        } catch (Exception e) {
            System.out.println("Error al registrar estudiante " + e.toString());
            return 2;
        } finally {
            conexion.closeConnection();
        }
    }
   
    /**
     * Método que no recibe parámetros y que retorna una lista con todos los estudiantes almacenados en el 
    sistema, retorna incluso los estudiantes que solo tiene ingresado la cédula y el programa 
     * @return Objeto de la clase List con la información de todos los estudiantes.
     */
    public List<Persona> getEstudiantes() {
        Persona e = null;
        List estudiantes = new ArrayList();

        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("estudiantes"));
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String nombre = rs.getString("nombre");
                String cedula = rs.getString("cedula");
                String prog = rs.getString("programa");
                String email = rs.getString("email");
                String ud = rs.getString("usuario");
                String apellido = rs.getString("apellido");
                e = new Persona(nombre, apellido, cedula, prog, email, ud);
                estudiantes.add(e);
            }
            return estudiantes;
        } catch (Exception ex) {
            System.out.println("Error obteniendo estudiante " + ex);
            return null;
        } finally {
            conexion.closeConnection();
        }
    }
   
    /**
     * Método que no recibe parámetros y que retorna una lista con todos los estudiantes almacenados en el 
    sistema, retorna solo los estudiantes que ya tiene nombre, apellidos y correo ingresado.
     * @return Objeto de la clase List con la información de todos los estudiantes.
     */
     public List<Persona> getEstudiantes2() {
        Persona e = null;
        List estudiantes = new ArrayList();

        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("estudiantes"));
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String nombre = rs.getString("nombre");
                String cedula = rs.getString("cedula");
                String prog = rs.getString("programa");
                String email = rs.getString("email");
                String ud = rs.getString("usuario");
                String apellido = rs.getString("apellido");
                e = new Persona(nombre, apellido, cedula, prog, email, ud);
                // con este if verificamos que no se muestren en la lista estudiantes que aun no entran al sistema
                if(e.getNombre()!=null && e.getApellido()!=null && e.getNombre().length()!=0 && e.getApellido().length()!=0){
                estudiantes.add(e);
                }
            }
            return estudiantes;
        } catch (Exception ex) {
            System.out.println("Error obteniendo estudiante " + ex);
            return null;
        } finally {
            conexion.closeConnection();
        }
    }
     
     /**
      * Método que recibe como parámetro la identificación de un profesor y retorna una lista con todos los 
     alumnos que están a cargo del profesor pasado como parámetro. 
      * @param profesor Identificación del profesor.
      * @return  Objeto de la clase List con la información de los estudiantes.
      */
    public List getEstudianteXProfesor(String profesor) {
        List estudiantes = new ArrayList();
        Persona e = null;

        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("obtenerEstudiantesProfesor"));
            ps.setString(1, profesor);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String nombre = rs.getString("nombre");
                String apellido = rs.getString("apellido");
                String cedula = rs.getString("cedula");
                String prog = rs.getString("programa");
                String email = rs.getString("email");
                String ud = rs.getString("usuario");

                e = new Persona(nombre, apellido, cedula, prog, email, ud);
                estudiantes.add(e); 
            }
            for (int i = 0; i < estudiantes.size();i++) {
                Persona estudiante =(Persona) estudiantes.get(i);
                ps = con.prepareStatement(BDConexion.getStatement("obtenerNombreTipoIntercambio"));
                ps.setString(1, estudiante.getCedula());
                rs = ps.executeQuery();
                if(rs.next()){
                  estudiante.setNombreIntercambio(rs.getString("nombreTipo")); 
                }else{
                    estudiante.setNombreIntercambio("No tiene Intercambio");
                }
               estudiantes.set(i, estudiante);
            }
            return estudiantes;
        } catch (SQLException ex) {
            System.out.println("Error obteniendo los estudiantes de un profesor " + ex.toString());
            return null;
        } finally {
            conexion.closeConnection();
        }

    }
    
    /**
     * Este método recibe como parámetro la identificación de un profesor y 
    retorna los estudiantes que ya tienen asignado el nombre y el apellido en la db y que están 
    a cargo de profesor pasado como parámetro. 
     * @param profesor Identificación del profesor pasado como parámetro.
     * @return  Objeto de la clase List que contiene la información de los estudiantes.
     */
     public List getEstudianteXProfesor2(String profesor) {
        List estudiantes = new ArrayList();
        Persona e = null;

        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("obtenerEstudiantesProfesor"));
            ps.setString(1, profesor);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String nombre = rs.getString("nombre");
                String apellido = rs.getString("apellido");
                String cedula = rs.getString("cedula");
                String prog = rs.getString("programa");
                String email = rs.getString("email");
                String ud = rs.getString("usuario");

                e = new Persona(nombre, apellido, cedula, prog, email, ud);
                // con este if verificamos que no se muestren en la lista estudiantes que aun no entran al sistema
                if(e.getNombre()!=null && e.getApellido()!=null && e.getNombre().length()!=0 && e.getApellido().length()!=0){
                estudiantes.add(e);
                }
            }
            return estudiantes;
        } catch (SQLException ex) {
            System.out.println("Error obteniendo los estudiantes de un profesor " + ex.toString());
            return null;
        } finally {
            conexion.closeConnection();
        }

    }

}
