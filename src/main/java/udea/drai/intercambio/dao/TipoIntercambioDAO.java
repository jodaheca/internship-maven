
package udea.drai.intercambio.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import udea.drai.intercambio.dto.TipoIntercambio;

/**
 * Clase que contiene los metodos que permiten hacer operaciones sobre la clase TipoIntercambio en la DB
 * @author Joaquin David Hernández Cárdenas <javidhc94@gmail.com>
 */
public class TipoIntercambioDAO {

    private BDConexion conexion;

    public TipoIntercambioDAO() {
        this.conexion = new BDConexion();
    }
    /**
     * Método que retorna una Lista con todos los tipos de intercabios almacenados en la DB
     * @return  Objeto de la clase List con toda la información de los tipos de intercambio.
     */
    public List obtenerTipoIntercambios(){
        List resultado=new ArrayList();
        try{
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("obtenerTipoIntercambio"));
            System.out.println(ps.toString());
            ResultSet tipos= ps.executeQuery();
            while(tipos.next()){
                TipoIntercambio nuevo = new TipoIntercambio();
                nuevo.setCodigo(Integer.toString(tipos.getInt("codigo")));
                nuevo.setNombre(tipos.getString("nombreTipo"));
                nuevo.setNumSemestres(tipos.getString("numSemestres"));
                resultado.add(nuevo);
            }
            return resultado;
        }catch(SQLException e){
            System.out.println("Error sacando los tipos de intercabios de la DB "+ e.toString());
            return null;
        } finally {
            conexion.closeConnection();
        }

    }
    /**
     * Método que mediante el código de un tipo de semestre retorna toda la información
     * asociada al Tipo de Intercambio
     * @param i Objeto de la clase TipoIntercambio que contiene el código.
     * @return  Objeto de la clase TipoIntercambio con la información del Tipo de intercambio
     */
    public TipoIntercambio getIntercambio(TipoIntercambio i){
        try{
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("getTipoIntercambio"));
            ps.setInt(1, Integer.parseInt(i.getCodigo()));
            ResultSet tipos= ps.executeQuery();
            while(tipos.next()){
                i.setNombre(tipos.getString("nombreTipo"));
                i.setNumSemestres(tipos.getString("numSemestres"));
            }
            return i;
        }catch(SQLException e){
            System.out.println("Error sacando los tipos de intercabios de la DB "+ e.toString());
            return null;
        } finally {
            conexion.closeConnection();
        }
    }

}
