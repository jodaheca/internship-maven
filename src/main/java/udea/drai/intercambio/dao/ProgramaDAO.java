
package udea.drai.intercambio.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import udea.drai.intercambio.dto.Persona;
import udea.drai.intercambio.dto.Programa;

/**
 *Clase que contiene todos los métodos DAO con los que se puede acceder a la DB y obtener información de los 
programas academicos.
 * @author Daniel Restrepo & Joaquin Hernandez
 */
public class ProgramaDAO {

    private BDConexion conexion;

    public ProgramaDAO() {
        this.conexion = new BDConexion();
    }
    /**
     * Método que recibe como parámetro un objeto de la clase Programa y lo almacena en la DB
     * @param pr Objeto de la clase Programa que se va a almacenar en la base de datos.
     * @return Int 1 si se ingresa correctamente, 2 si ocurre algún error.
     */
    public int agregarPrograma(Programa pr) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("agregarPrograma"));
            ps.setString(1, pr.getCodigo());
            ps.setString(2, pr.getNombre());
            ps.setString(3, pr.getDirector());

            System.out.println(ps.toString());
            ps.executeUpdate();
            ps.close();
            return 1;
        } catch (Exception ex) {
            System.out.println("Error al ingresar un nuevo programa! Error: " + ex);
            return 2;
        } finally {
            conexion.closeConnection();
        }

    }
    /**
     * Método que permite editar la información de un programa en la DB
     * @param programa Objeto de la clase Programa con la información del programa a modificar.
     * @return Int 1 si se actualiza exitosamente, 2 si ocurre algún error.
     */
     public int editarPrograma(Programa programa) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("actualizarPrograma"));
            ps.setString(1, programa.getNombre());
            ps.setString(2, programa.getDirector());
            ps.setString(3, programa.getCodigo());
            System.out.println(ps.toString());
            ps.executeUpdate();
            System.out.println("El programa de codigo:" +programa.getCodigo()+"Se actualizo correctamente");
            return 1;
        } catch (SQLException e) {
            System.out.println("Error al actualizar programa " + e.toString());
            return 2;
        } finally {
            conexion.closeConnection();
        }
    }
     /**
      * Método que recibe como parámetro el código de identificación de un Programa y lo elimina de la DB.
      * @param codigo Código del programa a eliminar.
      * @return 1 en caso de que la eliminación se realice exitosamete o 2 en caso contrario
      */
    public int eliminarPrograma(int codigo) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("eliminarPrograma"));
            System.out.println(ps.toString());
            ps.setInt(1, codigo);
            ps.executeUpdate();
            System.out.println("Programa Eliminado exitosamente");
            return 1;
        } catch (SQLException e) {
            System.out.println("Error al eliminar el programa");
            return 2;
        } finally {
            conexion.closeConnection();
        }
    }
    /**
     * Método que recibe como parámetro la identificación de un profesor y retorna un objeto de la clase List 
    que contiene todos los programas de los cuales el profesor es Director
     * @param id Identificación del profesor
     * @return Objeto de la clase List con los programas en los cuales el profesor es director.
     */
    public List<Programa> getProgramas(String id) {
        List inst = new ArrayList();
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("ObtenerProgramasProfesor"));
            ps.setString(1, id);
            System.out.println(ps.toString());
            ResultSet programas = ps.executeQuery();
            while (programas.next()) {
                Programa nueva = new Programa();
                nueva.setCodigo(Integer.toString(programas.getInt("codigo")));
                nueva.setNombre(programas.getString("nombre"));
                nueva.setDirector(programas.getString("director"));
                inst.add(nueva);
            }
            System.out.println("Los programas se recuperaron exitosamente");
            return inst;
        } catch (Exception e) {
            System.out.println("error al sacar los programas de la BD, Error= " + e.toString());
        } finally {
            conexion.closeConnection();
        }
        return null;

    }
    /**
     * Método que no recibe parámetros y retorna un objeto de la clase List con la información de todos los 
    programas almacenados en la DB
     * @return Objeto de la clase List con la información de los programas almacenados en la DB
     */
    public List<Programa> getProgramas() {
        List inst = new ArrayList();
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("ObtenerTodosLosProgramas"));
            System.out.println(ps.toString());
            ResultSet programas = ps.executeQuery();
            while (programas.next()) {
                Programa nueva = new Programa();
                nueva.setCodigo(Integer.toString(programas.getInt("codigo")));
                nueva.setNombre(programas.getString("nombre"));
                nueva.setDirector(programas.getString("director"));
                inst.add(nueva);
            }
            System.out.println("Los programas se recuperaron exitosamente");
            return inst;
        } catch (Exception e) {
            System.out.println("error al sacar los programas de la BD, Error= " + e.toString());
        } finally {
            conexion.closeConnection();
        }
        return null;

    }
    /**
     * Método que mediante el código de un programa permite obtener toda la información del programa
     * @param prog Código del programa
     * @return  Objeto de la clase Programa con la información del programa.
     */
    public Programa obtenerNombre(String prog) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("consultarPrograma"));
            ps.setString(1, prog);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Programa pr = new Programa(rs.getString("codigo"), rs.getString("nombre"));
                return pr;
            }

        } catch (Exception e) {
            System.out.println("Error obteniendo programa! " + e);
        } finally {
            conexion.closeConnection();
        }

        return null;
    }
    /**
     * Método que mediante el código de un programa permite obtener la información 
     * del director de dicho programa
     * @param codigo Código del programa
     * @return  Objeto de la clase Persona con la información del director del programa
     */
    public Persona profesorDePrograma(String codigo) {
        Persona profesor = null;
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("obtenerDirectorPrograma"));
            ps.setString(1, codigo);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                profesor = new Persona();
                profesor.setCedula(rs.getString("cedula"));
                profesor.setNombre(rs.getString("nombre"));
                profesor.setEmail(rs.getString("email"));
                profesor.setApellido(rs.getString("apellido"));
                return profesor;
            }

        } catch (Exception e) {
            System.out.println("Error obteniendo programa! " + e);
        } finally {
            conexion.closeConnection();
        }

        return profesor;
    }

}
