
package udea.drai.intercambio.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import udea.drai.intercambio.dto.Persona;
import udea.drai.intercambio.dto.Programa;

/**
 * Clase que contiene todos los métodos DAO que permiten acceder a la información de los pofesores en la DB
 * @author Joaquin Hernández Cárdenas <jdavidhc94@gmail.com>
 */

public class ProfesorDAO {

    private BDConexion conexion;

    public ProfesorDAO() {
        this.conexion = new BDConexion();
    }
    /**
     * Método que recibe como parámetro una lista con programas academicos y retorna una lista ordenada con los 
    Directores de cada programa.
     * @param programas List con los programas
     * @return  Objeto de la clase List con los Directores de cada programa.
     */
    public List getDirectores(List programas) {
        List direct = new ArrayList();
        Persona e;
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("personaPorCedula"));
            for (int i = 0; i < programas.size(); i++) {
                Programa actual = (Programa) programas.get(i);
                ps.setString(1, actual.getDirector());
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    String nombre = rs.getString("nombre");
                    String cedula = rs.getString("cedula");
                    String email = rs.getString("email");
                    String ud = rs.getString("usuario");
                    String apellido = rs.getString("apellido");
                    e = new Persona(nombre, apellido, cedula, "", email, ud);
                    direct.add(e);
                }
            }
            return direct;
        } catch (SQLException ex) {
            System.out.println("Error obteniendo directores de los programas " + ex);
            return null;
        } finally {
            conexion.closeConnection();
        }
    }

    /**
     * Método que no recibe parámetro y retorna un objeto List con todos los profesores que están almacenados
    en la DB
     * @return Onjeto de la clase List con la información de todos los profesores
     */
    public List getProfesores() {
        List profesores = new ArrayList();
        Persona e;
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("obtenerProfesores"));
            ps.setInt(1, 2);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String nombre = rs.getString("nombre");
                String apellido = rs.getString("apellido");
                String cedula = rs.getString("cedula");
                String email = rs.getString("email");
                String ud = rs.getString("usuario");
                e = new Persona(nombre, apellido, cedula, "", email, ud);
                profesores.add(e);
            }
            return profesores;
        } catch (SQLException ex) {
            System.out.println("Error obteniendo la lista de los profesores " + ex);
            return null;
        } finally {
            conexion.closeConnection();
        }
    }
    /**
     * Método que recibe como parámetro un objeto de la Clase persona con los datos correspondientes a un 
    profesor y lo ingresa a la DB
     * @param profesor Objeto de la clase Persona con la información de un Profesor.
     * @return Int 1 si el ingreso se realiza correctamente, 2 si ocurre algún error.
     */
    public int IngresarProfesor(Persona profesor) {
        String tipoUsuario = "2";
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("ingresarProfesor"));
            ps.setString(1, profesor.getCedula());
            ps.setString(2, profesor.getUsuario());
            ps.setString(3, tipoUsuario);
            ps.setString(4, profesor.getNombre());
            ps.setString(5, profesor.getApellido());
            ps.setString(6, profesor.getEmail());
            System.out.println(ps.toString());
            ps.executeUpdate();
            System.out.println("El profesor se registro exitosamente en la BD");
            return 1;
        } catch (SQLException e) {
            System.out.println("Error al registrar profesor " + e.toString());
            return 2;
        } finally {
            conexion.closeConnection();
        }
    }
    /**
     * Método que recine un objeto de la clase Persona con la información correspondiente a un pofesor ya 
    ingresado en la DB, cabia los datos que estan la DB por la información que recibe, la identificación de 
    profesor sigue siendo la misma, esta no se puede modificar.
     * @param profesor Objero de la clase Profesor
     * @return Int 1 si se actualiza correctamente, 2 si ocurre algún error.
     */
    public int editarProfesor(Persona profesor) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("actualizarProfesor"));
            ps.setString(1, profesor.getNombre());
            ps.setString(2, profesor.getApellido());
            ps.setString(3, profesor.getEmail());
            ps.setString(4, profesor.getCedula());
            System.out.println(ps.toString());
            ps.executeUpdate();
            System.out.println("El profesor de cedula:" +profesor.getCedula()+"Se actualizo correctamente");
            return 1;
        } catch (SQLException e) {
            System.out.println("Error al actualizar profesor " + e.toString());
            return 2;
        } finally {
            conexion.closeConnection();
        }
    }
    
    /**
     * Método que recibe como parámetro la identificación de un profesor y elimina de la Base de datos 
    el profesor que tiene la identificación ingresada.
     * @param identificacion Identificacion del profesor que se va a eliminar
     * @return Int 1 si se realiza exitosamente la eliminación, 2 si ocurre algún error.
     */
    public int eliminarProfesor(String identificacion) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("eliminarProfesor"));
            ps.setString(1, identificacion);
            ps.executeUpdate();
            System.out.println("El profesor con numero de indentificacion " + identificacion + " se ha elinado correctamente");
            return 1;
        } catch (SQLException e) {
            System.out.println("Error eliminado el profesor:" +e.toString() );
            return 2;
        } finally {
            conexion.closeConnection();
        }
    }
    /**
     * Método que recibe un número de identificación y verifica si existe un profesor que responda a la cédula
    ingresada, de ser asi retorna TRUE de lo contrario retorna FALSE
     * @param identificacion identificación del profesor que se desea consultar su existencia
     * @return  True si existe, False si no existe.
     */
    public boolean existeProfesor(String identificacion) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("obtenerProfesor"));
            ps.setString(1, identificacion);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                System.out.println("El profesor con numero de indentificacion " + identificacion + " existe en ma DB");
                return true;
            } else {
                return false;
            }

        } catch (SQLException e) {
            System.out.println("Error sacando el profesor");
            return false;
        } finally {
            conexion.closeConnection();
        }
    }
   /**
    * Método que recibe la identificación de un profesor y retorna un objeto de la clase persona con toda la 
    información del profesor en la DB.
    * @param ced Identificación del profesor
    * @return Objeto de la clase Persona con la información del profesor.
    */ 
    public Persona getProfesor(String ced) {
        Persona e = null;
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("personaPorCedula"));
            ps.setString(1,ced);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String nombre = rs.getString("nombre");
                String apellido = rs.getString("apellido");
                String cedula = rs.getString("cedula");
                String email = rs.getString("email");
                String ud = rs.getString("usuario");
                e = new Persona(nombre, apellido, cedula, "", email, ud);
            }
            return e;
        } catch (SQLException ex) {
            System.out.println("Error obteniendo la informacion del profesor " + ex);
            return null;
        } finally {
            conexion.closeConnection();
        }
    }    
}
