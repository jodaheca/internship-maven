/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udea.drai.intercambio.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import udea.drai.intercambio.dto.Bitacora;

/**
 *Clase que contiene los metodos DAO encargados de acceder la tabla de las Bitcoras en la DB
 * @author Daniel Restrepo Arcila.
 */
public class BitacoraDAO {

    BDConexion conexion;

    public BitacoraDAO() {
        conexion = new BDConexion();
    }
    /**
     * Metodo que recibe como parametro un objeto de la clase Bitacora y la almacena en la Base de datos
     * @param bi objeto de la clase Bitacora
     * @return  Entero que indica si la operación se realizo exitosamente
     */    
    public int ingresarBitacora(Bitacora bi) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("ingresarBitacora"));
            ps.setInt(1, bi.getCodAnt());
            ps.setInt(2, bi.getCodNew());
            ps.setString(3, bi.getUser());
            
            if(bi.getObservacion() == null){
                bi.setObservacion("");
            }
            ps.setString(4, bi.getObservacion());
            if(bi.getSsoffi()== null){
                bi.setSsoffi("");
            }
            ps.setString(5, bi.getSsoffi());
            ps.setString(6, bi.getFecha());
            if (ps.executeUpdate() == 1) {
                return 1;
            }
        } catch (Exception ex) {
            System.out.println("Error creando homologación!: " + ex);
        }finally{
            conexion.closeConnection();
        }
        return 0;
    }
    /*Metodo que retorna una lista con todas las bitacoras que ha realizado un estudiante, es decir
    devuelve la informacion de todas las materias que un estudiante a cambiado de su semestre.*/
     public List cargarBitacora(int numIntercambio) {
        List bitacora = new ArrayList();
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("consultarBitacora"));
            ps.setInt(1, numIntercambio);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Bitacora bi = new Bitacora();
                bi.setCodigo(rs.getInt("codigoBitacora"));
                bi.setCodAnt(rs.getInt("codigoAnt"));
                bi.setCodNew(rs.getInt("codigoNew"));
                bi.setUser(rs.getString("usuario"));
                bi.setObservacion(rs.getString("observacion"));
                bi.setSsoffi(rs.getString("codigoSSOFFI"));
                bi.setFecha(rs.getString("fechaModificacion"));
                bitacora.add(bi);
            }
            return bitacora;
        } catch (Exception ex) {
            System.out.println("Error creando homologación!: " + ex);
        }finally{
            conexion.closeConnection();
        }
        return null;
    }
     /*Metodo que recibe como parametro el codigo de una Bitacora y saca toda la informacion de esta Bitacora
     desde la Base de Datos, retorna un objeto de la clase Bitacora.*/
    public Bitacora cargarBitacora(Bitacora bi) {

        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("cargarBitacora"));
            ps.setInt(1, bi.getCodigo());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                bi.setCodAnt(rs.getInt("codigoAnt"));
                bi.setCodNew(rs.getInt("codigoNew"));
                bi.setUser(rs.getString("usuario"));
                bi.setFecha(rs.getString("fechaModificacion"));
                bi.setSsoffi(rs.getString("codigoSSOFFi"));
                bi.setObservacion(rs.getString("observacion"));
            }
            return bi;
        } catch (Exception ex) {
            System.out.println("Error cargando  la homologacion! " + ex);
        }finally{
            conexion.closeConnection();
        }

        return null;
    }

}
