
package udea.drai.intercambio.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import udea.drai.intercambio.dto.Institucion;
import udea.drai.intercambio.dto.Semestre;
import udea.drai.intercambio.dto.TipoIntercambio;

/**
 * Clase que contiene todos los metodos DAO de los tipos de Intercambio que se pueden realizar
 * @author Daniel Restrepo Arcila
 */
public class OpcionIntercambioDAO {

    private BDConexion conexion;

    public OpcionIntercambioDAO() {
        conexion = new BDConexion();
    }

    /**
     * Método que permite cargar los tipos de intercambios que estan registrados en el sistema
     * @return  Objeto de la clase List con toda la información de los tipos de intercambio.
     */
    public ArrayList cargarTipos() {
        ArrayList lista = new ArrayList();
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("cargarTipo"));
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                TipoIntercambio t = new TipoIntercambio();
                t.setCodigo(rs.getString("codigo"));
                t.setNombre(rs.getString("nombreTipo"));
                lista.add(t);
            }
        } catch (Exception e) {
            System.out.println("Error cargando los tipos de intercambio: " + e);
        } finally {
            conexion.closeConnection();
        }

        return lista;
    }

    /**
     * Método que permite cargar todas las Instituciones que hay en el sistema
     * @return  Objeto de la clase List con  la información de las Instituciones
     */
    public ArrayList cargarInstituciones() {
        ArrayList lista = new ArrayList();
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("cargarInstituciones"));
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Institucion i = new Institucion();
                i.setCodigo(Integer.toString(rs.getInt("codigo")));
                i.setNombre(rs.getString("nombre"));
                i.setPais(rs.getString("pais"));
                lista.add(i);
            }
            return lista;
        } catch (Exception e) {
            System.out.println("Error cargando las instituciones: " + e);
        } finally {
            conexion.closeConnection();
        }
        return lista;

    }

    /**
     * Método que permite cargar los peridos que hay en el sistema Periodo = Año mas Semestre
    Ejemplo: 2015-2
     * @return  Objeto de la clase List con la información de los periodos
     */
    public ArrayList cargarPeriodos() {
        ArrayList lista = new ArrayList();
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("cargarPeriodos"));
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Semestre sem = new Semestre();
                sem.setCodigo(rs.getInt("codigo"));
                sem.setAno(rs.getInt("ano"));
                sem.setPeriodo(rs.getInt("periodo"));
                lista.add(sem);
            }
            return lista;
        } catch (Exception e) {
            System.out.println("Error cargando Periodos académicos!: " + e);
        } finally {
            conexion.closeConnection();
        }

        return lista;

    }
}
