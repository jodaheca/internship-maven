package udea.drai.intercambio.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import udea.drai.intercambio.dto.InfoCompromiso;
import udea.drai.intercambio.dto.Intercambio;
import udea.drai.intercambio.dto.MateriaIntercambio;
import udea.drai.intercambio.dto.TipoIntercambio;

/**
 *Clase que contiene todos los metodos DAO que permiten obtener e ingresar la 
 * información de los Intercambios.
 * @author Daniel Restrepo & Joaquin Hernández
 */


public class IntercambioDAO {

    private BDConexion conexion;

    public IntercambioDAO() {
        conexion = new BDConexion();
    }
    
    /**
     * Metodo que permite obtener las materias que tiene un intercambio
     * @param codIntercambio código del intercambio.
     * @return Objeto de la clase List con la información de las materias del intercambio.
     */
    public List<MateriaIntercambio> obtenerMateriasIntercabios(int codIntercambio){
        List<MateriaIntercambio> materias  = new ArrayList();
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("obtenerMateriasIntercambio"));
            ps.setInt(1, codIntercambio);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                MateriaIntercambio materia = new MateriaIntercambio();
                materia.setMateriaLocal(rs.getString("materiaLocal"));
                materia.setCodLocal(rs.getString("codLocal"));
                materia.setMateriaExtrangera(rs.getString("materiaExtranjero"));
                materia.setCodExtrangera(rs.getString("codExtranjera"));
                materia.setSemestre(rs.getString("anio")+"-"+ rs.getString("periodo"));
                materias.add(materia);
            }
            return materias;

        } catch (SQLException ex) {
            System.out.println("Error recuperando las Materias de un Intercambio! " + ex);
            return null;
        } finally {
            conexion.closeConnection();
        }
    }

    /**
     * Método que recibe como parámetro un objeto de la clase Intercambio, se encarga de registrar un nuevo 
     intercambio en el sistema.
     * @param i Objeto de la clase intercambio con la información del intercambio.
     * @return  Integer 1 si se ingresa correctamente, 2 si ocurre algun error.
     */
    public int ingresarIntercambio(Intercambio i) {
        int status = 0;
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = conexion.getConnection();
            PreparedStatement ps;
            if (i.getCodigo() == 0) {
                ps = con.prepareStatement(BDConexion.getStatement("insertarIntercambio"));
            } else {
                ps = con.prepareStatement(BDConexion.getStatement("modificarIntercambio"));
                ps.setInt(9, i.getCodigo());
            }
            ps.setInt(1, i.getOrigen());
            ps.setInt(2, i.getDestino());
            ps.setInt(3, i.getInicio());
            ps.setInt(4, i.getFin());
            ps.setString(5, i.getEstudiante());
            ps.setInt(6, i.getTipo());
            ps.setString(7, i.getIda());
            ps.setString(8, i.getRegreso());

            if (ps.executeUpdate() == 1) {
                ps = con.prepareStatement(BDConexion.getStatement("maximoIntercambio"));
                if (i.getCodigo() == 0) {
                    ResultSet rs = ps.executeQuery();
                    System.out.println("Modificacion exitosa!");
                    if (rs.next()) {
                        return (rs.getInt("codigo"));
                    }
                } else {
                    return i.getCodigo();
                }
                return 0;
            }
        } catch (Exception e) {
            System.out.println("Error ingresando el intercambio! " + e);
        } finally {
            conexion.closeConnection();
        }
        return 0;

    }

    /**
     * Método que recibe como parámetro un objeto de la clase Intercambio el cual contiene solo el código
     del intercambio, mediante el código obtiene toda la información del intercambio.
     * @param i Objeto de la Clase Intercambio con el código del intercambio.
     * @return  Objeto de la clase Intercambio con la informacion del intercambio.
     */
    public Intercambio cargarIntercambio(Intercambio i) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("cargarIntercambio"));
            ps.setInt(1, i.getCodigo());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                i.setOrigen(rs.getInt("instOrigen"));
                i.setDestino(rs.getInt("instDestino"));
                i.setInicio(rs.getInt("SemestreInicio"));
                i.setFin(rs.getInt("SemestreFin"));
                i.setOrigen(rs.getInt("instOrigen"));
                i.setEstudiante(rs.getString("estudiante"));
                i.setTipo(rs.getInt("tipoIntercambio"));
                i.setIda(rs.getString("fechaIda"));
                i.setRegreso(rs.getString("fechaRetorno"));
            }
            return i;

        } catch (Exception ex) {
            System.out.println("Error cargando el intercambio! " + ex);
        } finally {
            conexion.closeConnection();
        }
        return null;
    }
    /**
     * Método que recibe como parámetro el código de un intermcabio y retorna un HashMap con toda la información 
    del intercambio
     * @param codigo Código del intercambio
     * @return  Objeto HasMap con la información del intercambio
     */
    
    public HashMap cargarIntercambios(String codigo) {
        HashMap hm = new HashMap();

        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("cargarintercambios"));
            ps.setString(1, codigo);
            System.out.println(ps.toString());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                hm.put(rs.getInt("codigo"), rs.getString("tipo") + " de " + rs.getString("IAno") + " - " + rs.getString("IPer") + " a " + rs.getString("FAno") + " - " + rs.getString("FPer"));
            }
            return hm;
        } catch (Exception ex) {
            System.out.println("Error ingresando el intercambio! " + ex);
        } finally {
            conexion.closeConnection();
        }

        return null;
    }

    /**
     * Método que recibe como parámetro el nombre y número de Semestres de un tipo de Intercambio y lo ingresa
    en el sistema.
     * @param nombre nombre de tipo de intercambio
     * @param numSemestres numero de semestres del tipo de intercambio
     * @return  Integer 1 si el tipo de intercambio se ingresa correctamente, 2 si ocurrev algún error.
     */
    public int agregarIntercambio(String nombre, String numSemestres) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("agregarIntercambio"));
            ps.setString(1, nombre);
            ps.setString(2, numSemestres);
            System.out.println(ps.toString());
            ps.executeUpdate();
            ps.close();
            System.out.println("Intercambio ingresado correctamente");
            return 1;
        } catch (Exception ex) {
            System.out.println("Error al ingresar un nuevo tipo de intercambio! Error: " + ex);
            return 2;
        } finally {
            conexion.closeConnection();
        }
    }
    
    /**
     * Método que recibe como parámetro el código de un intercabio y lo elimina de la Base de Datos.
     * @param codigo codigo del Intercambio.
     * @return Integer 1 en caso de que la eliminación se realice correctamente o 2 si ocurre algún error
     */
    public int eliminarIntercambio(int codigo) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("EliminarIntercambio"));
            System.out.println(ps.toString());
            ps.setInt(1, codigo);
            ps.executeUpdate();
            System.out.println("Intercambio Eliminada exitosamente");
            return 1;
        } catch (SQLException e) {
            System.out.println("Error al eliminar el intercambio");
            return 2;
        } finally {
            conexion.closeConnection();
        }
    }
    
    /**
     * Método que permite obtener los tipos de intercambios que hay registrados en el sistema
     * @return Objeto de la clase List con la información de todos los intercambios. 
     */
    public List<TipoIntercambio> getTipoIntercambios() {
        List inter = new ArrayList();
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("ObtenerTipoIntercambios"));
            System.out.println(ps.toString());
            ResultSet tipos = ps.executeQuery();
            while (tipos.next()) {
                TipoIntercambio nueva = new TipoIntercambio();
                nueva.setCodigo(Integer.toString(tipos.getInt("codigo")));
                nueva.setNombre(tipos.getString("nombretipo"));
                nueva.setNumSemestres(tipos.getString("numSemestres"));
                inter.add(nueva);
            }
            System.out.println("Los tipos de intercambios se recuperaron exitosamente");
            return inter;
        } catch (Exception e) {
            System.out.println("error al sacar los tipos de intercambios de la BD, Error= " + e.toString());
        } finally {
            conexion.closeConnection();
        }
        return null;

    }

    /**
     * Método que recibe como parámetro la Identificación de un estudiante y obtiene los intercambios 
     * los que esta el estudiante..
     * @param cedula Número de identificación del estudiante.
     * @return objeto de la clase List que contiene todos los intercambios que tiene el estudiante.
     */
    public List<Intercambio> ObtenerintercambiosXEstudiante(String cedula) {
        List inter = new ArrayList();
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("ObtenerIntercambiosEstudiante"));
            ps.setString(1, cedula);
            System.out.println(ps.toString());
            ResultSet intercambios = ps.executeQuery();
            while (intercambios.next()) {
                Intercambio nueva = new Intercambio();
                nueva.setCodigo(intercambios.getInt("codigo"));
                nueva.setEstudiante(Integer.toString(intercambios.getInt("estudiante")));
                nueva.setOrigen(intercambios.getInt("instOrigen"));
                nueva.setDestino(intercambios.getInt("instDestino"));
                nueva.setInicio(intercambios.getInt("SemestreInicio"));
                nueva.setFin(intercambios.getInt("SemestreFin"));
                inter.add(nueva);
            }
            System.out.println("Los tipos de intercambios se recuperaron exitosamente");
            return inter;
        } catch (Exception e) {
            System.out.println("error al sacar los tipos de intercambios de la BD, Error= " + e.toString());
        } finally {
            conexion.closeConnection();
        }
        return null;

    }
    /**
     * Método que recibe como parámetro el código de un Intercambio y retorma el semestre Inicial del intercambio
     * @param codigo Codigo del intercambio
     * @return Integer con el código del primer semestre del intercambio
     */
    public int getSemestreInicio(int codigo) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("ObtenerSemestreIniIntercambio"));
            ps.setInt(1, codigo);
            System.out.println(ps.toString());
            ResultSet semestreIni = ps.executeQuery();
            int inicio = 0;
            while (semestreIni.next()) {
                inicio = semestreIni.getInt("semestreInicio");
            }
            return inicio;
        } catch (Exception e) {
            System.out.println("Error al obtener el semestre de inicio de un intercambio" + e.toString());
            return 0;
        } finally {
            conexion.closeConnection();
        }
    }
    /**
     * Método que recibe como parámetro el código de un intercambio y retorna el semestre 
     * final de dicho intercambio.
     * @param codigo Código del intercambio.
     * @return Código del semestre final del intercambio.
     */
    public int getSemestreFin(int codigo) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("ObtenerSemestreFinIntercambio"));
            ps.setInt(1, codigo);
            System.out.println(ps.toString());
            ResultSet semestreIni = ps.executeQuery();
            int fin = 0;
            while (semestreIni.next()) {
                fin = semestreIni.getInt("semestreFin");
            }
            return fin;
        } catch (Exception e) {
            System.out.println("Error al obtener el semestre de fin de un intercambio" + e.toString());
            return 0;
        } finally {
            conexion.closeConnection();
        }
    }
    /**
     * Método que recibe como parámetro un objeto de la clase Intercambio el cual contiene el código del 
    intercabio, mediante dicho código obtiene toda la información necesario para generar el compromiso que 
    deben firmar el profesor y el estudiante.
     * @param i Objeto de la clase Intercambio
     * @return Objeto de la clase InfoCompromiso con la información del compromiso.
     */
    public InfoCompromiso compromiso(Intercambio i) {
        InfoCompromiso comp = new InfoCompromiso();
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("generarCompromiso"));
            ps.setInt(1, i.getCodigo());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                comp.setIntercambio(i);
                comp.setIntercambio(i);
                comp.setPrograma(rs.getString("programa"));
                comp.setEstudiante(rs.getString("nombreEst"));
                comp.setApellido(rs.getString("apellidoEst"));
                comp.setIdEstudiante(rs.getString("identificacion"));
                comp.setInstDestino(rs.getString("destino"));
                comp.setInstOrigen(rs.getString("origen"));
                comp.setEmail(rs.getString("correo"));
                comp.setSemInicio(rs.getString("anoInicio") + " - " + rs.getString("perInicio"));
                comp.setSemFin(rs.getString("anoFin") + " - " + rs.getString("perFin"));
                comp.setIda(rs.getString("ida"));
                comp.setVuelta(rs.getString("retorno"));
                comp.setNombreProfesor(rs.getString("nomProf") + " " + rs.getString("apellidoProf"));
            }
            return comp;
        } catch (Exception e) {
            System.out.println("Error sacando el compromiso, Error= " + e.toString());
        } finally {
            conexion.closeConnection();
        }

        return null;
    }
}
