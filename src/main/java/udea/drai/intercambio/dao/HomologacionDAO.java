
package udea.drai.intercambio.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import udea.drai.intercambio.dto.Bitacora;
import udea.drai.intercambio.dto.Homologacion;

/**
 *
 * @author Daniel Restrepo Arcila
 * Nota: Homologacion = Materia
 */
public class HomologacionDAO {

    BDConexion conexion;

    public HomologacionDAO() {
        conexion = new BDConexion();
    }
    /*Metodo que recibe como parametro un objeto de la clase Homologación y otro de la clase Bitacora, 
    ingresa la información de la Homoloacion en la DB y si el objeto Bitacora es diferente de null es porque
    la homologación ingresa es un cambio de otra homologación, por lo cuál es necesario almacenar la Bitacora
    para dejar historial de los cambios que realizan los estudiantes en sus materias*/
    public int ingresarHomologacion(Homologacion h1, Bitacora bi) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("insertarHomologacion"));
            ps.setString(1, h1.getMateriaLocal());
            ps.setString(2, h1.getMateriaExtranjero());
            ps.setInt(3, h1.getActa());
            ps.setDate(4, h1.getFechaActa());
            ps.setString(5, h1.getRevision().toString());
            ps.setInt(6, h1.getIntercambio());
            ps.setInt(7, h1.getSemestre());
            ps.setString(8, h1.getCodLocal());
            ps.setString(9, h1.getCodExtranjero());
            ps.setInt(10, h1.getEstado());
            if (ps.executeUpdate() == 1) {
                if (bi != null) {
                    BitacoraDAO bDAO = new BitacoraDAO();
                    ps = con.prepareStatement(BDConexion.getStatement("obtenerProxHomologacion"));
                    ResultSet rs = ps.executeQuery();
                    if (rs.next()) {
                        bi.setCodNew(rs.getInt("ultimo"));
                    }
                    if (bDAO.ingresarBitacora(bi) == 1) {
                        return 1;
                    }
                }
                return 1;

            }
        } catch (Exception e) {
            System.out.println("Error creando homologación!: " + e);
        } finally {
            conexion.closeConnection();
        }
        return -1;
    }
    /*Este metodo recibe como parametro un entero correspondiente al codigo de una homologación, lo que hace
    es inhabilitar la homologación para que esta no sea mostrada al usuario final*/
    public int inhabilitar(int hom) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("cambiarEstado"));
            ps.setInt(1, hom);
            if (ps.executeUpdate() == 1) {
                return 1;
            }
        } catch (Exception ex) {
            System.out.println("Error inhabilitando homologación!: " + ex);
        } finally {
            conexion.closeConnection();
        }

        return 0;
    }
    
    /*Metodo que recibe como parametro el codio de un Intercambio, retorna una lista con todas las homologaciones
    que tiene el intercambio recibido como parametro*/
    public HashMap cargarHomologaciones(int intercambio) {
        HashMap lista = new HashMap();
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("cargarHomologaciones"));
            ps.setInt(1, intercambio);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                lista.put(rs.getInt("codigo"), rs.getString("mLocal") + " Por " + rs.getString("mExtranjero"));
            }
            return lista;
        } catch (Exception e) {
            System.out.println("Error cargando Homologaciones! " + e);
        } finally {
            conexion.closeConnection();
        }
        return null;
    }
    /*Metodo que recibe como parametro el codigo de un intercambio, retorna la informacion necesaria para 
    armar el comprobante que el profesor y el estudiante deben descargar, en este se encuentran las materias 
    que el estudiante cursara*/
    public ArrayList homologacionCompromiso(int intercambio) {
        ArrayList a = new ArrayList();
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("listarHomologacion"));
            ps.setInt(1, intercambio);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Homologacion hm = new Homologacion();
                hm.setCodigo(rs.getInt("codigo"));
                hm.setActa(rs.getInt("numActa"));
                if(rs.getString("fechaActa") != null){
                    hm.setFechaActa(Date.valueOf(rs.getString("fechaActa")));
                }else{
                    hm.setFechaActa(null);
                }
                hm.setIntercambio(rs.getInt("intercambio"));
                hm.setMateriaLocal(rs.getString("materiaLocal"));
                hm.setMateriaExtranjero(rs.getString("materiaExtranjero"));
                hm.setRevision(rs.getString("revision").charAt(0));
                hm.setSemestre(rs.getInt("semestre"));
                hm.setCodLocal(rs.getString("codLocal"));
                hm.setCodExtranjero(rs.getString("codExtranjera"));
                hm.setNota(rs.getString("notas"));
                hm.setEstado(rs.getInt("activo"));
                //lista.put(rs.getInt("codigo"), rs.getString("mLocal")+" Por "+rs.getString("mExtranjero")+" Autorizado el "+rs.getString("fechaActa"));
                a.add(hm);
            }
            return a;
        } catch (Exception e) {
            System.out.println("Error cargando Homologaciones! " + e);
        } finally {
            conexion.closeConnection();
        }
        return null;
    }
    
    /*Metodo que recibe un objeto de la clase Homologacion que contiene solo el codigo de la Homologacion
    va a la DB y saca toda la informacion de la Homologacion y la retorna.*/
    public Homologacion cargarHomologacion(Homologacion hm) {
       
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
             Connection con = conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("cargarHomologacion"));
            ps.setInt(1, hm.getCodigo());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                hm.setActa(rs.getInt("numActa"));
                if(rs.getString("fechaActa") != null){
                    hm.setFechaActa(Date.valueOf(rs.getString("fechaActa")));
                }else{
                    hm.setFechaActa(null);
                }
                hm.setIntercambio(rs.getInt("intercambio"));
                hm.setMateriaLocal(rs.getString("materiaLocal"));
                hm.setMateriaExtranjero(rs.getString("materiaExtranjero"));
                hm.setRevision(rs.getString("revision").charAt(0));
                hm.setSemestre(rs.getInt("semestre"));
                hm.setCodLocal(rs.getString("codLocal"));
                hm.setCodExtranjero(rs.getString("codExtranjera"));
                hm.setNota(rs.getString("notas"));
                hm.setEstado(rs.getInt("activo"));
            }
            return hm;
        } catch (Exception ex) {
            System.out.println("Error cargando  la homologacion! " + ex);
        } finally {
            conexion.closeConnection();
        }

        return null;
    }
    /*Metodo que permite modificar una homologacion ya existente en el sistema*/
    public int modificarHomologacion(Homologacion h) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("modificarHomologacion"));
            ps.setString(1, h.getMateriaLocal());
            ps.setString(2, h.getMateriaExtranjero());
            ps.setInt(3, h.getActa());
            ps.setString(4, h.getFechaActa().toString());
            ps.setString(5, h.getRevision().toString());
            ps.setInt(6, h.getIntercambio());
            ps.setInt(7, h.getCodigo());

            int rs = ps.executeUpdate();
            if (rs == 1) {
                return 1;
            }
        } catch (Exception ex) {
            System.out.println("Error cargando  la homologacion! " + ex);
        } finally {
            conexion.closeConnection();
        }
        return 0;
    }
    /**
     * Metodo mediante el cual se modifican tres campos de una homologacion
     * @param nota nota que el estudiante obtiene en esta materia
     * @param estado Esta de la Materia (Revisado, Pendiente, Incompleto,No revisado)
     * @param cod   Codigo de la holologacion
     * @return Int  si es 1 indica que la modificacion se realizo exitosamentede 
     * lo contrario retorna 0
     */
    public int modificarHomologacion(String nota, String estado, String cod) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("modificarHomologacion2"));
            ps.setString(1, nota);
            ps.setString(2, estado);
            ps.setString(3, cod);

            int rs = ps.executeUpdate();
            if (rs == 1) {
                return 1;
            }
        } catch (Exception ex) {
            System.out.println("Error actualizando  la homologacion! " + ex);
        } finally {
            conexion.closeConnection();
        }
        return 0;
    }
    
     /**
      * Metodo que permite modificar 4 campos de una homologacion de un estudiante
      *@param nota nota que el estudiante obtiene en esta materia
     * @param estado Esta de la Materia (Revisado, Pendiente, Incompleto,No revisado)
     * @param cod   Codigo de la holologacion
     * @param comentario Comentario que el profesor hace sobre la homologacion
     * @return Int  si es 1 indica que la modificacion se realizo exitosamentede 
     * lo contrario retorna 0
     */
    public int modificarHomologacion(String nota, String estado, String cod, String comentario) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("modificarHomologacion3"));
            ps.setString(1, nota);
            ps.setString(2, estado);
            ps.setString(3, comentario);
            ps.setString(4, cod);

            int rs = ps.executeUpdate();
            if (rs == 1) {
                return 1;
            }
        } catch (Exception ex) {
            System.out.println("Error actualizando  la homologacion! " + ex);
        } finally {
            conexion.closeConnection();
        }
        return 0;
    }
  
    /**
     * Metodo que recibe como parametro el codigo de un intercambio y un semestre del intercambio, retorna una
     * lista con las homologaciones que corresponde al intercambio seleccionado y mas especificamente al semestre
     * ingresado
     * @param intercambio Codigo del intercambio
     * @param semestre Codigo del semestre
     * @return List con todas las materias de un estudiante en un semestre e intercambio 
     * especifico
     */
      public List<Homologacion> obtenerHomologaciones(int intercambio, int semestre) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("obtenerhomologaciones"));
            ps.setInt(1, intercambio);
            ps.setInt(2, semestre);
            System.out.println(ps.toString());
            ResultSet rs = ps.executeQuery();
            List homologaciones = new ArrayList();
            while (rs.next()) {
                Homologacion nueva = new Homologacion();
                nueva.setCodigo(rs.getInt("codigo"));
                nueva.setComentario(rs.getString("comentario"));
                nueva.setMateriaLocal(rs.getString("materiaLocal"));
                nueva.setMateriaExtranjero(rs.getString("materiaExtranjero"));
                nueva.setActa(rs.getInt("numActa"));
                nueva.setFechaActa(rs.getDate("fechaActa"));
                String revision = rs.getString("revision");
                char[] nuevo = revision.toCharArray();
                nueva.setRevision(nuevo[0]);
                nueva.setIntercambio(intercambio);
                nueva.setSemestre(semestre);
                nueva.setCodLocal(rs.getString("codLocal"));
                nueva.setCodExtranjero(rs.getString("codExtranjera"));
                nueva.setNota(rs.getString("notas"));
                nueva.setActa(1);
                homologaciones.add(nueva);
            }
            return homologaciones;
        } catch (Exception e) {
            System.out.println("Error al recuperar las homologaciones de un estudiante" + e.toString());
            return null;
        } finally {
            conexion.closeConnection();
        }

    }
}
