

package udea.drai.intercambio.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import udea.drai.intercambio.dto.Requisito;

/**
 *Clase que contiene los métodos DAO mediante los cuales se puede acceder a toda la información de los Requisitos
    que debe cumplir un estudiante para realizar un intercambio.
  * @author Joaquin David Hernández Cárdenas <jdavidhc94@gmail.com>
 */
public class RequisitoDAO {
    private BDConexion conexion;

    public RequisitoDAO() {
        this.conexion = new BDConexion();
    }
    /**
     * Método que retorna un objeto de la clase List con la información de todos los requisitos 
     * existentes en la DB
     * @return Objeto de la clase List con la información de todos los requisitos.
     */
    public List getRequisitos(){
       List requisitos= new ArrayList();
       try{
           if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
           Connection con = this.conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("obtenerRequisitos"));
            System.out.println(ps.toString());
            ResultSet rq= ps.executeQuery();
            while(rq.next()){
                Requisito nuevo = new Requisito();
                nuevo.setCodigo(rq.getInt("codigo"));
                nuevo.setNombre(rq.getString("nombre"));
                requisitos.add(nuevo);
            }
           return requisitos;
       }catch(SQLException e){
           System.out.println("Error sacando los requisitos de la DB"+e.toString());
           return null;
       }finally {
            conexion.closeConnection();
        }
    }
}
