
package udea.drai.intercambio.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import udea.drai.intercambio.dto.registro;

/**
 *
 * @author Daniel Restrepo
 */
public class subirDAO {

    BDConexion conexion;

    public subirDAO() {
        conexion = new BDConexion();
    }

    public int subir(registro rg) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("subir"));
            ps.setInt(4, rg.getIntercambio());
            ps.setBinaryStream(1, rg.getArchivo().getInputStream(), (int) rg.getArchivo().getSize());
            ps.setString(2, rg.getFecha());
            ps.setInt(3, rg.getSemestre());
            if (ps.executeUpdate() == 1) {
                return 1;
            }
        } catch (Exception ex) {
            System.out.println("Error subiendo el archivo! " + ex);
        } finally {
            conexion.closeConnection();
        }

        return 0;
    }

    public HashMap listarArchivos(int intercambio) {
        HashMap hm = new HashMap();
        Connection con;
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            con = conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("listaraArchivos"));
            ps.setInt(1, intercambio);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                hm.put(rs.getString("idSubida"), "Notas del semestre " + rs.getString("ano") + " - " + rs.getString("periodo") + " subida el " + rs.getString("fecha"));
            }

            return hm;
        } catch (Exception ex) {
            System.out.println(ex);
        } finally {
            conexion.closeConnection();
        }
        return null;
    }

    public registro descargar(int id) {
        try {
            if (conexion.getConnection().isClosed()) {
                conexion = new BDConexion();
            }
            Connection con = conexion.getConnection();
            PreparedStatement ps = con.prepareStatement(BDConexion.getStatement("descargar"));
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                registro r = new registro();
                r.setBlob(rs.getBlob("archivo"));
                r.setFecha(rs.getString("fecha"));
                r.setSemestre(rs.getInt("semestre"));
                con.close();
                return r;
            }

        } catch (Exception ex) {
            System.out.println("Error descargando el archivo! " + ex);
        } finally {
            conexion.closeConnection();
        }
        return null;

    }
}
