package udea.drai.intercambio.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Clase que contiene metodos de utilidad creados para realizar operaciones
 * sobre Semestres.
 *
 * @author Joaquin Hernandez Cardenas <jdavidhc94@gmail.com>
 */
public class SemestreUtil {

    /**
     * Metodo mediante el cual se obtiene el semestre en el que estamos
     * actualmente, combina el año en el que estamos y calcula si es Semestre 1,
     * es decir entre enero y junioco si es semestre 2 es decir entre julio y diciembre
     *
     * @return String con el semestre actual
     */
    public String calcularSemestre() {
        String semestre = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd HH:mm:ss");
        Calendar calendar = new GregorianCalendar();
        int year = calendar.get(Calendar.YEAR);
        // Enero = 0 y Dciembre = 11
        int month = calendar.get(Calendar.MONTH);

        if (month <= 5) {
            semestre = year + "-" + "1";
        } else {
            semestre = year + "-" + "2";
        }
        return semestre;
    }
}
