package udea.drai.intercambio.util;

import java.util.ArrayList;
import java.util.List;
import udea.drai.intercambio.dao.IntercambioDAO;
import udea.drai.intercambio.dao.SemestreDAO;
import udea.drai.intercambio.dto.Semestre;

/**
 * Clase que contiene los metodos de utilidad creados para un intercambio
 * @author Joaquin Hernandez Cardenas <jdavidhc94@gmail.com>
 * 
 */
public class IntercambioUtil {

    /**
     * Metodo que recibe el codigo de un intercambio y retorna una lista
     * ordenada con los semestres que tiene el intercambio
     *
     * @param codIntercambio Codigo del intercambio
     * @return semestres  Lista con los semestres del intercambio 
     */
    public List obtenerSemestre(int codIntercambio) {

        IntercambioDAO managerIntercambio = new IntercambioDAO();
        int sInicio = managerIntercambio.getSemestreInicio(codIntercambio);
        int sFin = managerIntercambio.getSemestreFin(codIntercambio);
        List<Semestre> semestres = new ArrayList();
        if (sInicio != 0 && sFin != 0) {
            Semestre inicial = new Semestre();
            Semestre ultimo = new Semestre();
            SemestreDAO managerSemestre = new SemestreDAO();
            inicial = managerSemestre.obtenerSemestreCodigo(sInicio);
            ultimo = managerSemestre.obtenerSemestreCodigo(sFin);
            int anioInicial = inicial.getAno();
            int anioUltimo = ultimo.getAno();
            int periodoInicial = inicial.getPeriodo();
            while (anioInicial <= anioUltimo) {
                Semestre nuevo = new Semestre();
                nuevo = managerSemestre.obtenerSemestreAnioPeriodo(anioInicial, periodoInicial);
                if (nuevo != null) {
                    semestres.add(nuevo);
                }
                if (periodoInicial == 1) {
                    nuevo = managerSemestre.obtenerSemestreAnioPeriodo(anioInicial, 2);
                    if (nuevo != null) {
                        semestres.add(nuevo);
                    }
                }
                periodoInicial = 1;
                anioInicial = anioInicial + 1;
            }
            Semestre ultimoLista = semestres.get(semestres.size() - 1);
            if (ultimo.getCodigo() != ultimoLista.getCodigo()) {
                semestres.remove(semestres.size() - 1);
            }
        }

        return semestres;

    }

}
