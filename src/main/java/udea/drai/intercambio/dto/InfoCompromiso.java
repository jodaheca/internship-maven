/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package udea.drai.intercambio.dto;

/**
 *
 * @author Usuario
 */
public class InfoCompromiso {
    private String programa;
    private String estudiante;
    private String apellido;
    private String nombreProfesor;

    public String getNombreProfesor() {
        return nombreProfesor;
    }

    public void setNombreProfesor(String nombreProfesor) {
        this.nombreProfesor = nombreProfesor;
    }
    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    private String idEstudiante;
    private String instOrigen;
    private Intercambio intercambio;

    public Intercambio getIntercambio() {
        return intercambio;
    }

    public void setIntercambio(Intercambio intercambio) {
        this.intercambio = intercambio;
    }
    public String getInstOrigen() {
        return instOrigen;
    }

    public void setInstOrigen(String instOrigen) {
        this.instOrigen = instOrigen;
    }
    private String instDestino;
    private String email;
    private String semInicio;
    private String semFin;
    private String ida;
    private String vuelta;

    public String getPrograma() {
        return programa;
    }

    public void setPrograma(String programa) {
        this.programa = programa;
    }

    public String getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(String estudiante) {
        this.estudiante = estudiante;
    }

    public String getIdEstudiante() {
        return idEstudiante;
    }

    public void setIdEstudiante(String idEstudiante) {
        this.idEstudiante = idEstudiante;
    }

    public String getInstDestino() {
        return instDestino;
    }

    public void setInstDestino(String instDestino) {
        this.instDestino = instDestino;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSemInicio() {
        return semInicio;
    }

    public void setSemInicio(String semInicio) {
        this.semInicio = semInicio;
    }

    public String getSemFin() {
        return semFin;
    }

    public void setSemFin(String semFin) {
        this.semFin = semFin;
    }

    public String getIda() {
        return ida;
    }

    public void setIda(String ida) {
        this.ida = ida;
    }

    public String getVuelta() {
        return vuelta;
    }

    public void setVuelta(String vuelta) {
        this.vuelta = vuelta;
    }
  
}
