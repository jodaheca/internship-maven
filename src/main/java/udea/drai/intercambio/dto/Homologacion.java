/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package udea.drai.intercambio.dto;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author Usuario
 */
public class Homologacion implements Serializable {
    private int codigo;
    private String materiaLocal;
    private String materiaExtranjero;
    private int acta;
    private Date fechaActa;
    private Character revision;
    private int intercambio;
    private int semestre;
    private String codLocal;
    private String codExtranjero;
    private String nota;
    private int estado;
    private String comentario;

    /**
     * @return the codigo
     */
    public int getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the materiaLocal
     */
    public String getMateriaLocal() {
        return materiaLocal;
    }

    /**
     * @param materiaLocal the materiaLocal to set
     */
    public void setMateriaLocal(String materiaLocal) {
        this.materiaLocal = materiaLocal;
    }

    /**
     * @return the materiaExtranjero
     */
    public String getMateriaExtranjero() {
        return materiaExtranjero;
    }

    /**
     * @param materiaExtranjero the materiaExtranjero to set
     */
    public void setMateriaExtranjero(String materiaExtranjero) {
        this.materiaExtranjero = materiaExtranjero;
    }

    /**
     * @return the acta
     */
    public int getActa() {
        return acta;
    }

    /**
     * @param acta the acta to set
     */
    public void setActa(int acta) {
        this.acta = acta;
    }

    /**
     * @return the fechaActa
     */
    public Date getFechaActa() {
        return fechaActa;
    }

    /**
     * @param fechaActa the fechaActa to set
     */
    public void setFechaActa(Date fechaActa) {
        this.fechaActa = fechaActa;
    }

    /**
     * @return the revision
     */
    public Character getRevision() {
        return revision;
    }

    /**
     * @param revision the revision to set
     */
    public void setRevision(Character revision) {
        this.revision = revision;
    }

    /**
     * @return the intercambio
     */
    public int getIntercambio() {
        return intercambio;
    }

    /**
     * @param intercambio the intercambio to set
     */
    public void setIntercambio(int intercambio) {
        this.intercambio = intercambio;
    }

    /**
     * @return the semestre
     */
    public int getSemestre() {
        return semestre;
    }

    /**
     * @param semestre the semestre to set
     */
    public void setSemestre(int semestre) {
        this.semestre = semestre;
    }

    /**
     * @return the codLocal
     */
    public String getCodLocal() {
        return codLocal;
    }

    /**
     * @param codLocal the codLocal to set
     */
    public void setCodLocal(String codLocal) {
        this.codLocal = codLocal;
    }

    /**
     * @return the codExtranjero
     */
    public String getCodExtranjero() {
        return codExtranjero;
    }

    /**
     * @param codExtranjero the codExtranjero to set
     */
    public void setCodExtranjero(String codExtranjero) {
        this.codExtranjero = codExtranjero;
    }

    /**
     * @return the nota
     */
    public String getNota() {
        return nota;
    }

    /**
     * @param nota the nota to set
     */
    public void setNota(String nota) {
        this.nota = nota;
    }

    /**
     * @return the estado
     */
    public int getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(int estado) {
        this.estado = estado;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }
    
    
    
}
