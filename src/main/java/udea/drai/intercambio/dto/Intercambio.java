/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package udea.drai.intercambio.dto;

import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import java.util.Date;

/**
 *
 * @author Usuario
 */
public class Intercambio {
        private int codigo;
        private int tipo ;
        private int origen ;
        private int destino ;
        private int inicio ;
        private int fin ;
        private String ida ;
        private String regreso ;
        private String estudiante;

        public Intercambio(){
            
        }
        
        public Intercambio(int codigo, int tipo, int origen, int destino, int inicio, int fin, String ida, String regreso, String e){
            this.codigo = codigo;
            this.tipo = tipo;
            this.origen=origen;
            this.destino=destino;
            this.inicio = inicio;
            this.fin= fin;
            this.ida=ida;
            this.regreso=regreso;
            this.estudiante=e;
        }
    /**
     * @return the tipo
     */
    public int getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the origen
     */
    public int getOrigen() {
        return origen;
    }

    /**
     * @param origen the origen to set
     */
    public void setOrigen(int origen) {
        this.origen = origen;
    }

    /**
     * @return the destino
     */
    public int getDestino() {
        return destino;
    }

    /**
     * @param destino the destino to set
     */
    public void setDestino(int destino) {
        this.destino = destino;
    }

    /**
     * @return the inicio
     */
    public int getInicio() {
        return inicio;
    }

    /**
     * @param inicio the inicio to set
     */
    public void setInicio(int inicio) {
        this.inicio = inicio;
    }

    /**
     * @return the fin
     */
    public int getFin() {
        return fin;
    }

    /**
     * @param fin the fin to set
     */
    public void setFin(int fin) {
        this.fin = fin;
    }

    /**
     * @return the ida
     */
    public String getIda() {
        return ida;
    }

    /**
     * @param ida the ida to set
     */
    public void setIda(String ida) {
        this.ida = ida;
    }

    /**
     * @return the regreso
     */
    public String getRegreso() {
        return regreso;
    }

    /**
     * @param regreso the regreso to set
     */
    public void setRegreso(String regreso) {
        this.regreso = regreso;
    }

    /**
     * @return the estudiante
     */
    public String getEstudiante() {
        return estudiante;
    }

    /**
     * @param estudiante the estudiante to set
     */
    public void setEstudiante(String estudiante) {
        this.estudiante = estudiante;
    }

    /**
     * @return the codigo
     */
    public int getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
}
