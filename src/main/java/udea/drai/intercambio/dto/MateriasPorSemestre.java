
package udea.drai.intercambio.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * DTO que permite tener la información de las materias de un estudiante en 
 * un semestre especifico, es utilizado para enviar al profesor la información de todos 
 * las materias de un estudiante con sus respectivo semestres
 * 
 * @author Joaquin David Hernández  <jdavidhc94@gmail.com>
 */
public class MateriasPorSemestre  implements Serializable{
    private List materias;
    private Semestre semestre;

    /**
     * Constructor de la clase
     */
    public MateriasPorSemestre() {
        semestre = new Semestre();
        materias = new ArrayList();
    }

    /**
     * Metodo para obtener las materias
     * @return List que contiene todas las materias del estudiante en un 
     * semestres especifico
     */
    public List getMaterias() {
        return materias;
    }

    /**
     * Metodo para ingresar las materias de un estudiante
     * @param materias 
     */
    public void setMaterias(List materias) {
        this.materias = materias;
    }

    /**
     * Metodo para obtener el semestre en el que se cursaron o cursaran las 
     * materias
     * @return  objeto de la clase Semestre
     */
    public Semestre getSemestre() {
        return semestre;
    }

    /**
     * Metodo para ingresar el semestre del estudiante
     * @param semestre 
     */
    public void setSemestre(Semestre semestre) {
        this.semestre = semestre;
    }
    
    
    
    
    
    
}
