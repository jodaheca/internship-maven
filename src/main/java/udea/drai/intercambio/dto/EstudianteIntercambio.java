/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package udea.drai.intercambio.dto;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author Usuario
 */
public class EstudianteIntercambio  implements Serializable{
    String cedula;
    String nombre;
    String apellido;
    String tipoIntercambio;
    String programa;
    String Institucion;
    int semestreInicio;
    int semestreFin;
    Date fechaIda;
    Date fechaRegreso;
    int codigoIntercambio;
    

    public EstudianteIntercambio() {
    }

    public int getCodigoIntercambio() {
        return codigoIntercambio;
    }

    public void setCodigoIntercambio(int condigoIntercambio) {
        this.codigoIntercambio = condigoIntercambio;
    }

    public String getInstitucion() {
        return Institucion;
    }

    public void setInstitucion(String Institucion) {
        this.Institucion = Institucion;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    

    public String getPrograma() {
        return programa;
    }

    public void setPrograma(String programa) {
        this.programa = programa;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipoIntercambio() {
        return tipoIntercambio;
    }

    public void setTipoIntercambio(String tipoIntercambio) {
        this.tipoIntercambio = tipoIntercambio;
    }

    public int getSemestreInicio() {
        return semestreInicio;
    }

    public void setSemestreInicio(int semestreInicio) {
        this.semestreInicio = semestreInicio;
    }

    public int getSemestreFin() {
        return semestreFin;
    }

    public void setSemestreFin(int semestreFin) {
        this.semestreFin = semestreFin;
    }

    public Date getFechaIda() {
        return fechaIda;
    }

    public void setFechaIda(Date fechaIda) {
        this.fechaIda = fechaIda;
    }

    public Date getFechaRegreso() {
        return fechaRegreso;
    }

    public void setFechaRegreso(Date fechaRegreso) {
        this.fechaRegreso = fechaRegreso;
    }
    
    
}
