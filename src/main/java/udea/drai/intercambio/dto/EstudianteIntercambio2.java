/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package udea.drai.intercambio.dto;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author Usuario
 */
public class EstudianteIntercambio2  implements Serializable {
    String cedula;
    int codigoIntercambio;
    String nombre;
    String apellido;
    String tipoIntercambio;
    String semestreInicio;
    String semestreFin;
    String programa;
    String institucion;
    Date fechaIda;
    Date fechaRegreso;

    public EstudianteIntercambio2() {
        this.cedula = "";
        this.nombre = "";
        this.apellido = "";
        this.tipoIntercambio = "";
        this.semestreInicio = "";
        this.semestreFin = "";
        this.programa = "";
        this.institucion = "";
    }

    public int getCodigoIntercambio() {
        return codigoIntercambio;
    }

    public void setCodigoIntercambio(int codigoIntercambio) {
        this.codigoIntercambio = codigoIntercambio;
    }
    
    public Date getFechaRegreso() {
        return fechaRegreso;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getPrograma() {
        return programa;
    }

    public void setPrograma(String programa) {
        this.programa = programa;
    }

    public void setFechaRegreso(Date fechaRegreso) {
        this.fechaRegreso = fechaRegreso;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipoIntercambio() {
        return tipoIntercambio;
    }

    public void setTipoIntercambio(String tipoIntercambio) {
        this.tipoIntercambio = tipoIntercambio;
    }

    public String getSemestreInicio() {
        return semestreInicio;
    }

    public void setSemestreInicio(String semestreInicio) {
        this.semestreInicio = semestreInicio;
    }

    public String getSemestreFin() {
        return semestreFin;
    }

    public void setSemestreFin(String semestreFin) {
        this.semestreFin = semestreFin;
    }

    public Date getFechaIda() {
        return fechaIda;
    }

    public void setFechaIda(Date fechaIda) {
        this.fechaIda = fechaIda;
    }
    
}
