/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package udea.drai.intercambio.dto;

/**
 *
 * @author Usuario
 */
public class Revision {
   int codigo;
   int codRequisito;
   String estado="";
   int intercambio;
   String estudiante;

    public Revision() {
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getCodRequisito() {
        return codRequisito;
    }

    public void setCodRequisito(int codRequisito) {
        this.codRequisito = codRequisito;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getIntercambio() {
        return intercambio;
    }

    public void setIntercambio(int intercambio) {
        this.intercambio = intercambio;
    }

    public String getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(String estudiante) {
        this.estudiante = estudiante;
    }
   
}
