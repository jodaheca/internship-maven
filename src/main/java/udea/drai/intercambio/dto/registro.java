/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package udea.drai.intercambio.dto;

import java.sql.Blob;
import org.apache.commons.fileupload.FileItem;

/**
 *
 * @author Usuario
 */
public class registro {
    private String nombre;
    private String fecha;
    private FileItem archivo;
    private Blob blob;

    public Blob getBlob() {
        return blob;
    }

    public void setBlob(Blob blob) {
        this.blob = blob;
    }
    private int semestre;
    private int intercambio;

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the archivo
     */
    public FileItem getArchivo() {
        return archivo;
    }

    /**
     * @param archivo the archivo to set
     */
    public void setArchivo(FileItem archivo) {
        this.archivo = archivo;
    }

    /**
     * @return the semestre
     */
    public int getSemestre() {
        return semestre;
    }

    /**
     * @param semestre the semestre to set
     */
    public void setSemestre(int semestre) {
        this.semestre = semestre;
    }

    /**
     * @return the intercambio
     */
    public int getIntercambio() {
        return intercambio;
    }

    /**
     * @param intercambio the intercambio to set
     */
    public void setIntercambio(int intercambio) {
        this.intercambio = intercambio;
    }
    
}
