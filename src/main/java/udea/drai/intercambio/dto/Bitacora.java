/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package udea.drai.intercambio.dto;

import java.io.Serializable;

/**
 *
 * @author Usuario
 */
public class Bitacora  implements Serializable{
    private int codigo;
    private int codAnt;
    private int codNew;
    private String user;
    private String observacion;
    private String ssoffi;
    private String fecha;

    /**
     * @return the codigo
     */
    public int getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the codAnt
     */
    public int getCodAnt() {
        return codAnt;
    }

    /**
     * @param codAnt the codAnt to set
     */
    public void setCodAnt(int codAnt) {
        this.codAnt = codAnt;
    }

    /**
     * @return the codNew
     */
    public int getCodNew() {
        return codNew;
    }

    /**
     * @param codNew the codNew to set
     */
    public void setCodNew(int codNew) {
        this.codNew = codNew;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the observacion
     */
    public String getObservacion() {
        return observacion;
    }

    /**
     * @param observacion the observacion to set
     */
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    /**
     * @return the ssoffi
     */
    public String getSsoffi() {
        return ssoffi;
    }

    /**
     * @param ssoffi the ssoffi to set
     */
    public void setSsoffi(String ssoffi) {
        this.ssoffi = ssoffi;
    }

    /**
     * @return the fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    
}
