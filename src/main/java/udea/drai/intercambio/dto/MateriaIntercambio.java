

package udea.drai.intercambio.dto;

/**
 *
 * @author Joaquin Hernández Cárdenas
 */
public class MateriaIntercambio {
    String materiaLocal;
    String materiaExtrangera;
    String codLocal;
    String codExtrangera;
    String Semestre;

    public MateriaIntercambio() {
        this.materiaLocal = "";
        this.materiaExtrangera = "";
        this.codLocal = "";
        this.codExtrangera = "";
        this.Semestre = "";
    }

    public String getMateriaLocal() {
        return materiaLocal;
    }

    public void setMateriaLocal(String materiaLocal) {
        this.materiaLocal = materiaLocal;
    }

    public String getMateriaExtrangera() {
        return materiaExtrangera;
    }

    public void setMateriaExtrangera(String materiaExtrangera) {
        this.materiaExtrangera = materiaExtrangera;
    }

    public String getCodLocal() {
        return codLocal;
    }

    public void setCodLocal(String codLocal) {
        this.codLocal = codLocal;
    }

    public String getCodExtrangera() {
        return codExtrangera;
    }

    public void setCodExtrangera(String codExtrangera) {
        this.codExtrangera = codExtrangera;
    }

    public String getSemestre() {
        return Semestre;
    }

    public void setSemestre(String Semestre) {
        this.Semestre = Semestre;
    }
    
    
    
    
           
}
