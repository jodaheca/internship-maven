package udea.drai.intercambio.dto;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

public class FormatoDocumento extends PdfPageEventHelper {

    private Image foto;
    PdfPTable table = new PdfPTable(2);

    /**
     * Constructor de la clase, inicializa la imagen que se utilizara en el
     * membrete
     */
    public FormatoDocumento(String absoluteDiskPath) {
        try {
            foto = Image.getInstance(absoluteDiskPath);
            foto.scaleToFit(800, 1000);
            foto.setAlignment(Chunk.ALIGN_MIDDLE);
            foto.setAbsolutePosition(-20, -10);
        } catch (Exception r) {
            System.err.println("Error al leer la imagen");
        }
    }

    /**
     * Manejador del evento onEndPage, usado para generar el encabezado
     */
    public void onEndPage(PdfWriter writer, Document document) {

        try {
            document.add(foto);
        } catch (Exception doc) {
            doc.printStackTrace();
        }
    }
}
